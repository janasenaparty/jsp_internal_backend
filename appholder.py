import os
import sys
import simplejson as json
import flask
from flask_session      import Session
from flask import session
from flask_jwt_extended import JWTManager
from flask_sqlalchemy   import SQLAlchemy
import logging
from flask_limiter      import Limiter
from flask_limiter.util import get_remote_address
from celery             import Celery
from celery.schedules   import crontab
from flask_cors import CORS
from flask_wtf.csrf import CSRFProtect
from datetime import timedelta
# Local Imports

from utils        import saasutils
import config 
# Azure connections
from azure.storage.blob import BlockBlobService
from azure.storage.table import TableService
from azure.storage.models import CorsRule



table_service = TableService(account_name='janasenabackup',
                             account_key='B4Smr3G5FYzIcta5cTisx728LLGvYLJOtxqGVANOfx+udWYQh+NZ5dLE37sUtcYuyEu+ZPihGM4QXm7NgUo4IA==')
block_blob_service = BlockBlobService(account_name='janasenabackup',
                                      account_key='B4Smr3G5FYzIcta5cTisx728LLGvYLJOtxqGVANOfx+udWYQh+NZ5dLE37sUtcYuyEu+ZPihGM4QXm7NgUo4IA==')


# Make sure Logging framework is initialized early on.
application =  jwt = db  = limiter = celeryapp = jwtGenerator = None
#

if application is None:
    saasutils.initialize_logging()

logger = logging.getLogger(__name__)

ServicesConfigFile = saasutils.getConfigurationFile()

ServicesConfig = None
ServicesConfigDict = None


def make_celery(app):
    celery = Celery(
        app.import_name,
        broker=app.config['CELERY_BROKER_URL']
    )
    celery.conf.update(app.config)
    
    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)
    
    celery.Task = ContextTask
    return celery


if application is None:
    logger.info("Reading Services Configuration file: {}".format(ServicesConfigFile))
    ServicesConfigEncrypted = {}
    ServicesConfig = saasutils.getServiceConfig(ServicesConfigFile)
    ServicesConfigDict = saasutils.GetJSON(ServicesConfig)
    application_config = {}
    for service in ["flask", "jwt","redis"]:
        application_config.update(ServicesConfigDict.get('credentials').get(service))
   
    application = flask.Flask(__name__,static_url_path="/static")
    application.config["SQLALCHEMY_DATABASE_URI"]= config.SQLALCHEMY_DATABASE_URI
    application.config["DB_CONNECTION"] = config.DB_CONNECTION
    application.config["SQLALCHEMY_TRACK_MODIFICATIONS"]= True
    application.config["SQLALCHEMY_ENGINE_OPTIONS"]= {"pool_recycle":  600,"echo":False,"echo_pool":False,"max_overflow":  10,"pool_size":     10,"pool_timeout":  60,"pool_pre_ping": True}
    application.config.update(application_config)
    CORS(application)
    # Secret key for csrf
    application.config['PROPAGATE_EXCEPTIONS'] = True
    csrf = CSRFProtect()
    csrf.init_app(application)
    # Initialize Server Side sessions.
    # Initiate the Server Side Session Objects for Security.
    # For now we are using filesystem based session storage.
    # We will move to redis cache based.s
    #import redis
    
    # application.config['SESSION_TYPE'] = 'redis'
    # application.config['SESSION_REDIS'] = redis.from_url(application.config['SESSION_REDIS'])
    # application.config.from_object(__name__)
    # Session(application)
    
    jwt = JWTManager(application)
    # Initialize the DB.
    
    db = SQLAlchemy()
    db.init_app(application)
    with application.app_context():
        db.metadata.reflect(db.engine, views=True,extend_existing=True)

    # Initialize rate limiter.
    limiter = Limiter(application,
                      key_func=get_remote_address)
    
    
    application.config['CELERY_TIMEZONE'] = 'Asia/Kolkata'
    application.config['CELERY_ACCEPT_CONTENT'] = ['msgpack','yaml','json']
    application.config['CELERY_WORKER_MAX_TASKS_PER_CHILD'] = 500

    application.config['CELERY_TASK_SERIALIZER'] = 'json'
    application.config['CELERY_RESULT_SERIALIZER'] = 'json'
    application.config['CELERY_BROKER_URL'] = config.REDIS_LIVE_URL
    application.config['CELERY_RESULT_BACKEND'] = 'redis'  
    
    celery_app=celeryapp = make_celery(application)
    celeryapp.conf.update(application.config)   


    # Initialize the blueprints
    import blueprints
    logger.info(application.url_map)



from celery_tasks.sms_email import donations_check_task
from celery_tasks.sms_email import supporter_id_update
from utilities.others import missedcalls_mapping_task
@celery_app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):    
    sender.add_periodic_task(crontab(minute=0, hour='*/6'), donations_check_task, name="donations_check_task")    
    #sender.add_periodic_task(crontab(minute=23, hour='50'), supporter_id_update, name="supporter_id_update")    
    #sender.add_periodic_task(crontab(hour=23,minute=55),missedcalls_mapping_task,name='missedcalls_mapping_task')


from flask_wtf.csrf import CSRFError

# def generate_csrf_token():
#     print(session["_csrf_token"])
#     if '_csrf_token' not in session:
#         session['_csrf_token'] = some_random_string()
#     return session['_csrf_token']

@application.errorhandler(CSRFError)
def errorhandler(reason):
    token = session.pop('_csrf_token', None)
    print(token)
    print(session.pop('csrf_token', None))
    if not token or token != request.form.get('csrf_token'):
        errors = []
        errors.append('Your CSRF Token Expired.Please Try Again')
        data = {'status': 403}
        message = {
            'errors': errors,
            'data': data,
        }
        resp = flask.jsonify(message)
        resp.status_code = 403

        return resp


# application.jinja_env.globals['csrf_token'] = generate_csrf_token


@application.route("/token", methods=['POST', 'GET'])
def token():
    token = generate_csrf_token()
    return json.dumps({"errors": [], "data": {"status": '1', "token": token}})

# Below this code is all production methods.
@application.teardown_appcontext
def shutdown_session(exception=None):
    db.session.remove()


@application.errorhandler(429)
def ratelimit_handler(error):
    response = flask.jsonify({
        "serviceError": "Too many requests. API rate limit exceeded. Please try after sometime.",
        "serviceResult": None,
        "serviceName": "JanaSena Portal"
    })
    response.status_code = 200
    return response


@application.errorhandler(saasutils.PermissionDenied)
def permission_denied(error):
    response             = flask.jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@application.errorhandler(saasutils.UIGenericError)
def generic_ui_error(error):
    response             = flask.jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@application.errorhandler(saasutils.PostDataMissing)
def generic_post_error(error):
    response             = flask.jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@application.errorhandler(saasutils.ArgumentsMissing)
def generic_get_error(error):
    response             = flask.jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@application.errorhandler(saasutils.SystemGenericError)
def generic_system_error(error):
    response             = flask.jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@jwt.additional_claims_loader
def add_claims_to_access_token(identity):
    try:
        return {
            'post_data': flask.request.get_json()
        }
    except Exception as ex:
        return {
            'post_data': {}
        }

@jwt.expired_token_loader
def handle_expired_error(jwt_header,jwt_data):  
  response = flask.jsonify({
        "serviceError": "Secure Token Expired. Please contact support team.",
        "serviceResult": None,
        "serviceName": "JanaSena Portal Portal"
    })
  response.status_code = 401
  return response

@jwt.token_verification_failed_loader
@jwt.invalid_token_loader
@jwt.revoked_token_loader
@jwt.unauthorized_loader
def jwt_error_handler(e):
    response = flask.jsonify({
        "serviceError": "Secure Token Validation Failed. Please contact support team.",
        "serviceResult": None,
        "serviceName": "JanaSena Portal Portal"
    })
    response.status_code = 401
    return response