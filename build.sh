#!/bin/bash
echo "build started...."
docker login --username jsptech --password Janasena@2014
jsp_internal_frontend=/opt/codebase/hello_world
CURR_DIR=`pwd`
if [ ! -d $jsp_internal_frontend ]
then
     echo "$jsp_internal_frontend is not there go with default... jsp_internal_backend needs jsp_internal_frontend repo"
     exit 1 
else
     cd jsp_internal_frontend
     echo "Building frontend ..."
     npm install 
     echo "Setup appropriate PATH..."
          
     echo "=================================="
     npm run build --prod
     echo "Completed  build..."
     cd $CURR_DIR
     if [ -d $CURR_DIR/dist ] 
     then 
	    rm -rf $CURR_DIR/dist
        mkdir -p $CURR_DIR/dist
     else 
        mkdir -p $CURR_DIR/dist
     fi 
     echo "Copying build related files..."
     cp -r $jsp_internal_frontend/build/*  dist/
     echo "Completed copying build related files..."
fi
if [ ! -d dist ]
then
    echo "Portal UI Build Failed"
    exit 1
fi

rm -rf portalui.tar.gz
(cd dist ; tar czf ../portalui.tar.gz *)
# copying ui files completed 


docker build  -t  jsptech/jspdocker:jsp-internal-backend-1.0.1 .
if [ $? -ne 0 ]
then
    echo "Docker Build Failed"
    cd ..
    exit 1
fi
rm -rf portalui.tar.gz
rm -rf certs
rm -rf dist
docker tag jsptech/jspdocker:jsp-internal-backend-1.0.1 jsptech/jspdocker:jsp-internal-backend-1.0.1
if [ $? -ne 0 ]
then
    echo "Docker tagging Failed"
    cd ..
    exit 1
fi

docker push jsptech/jspdocker:jsp-internal-backend-1.0.1
if [ $? -ne 0 ]
then
    echo "Docker push failed"
    cd ..
    exit 1
fi
