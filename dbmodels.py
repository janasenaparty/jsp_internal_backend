from sqlalchemy import *

from appholder import db
from models.saasmodel import SAASModel
import datetime

class JanasenaMissedcalls2(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['janasena_missedcalls2']    
    
    def __repr__(self):
        return '<janasena_missedcalls2 {}>'.format(self.jsp_supporter_seq_id)
                                           


class ShatagniMagazine(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['shatagni_magazine_team']    
    
    def __repr__(self):
        return '<shatagni_magazine_team {}>'.format(self.id)

class ACHead(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['ac_head']    
    
    def __repr__(self):
        return '<ac_head {}>'.format(self.id)


class MembershipTargets(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['membership_targets']
    def __repr__(self):
        return '<MembershipTargets %r>' % (self.id)

class MembershipFields(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['member_fields'] 

    __mapper_args__ = {'primary_key': [__table__.c.seq_id,__table__.c.membership_id]}
    def __repr__(self):
        return '<MembershipFields %r>' % (self.seq_id)
class SMSDeliveryCodes(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['sms_delivery_codes']         

    __mapper_args__ = {'primary_key': [__table__.c.code]}    
    def __repr__(self):
        return '<SMSDeliveryCodes %r>' % (self.code)

class AdminIPAddress(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['admin_ip_address']       

    __mapper_args__ = {'primary_key': [__table__.c.seq_id]} 
        
    def __repr__(self):
        return '<adminIPAddress %r>' % (self.seq_id)


class PollingStations(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['polling_booths_2018_jan'] 
    __mapper_args__ = {'primary_key': [__table__.c.seq_no]} 
    def __repr__(self):
        return '<PollingStations %r>' % (self.seq_no)

class Mandals(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['mandals']
    __mapper_args__ = {'primary_key': [__table__.c.id]}

    def __repr__(self):
        return '<Mandals %r>' % (self.id)
class member_session(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['member_session']


    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.seq_id)

    def __repr__(self):
        return '<member_session %r>' % (self.membership_id)

class NRIMembers(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['nri_members']



    def __repr__(self):
        return '<NRIMembers %r>' % (self.id)

class Memberships(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['janasena_membership']

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def get_role(self):
        if self.is_volunteer == 'T':
            return 'V'
        else:

            return 'M'

    def __repr__(self):
        return '<Memberships %r>' % (self.membership_id)

class MembersThroughSMS(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['member_with_sms']

    def __repr__(self):
        return '<member_with_sms %r>' % (self.voter_id)

class AssemblyConstituencies(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['assembly_constituencies']

        
    def __repr__(self):
        return '<AssemblyConstituencies %r>' % (self.seq_id)

class VoterStatus(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['voter_registration_status']

    def __repr__(self):
        return '<VoterStatus %r>' % (self.seq_id)

class Voters(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['voters']


    def __repr__(self):
        return '<Voters %r>' % (self.voter_id)

class MembershipPending(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['membership_pending']


    def __repr__(self):
        return '<MembershipPending %r>' % (self.user_mobile)

class Admin(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['admin_team']


    def is_authenticated(self):
        return True

    def is_active(self, session_id):
        active_session = Admin.query.join(AdminSession, Admin.id == AdminSession.admin_id).filter(
            AdminSession.session_id == session_id, AdminSession.status == 'A').first()

        if active_session:
            return True
        return False

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def get_role(self):
        return unicode(self.urole)

    def __repr__(self):
        return '<Admin %r>' % (self.email)

class AdminSession(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['session_admin']

    def __repr__(self):
        return '{}-{}-{}-{}'.format(self.admin_id, self.status, self.session_id, self.session_val)

class Frontdesk(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['front_desk_team']

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def get_role(self):
        return unicode(self.urole)

    def __repr__(self):
        return '<Frontdesk %r>' % (self.email)

class OfflineMemberships(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['janasainyam_team']


    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def get_role(self):
        return unicode(self.urole)

    def __repr__(self):
        return '<OfflineMemberships %r>' % (self.email)
class Janasena_Missedcalls(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['janasena_missedcalls']


    def __repr__(self):
        return '<Missedcalls %r>' % (self.jsp_supporter_seq_id)

class BisSession(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['bis_session']



    def is_authenticated(self):
        return True


    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def get_role(self):
        return unicode(self.urole)

    def __repr__(self):
        return '<BIS %r>' % (self.admin_id)

class JspsurveySession(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['jspsurvey_session']



    def is_authenticated(self):
        return True


    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def get_role(self):
        return unicode(self.urole)

    def __repr__(self):
        return '<BIS %r>' % (self.admin_id)



################################ kriya models ##################

def check_valid_date(inputDate):
        import datetime
        year,month,day = inputDate.split('-')
        isValidDate = True
        try :
            datetime.datetime(int(year),int(month),int(day))
        except ValueError :
            isValidDate = False
        return isValidDate

##################### KRIYA Modals ############## ##########################################
class KriyaVolunteers(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['kriya_volunteers']



        
    Active    = "Active"
    Suspended = "Suspended"
    Pending   = "Pending"
    
    

    def __repr__(self):
        return '<KriyaVolunteer %r>' % self.id
    
    def isUserActive(self):
        return self.status == User.Active
    def verifyPin(self,pin):
        return self.pin == pin  


    @staticmethod
    def validateUser( name, mobile,state,district_id,constituency_id):
        
        if  name is None :
            logger.error(" Name should not be empty")
            return False, "Name should not be empty"

        if  len(name) > 100 :
            logger.error(" Name cannot be bigger than 100 characters")
            return False, "Name is too big."      
        if  mobile is None :
            logger.error(" mobile should not be empty")
            return False, "mobile should not be empty"
        
        if len(mobile) > 10:
            logger.error("Mobile Number cannot be bigger than 10 digits")
            return False, "Mobile Number is too big."
        if state is None:
            return False, "state should not be empty: {}".format(state)
        if district_id is None:
            return False, "district_id should not be empty: {}".format(district_id)
        if constituency_id is None:
            return False, "constituency_id should not be empty: {}".format(constituency_id)
        return True, None



class KriyaMembers(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['kriya_members']



    def __repr__(self):
        return '<KriyaMember %r>' % self.id
    
    
   
    @staticmethod
    def validateUser(name, mobile, email, state, district_id, constituency_id,dob,
        gender,education,occupation,address,working_with_janasena_from_year_and_month,
        do_you_know_janasena_principles,
        which_principles_are_you_aware_of,how_much_time_can_you_spend_for_party_activities,aadhar_number):   


        
        if len(name) > 100:
            logger.error(" Name cannot be bigger than 100 characters")
            return False, "Name is too big."      
        if name is None or name.isspace():
            return False, "Full name is missing."   
        if mobile is None or mobile.isspace():
            return False, "Mobile number is missing."
        if len(mobile) != 10:            
            return False, "Invalid mobile number."

        # Validate Email:
        if email is None or email.isspace():            
            return False, "email id missing: "
        if state is None or state.isspace():
            return False, "state should not be empty: "
        if district_id is None or district_id.isspace():
            return False, "district_id should not be empty"
        if constituency_id is None or constituency_id.isspace():
            return False, "constituency_id should not be empty"
        if dob is None or dob.isspace():
            return False, "dob field should not be empty"
        if not check_valid_date(dob):
            return False, "Invalid date of birth. please check agaiin"
        if education is None or education.isspace():
            return False, "education field should not be empty"    
        if occupation is None or occupation.isspace():
            return False, "occupation field should not be empty"  
        if address is None or address.isspace():
            return False, "address field should not be empty" 
        if working_with_janasena_from_year_and_month is None or working_with_janasena_from_year_and_month.isspace():
            return False," journey_year missing"
        if working_with_janasena_from_year_and_month is None or working_with_janasena_from_year_and_month.isspace():
            return False," journey_year missing"
        if do_you_know_janasena_principles is None or do_you_know_janasena_principles.isspace():
            return False," do_you_know_janasena_principles missing"
        if which_principles_are_you_aware_of is None:
            return False," which_principles_are_you_aware_of missing"
        if how_much_time_can_you_spend_for_party_activities is None or how_much_time_can_you_spend_for_party_activities.isspace():
            return False," how_much_time_can_you_spend_for_party_activities field missing"
        if aadhar_number is None or aadhar_number.isspace():
            return False, "Aadhar number is missing."
        if len(aadhar_number) != 12:            
            return False, "your aadhar number is invalid"

        
        return True, None


class KriyaMemberNomineeDetails(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['kriya_member_nominee_details']

    


    def __repr__(self):
        return '<KriyamemberNominee %r>' % self.id
    
class KriyaVolunteerSession(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['kriya_volunteer_session']  

    def __repr__(self):
        return '<KriyaVolunteerSession %r>' % self.id


class KriyaMemberpaymentDetails(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['kriya_member_payment_details']  
    def __repr__(self):
        return '<KriyaMemberpaymentDetails %r>' % self.id


from sqlalchemy.ext.declarative import declarative_base, DeferredReflection

Base = declarative_base(cls=DeferredReflection)


# class Donations(SAASModel, db.Model):
#     __table__ = db.Model.metadata.tables['donations_view']


#     def __repr__(self):
#         return '<janasena_donations %r>' % self.seq_id

   
class KriyaMembersDashboard(SAASModel,db.Model):
    __table__ = db.Model.metadata.tables['kriya_members_dashboard_view']

    __mapper_args__ = {'primary_key': [__table__.c.id,__table__.c.aadhar_number]}
    table_metadata = [{"data":"id","title":"id"},{"data":"name","title":"name"},{"data":"mobile","title":"mobile"},{"data":"gender","title":"gender"},{"data":"state","title":"state"},{"data":"district_name","title":"District"},{"data":"constituency_name","title":"Assembly"},{"data":"aadhar_number","title":"Aadhar"},{"data":"nominee_name","title":"Nominee name"},{"data":"nominee_aadhar_number","title":"nominee_aadhar_number"},{"data":"dob","title":"dob"},{"data":"age","title":"age"},{"data":"jsp_id","title":"jsp_id"},{"data":"payment_date","title":"Payment Date"},{"data":"payment_status","title":"Payment Status"},{"data":"payment_id","title":"Payment ID"},{"data":"how_much_time_can_you_spend_for_party_activities","title":"how_much_time_can_you_spend_for_party_activities"},{"data":"payment_verified","title":"Payment Verified"},{"data":"added_by","title":"Added By"},{"data":"remarks","title":"Remarks"}]
    UI_Columns = ['state','district_id','age','constituency_id','address','district_name','constituency_name','aadhar_number','nominee_name','nominee_aadhar_number','working_with_janasena_from_year_and_month','do_you_know_janasena_principles','which_principles_are_you_aware_of','participated_jsp_activities','how_much_time_can_you_spend_for_party_activities','payment_mode','payment_id','payment_date','payment_url','payment_status','aadhar_card','photo','added_by','dob','jsp_id','id','payment_verified','name','mobile','remarks','gender','email']
    filters= [{"id":"state","label":"State","type":"integer","input":"select","values":{"1":"Andhra Pradesh","2":"Telangana"},"filter_name":"state","has_dependent_filters":'true',"dependent_filters_props":{"district_level_filter":'true',"assembly_level_filter":'true',"district_column_name":"district_id","assembly_column_name":"constituency_id"},"operators":["equal"]},{"id":"name","label":"Member Name","type":"string","operators":["equal","contains"]},{"id":"mobile","label":"Phone Number","type":"string","operators":["equal","contains"]},{"id":"aadhar_number","label":"Aadhar Number","type":"string","operators":["equal","contains"]},{"id":"added_by","label":"Volunteer","type":"string","operators":["equal","contains"]},{"id":"payment_verified","label":"Payment Verification","type":"string","input":"radio","operators":["equal","not_equal"],"values":[{"Yes":"Completed"},{"No":"Not Completed"}]},{"id":"payment_status","label":"Payment Status","type":"string","input":"radio","operators":["equal","not_equal"],"values":[{"Success":"Success"},{"Pending":"Pending"}]},{"id":"gender","label":"Gender","type":"string","input":"radio","operators":["equal","not_equal"],"values":[{"Male":"Male"},{"Female":"Female"}]},{"id":"age","label":"Age","type":"integer","input":"text","operators":["equal","between","less","greater","less_or_equal","greater_or_equal"]},{"id":"jsp_id","label":"JSP ID","type":"string","operators":["equal","contains"]},{"id":"payment_date","label":"Creation Date","type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":"true","autoclose":"true"},"operators":["between","equal","less_or_equal","greater","less","greater_or_equal"]}]
    def __repr__(self):
        return '<janasena_kriya_members_dashboard %r>' % self.id




class JspItVolunteersDashboard(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['jsp_it_volunteers_dashboard_view']
    __mapper_args__ = {'primary_key': [__table__.c.seq_id]}


    table_metadata = [
    {"data":"id","title":"id"},
   { "data": "name" ,"title": "name"},
   { "data": "email" ,"title": "email"},
   { "data": "phone","title" :"phone"},
   { "data": "whatsapp_no","title":"whatsapp_no" },
   { "data": "work_location","title":"work_location" },
   { "data": "skill_set","title":"skill_set" },
   { "data": "jsp_id","title":"jsp_id" },
   { "data": "support_type","title":"support_type" },
   {"data":"state","title":"State"},       
   { "data": "district_name" ,"title":"District"},
   { "data": "constituency_name" ,"title":"Assembly"},
   { "data": "experience","title":"Experience" },
   { "data": "dob","title":"dob" },
   { "data": "country" ,"title":"Country"},
   {"data":"updated_already","title":"Updated_already"},
   {"data":"create_dttm","title":"create_dttm"},
   {"data":"district_name","title":"district_name"},
   {"data":"constituency_name","title":"constituency_name"},
   
   ]
    UI_Columns = ['id','name','email','phone','whatsapp_no','work_location',
    'skill_set','jsp_id','support_type', 
    'state','district_id','constituency_id','experience',
    'dob','country','updated_already','create_dttm','district_name','constituency_name']
    
    def __repr__(self):
        return '<jsp_it_volunteers_dashboard %r>' % self.seq_id
