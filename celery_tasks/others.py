
import io
import os
import urllib3
import base64
from io import BytesIO,StringIO

import PIL
import numpy as np
import pandas as pd
import psycopg2
from PIL import Image
from PyPDF2 import PdfFileReader, PdfFileWriter
from azure.storage.blob import BlockBlobService
from flask import current_app
from psycopg2.extras import RealDictCursor
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.utils import ImageReader
from reportlab.pdfgen.canvas import Canvas

import config
from appholder import celery_app, application as app, db
from dbmodels import Janasena_Missedcalls
from utilities.classes import encrypt_decrypt, Membership_details
from utilities.general import update_membership_targets, CustomPara, upload_image_custom


def write_voters_count():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()

        cur.execute("select state_id,constituency_name,constituency_id from assembly_constituencies where state_id<=2 order by constituency_id ")
        rows =cur.fetchall()
        
        for ac in rows:
            assembly=row[2]
            state_id=row[0]
            
            cur.execute(
                "select state,ac_no,sex,count (*) from voters group by sex,ac_no,state having  ac_no=%s and state=%s",
                (assembly, state_id))
            rows = cur.fetchall()
            total_voters = 0
            male_voters = 0
            female_voters = 0

            for row in rows:
                total_voters = total_voters + row[3]
                
                if str(row[2]).rstrip() == 'Male':
                    male_voters = male_voters + row[3]
                elif str(row[2]).rstrip() == 'Female':
                    female_voters = female_voters + row[3]
            # print total_voters, male_voters, female_voters
            cur.execute(
                "update assembly_constituencies set total_votes=%s,male_votes=%s,female_votes=%s where constituency_id=%s and state_id=%s",
                (total_voters, male_voters, female_voters, id, 2))
            con.commit()

        con.commit()
        con.close()
    except (psycopg2.Error, Exception) as e:
        con.close()
        print(str(e))

@celery_app.task
def clear_and_update_voter(voter_id, phone, membership_id, referral_id):
    try:
        con = psycopg2.connect(app.config["DB_CONNECTION"])
        cur = con.cursor()
        cur.execute("delete from membership_pending  where user_mobile=%s or lower(user_mobile)=%s ",
                    (phone, membership_id.lower()))

        if voter_id != '' and voter_id is not None:
            cur.execute("insert into voter_registration_status(voter_id,status)values(%s,%s)", (voter_id, 'T'))
        con.commit()
        con.close()
        # voter_details_instanse = Voter_details()
        # voter_details_instanse.update_voter_status(voter_id)
        update_membership_targets(membership_id, referral_id)
    except (psycopg2.Error, Exception) as e:
        con.close()
        print(str(e))
        

@celery_app.task
def delete_blob(container_name, blob_name):
    block_blob_service = BlockBlobService(account_name='janasenabackup',
                                          account_key='B4Smr3G5FYzIcta5cTisx728LLGvYLJOtxqGVANOfx+udWYQh+NZ5dLE37sUtcYuyEu+ZPihGM4QXm7NgUo4IA==')

    block_blob_service.delete_blob(container_name, blob_name)

@celery_app.task
def shitty_form_task(name, age, caste, gender, qualification, mobile_no, email, job, ac, pc, jsp_id, voter_id, exp,

                village,

                mandal, image_url,
                skills=''
                ):
    skills = skills.replace('"', '').split(',')
    if image_url == "" or image_url[:4] == 'data':
        # print " no image didn't create pdf"
        return
    print("Inside celery task")
    if mobile_no:
        # folder = r"C:/Users/janasena/PycharmProjects/do-func/formbuilding/new_pdfs/" + pc + '/' + ac + '/'

        if(type(age) == int):
            age = str(age)

        packet = io.BytesIO()
        can = Canvas(packet, pagesize=A4)
        can.setFillColor(colors.blue)
        can.setFont('Helvetica-Bold', 10)
        ps = ParagraphStyle('title', fontSize=10, leading=27,
                            firstLineIndent=190,textColor = colors.blue
                            )
        # print(ps.listAttrs())

        basewidth = 300
        tick = u"\u2713"


        can.drawString(89.13258826517648, 690, name.upper())

        can.drawString(77.04978409956811, 663, village.upper())
        can.drawString(276.35636271272534, 663, mandal.upper())

        # can.drawString(107.21234442468881, 630, dob.upper())
        can.drawString(108.7490474980949, 630, age.upper())
        can.drawString(227.2428244856489, 630, gender.upper())

        can.drawString(112.17551435102865, 600, caste.upper())
        can.drawString(321.4820929641859, 600, qualification.upper())

        can.drawString(68.97002794005584, 568, job.upper())
        can.drawString(332.0434340868681, 568, mobile_no.upper())

        can.drawString(90.09271018542029, 536, email.upper())

        can.drawString(141.9392938785877, 505, ac.upper())

        can.drawString(160.18161036322067, 474, pc.upper())

        can.drawString(123.69697739395474, 443, jsp_id.upper())
        can.drawString(375.2489204978409, 443, voter_id.upper())
        if ('motivational speakers' in skills):
            can.drawString(25.2032004064007, 368, tick.upper())
        if ('local issues' in skills):
            can.drawString(305.2413004826009, 368, tick.upper())
        if ('Door to door election campaign' in skills):
            can.drawString(25.2032004064007, 338, tick.upper())
        if ('law and order' in skills):
            can.drawString(305.2413004826009, 338, tick.upper())

        if 'social media publicity' in skills:
            can.drawString(25.2032004064007, 308, tick.upper())
        if 'banners publicity' in skills:
            can.drawString(305.2413004826009, 308, tick.upper())
        if 'content writing' in skills:
            can.drawString(25.2032004064007, 274, tick.upper())
        if 'leadership' in skills:
            can.drawString(305.2413004826009, 274, tick.upper())

        if 'political analysis ' in skills:
            can.drawString(25.2032004064007, 226, tick.upper())

        if 'cultural activities' in skills:
            can.drawString(305.2413004826009, 226, tick.upper())

        experi_p = CustomPara("<b>" + exp + "</b>", ps)
        experi_p.wrapOn(can, 500, 10)
        experi_p.drawOn(can, 42.4056388113, 65)

        if (image_url != "" and image_url != "nan"):
            try:
                print(image_url)
                # urlretrieve(image_url, 'temp.jpg')
                if(image_url[:4] == 'data'):
                    print('base64')
                    image_url = image_url.split(',')[1]
                    filedata = StringIO(base64.decodestring(image_url))
                else:
                    filedata = urllib3.urlopen(image_url)
                # datatowrite = filedata.read()
                # with open('temp.jpg', 'wb') as f:
                #     f.write(datatowrite)
                # time.sleep(2)
                img = Image.open(filedata)
                wpercent = (basewidth / float(img.size[0]))
                hsize = int((float(img.size[1]) * float(wpercent)))
                img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
                img = img.crop((0, 0, basewidth, 350))

                im = ImageReader(img)

                can.drawImage(im, 422, 535, 102, preserveAspectRatio=True, anchor='c')
            except Exception as e:

                print(str(e))
                print()
                return

        can.showPage()
        can.save()

        # Move to the beginning of the StringIO buffer
        packet.seek(0)
        new_pdf = PdfFileReader(packet)
        # Read your existing PDF
        print(os.getcwd())
        with app.app_context():
            with current_app.open_resource('static/pdf_downloader/f3.pdf','rb') as f:
                # print(f.read())
                existing_pdf = PdfFileReader(f)
                output = PdfFileWriter()
                # Add the "watermark" (which is the new pdf) on the existing page
                page = existing_pdf.getPage(0)
                page.mergePage(new_pdf.getPage(0))
                output.addPage(page)
                # Finally, write "output" to a real file
                name = name.strip()
                file_name = str(jsp_id).lower() + ".pdf"

                try:
                    # folder = r"C:/Users/janasena/PycharmProjects/do-func/formbuilding/new_pdfs/"+pc+'/'+ac+'/'


                    # print(temp)

                    f = io.BytesIO()
                    output.write(f)

                    # # temp.seek(0)
                    # f.seek(0)
                    # print(f)
                    # outputStream = open(temp, "rb")

                    f.seek(0)
                    if config.ENVIRONMENT_VARIABLE == 'local':
                        file_url = upload_image_custom(f, "assembly-pdfs-test",file_name)
                    else:
                        file_url = upload_image_custom(f, "assembly-pdfs", file_name)
                    print(file_url)
                    con = psycopg2.connect(config.DB_CONNECTION)
                    with con.cursor() as cur:
                        cur.execute("""
                            update assembly_comittee
                             set pdf_url=%s
                             where membership_id=%s
                        """,(file_url,jsp_id,))
                        con.commit()
                        return file_url

                except Exception as e:
                    print("here ")
                    print(str(e))

@celery_app.task
def generate_current():
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor(cursor_factory=RealDictCursor) as cur:
        cur.execute("""
            select acm.seq_id,acm.name,acm.village,m.mandal_name as mandal,acm.age,acm.gender,acm.caste,acm.education,acm.occupation,acm.phone,acm.email,
 	        acs.constituency_name as assembly,acs.parliament_constituency_name as parliament,acm.membership_id,acm.voter_id,acm.experience,acm.skills,acm.photo_url,acm.pdf_url
            from assembly_comittee acm
            join assembly_constituencies acs on acm.assembly=acs.constituency_id and acs.state_id=acm.state_id
            left join mandals m on m.id=acm.mandal and m.state_id=acm.state_id and m.constituency_id=acm.assembly
            where  acm.photo_url != ''  and (pdf_url is null or pdf_url = '') and source is null
            order by acm.seq_id

        """)
        rows = cur.fetchall()
        for row in rows:
            if row['pdf_url'] in [None, '']:
                print(row['skills'],type(row['skills']))

                shitty_form_task(name=row['name'],

                                       age=str(row['age']) if row['age'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                 np.NAN] else '',
                                       caste=row['caste'] if row['caste'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                  np.NAN] else '',
                                       gender=row['gender'] if row['gender'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                     np.NAN] else '',
                                       qualification=row['education'] if row['education'] not in ['NAN', 'NaN', 'nan',
                                                                                                  np.NaN,
                                                                                                  np.NAN] else '',
                                       mobile_no=row['phone'] if row['phone'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                      np.NAN] else '',
                                       email=row['email'] if row['email'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                  np.NAN] else '',
                                       job=row['occupation'] if row['occupation'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                          np.NAN] else '',
                                       ac=row['assembly'] if row['assembly'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                     np.NAN] else '',
                                       pc=row['parliament'] if row['parliament'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                         np.NAN] else '',
                                       jsp_id=str(
                                           row['membership_id'] if row['membership_id'] not in ['NAN', 'NaN', 'nan',
                                                                                                np.NaN,
                                                                                                np.NAN] else ''),
                                       voter_id=str(
                                           row['voter_id'] if row['voter_id'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                      np.NAN] else ''),
                                       exp=row['experience'] if row['experience'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                          np.NAN] else '',

                                       village=row['village'] if row['village'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                        np.NAN] else '',

                                       mandal=row['mandal'] if row['mandal'] not in ['NAN', 'NaN', 'nan', np.NaN,
                                                                                     np.NAN] else '',
                                       image_url=str(row['photo_url']),
                                       skills=row['skills'])



@celery_app.task
def get_map():
    print("got hit")
    bio = BytesIO()
    con = psycopg2.connect(config.DB_CONNECTION)
    files=['static/assets/Vizag district local admins .xlsx','static/assets/Vizag IT Volunteers.xlsx']
    for file in files:
        with con.cursor() as cur:
            bio = BytesIO()
            df1 = pd.read_excel(file)

            for row in df1.iterrows():
                # print(row[0])
                # print(row[1])
                item = row[1]['phone']
                items = str(item).strip().split('/')
                for item in items:
                    if len(item) == 12:
                        search = item
                    else:
                        search = '91' + item

                    crypted = encrypt_decrypt(str(search),'E')
                    cur.execute("""
                        select membership_id,polling_station_id,voter_id,constituency_id,state_id
                        from janasena_membership
                        where phone=%s
                    """,(crypted,))
                    r  = cur.fetchone()
                    if r is not None:
                        df1.loc[row[0],'membership_id'] = r[0]
                        polling_station_id = r[1]

                        df1.loc[row[0],'Voter ID'] = encrypt_decrypt(r[2])
                        constituency_id = r[3]
                        state_id = r[4]
                        cur.execute("""
                            select polling_station_name
                            from polling_booths_2018_jan
                            where constituency_id=%s and state_id=%s and part_no=%s
                        """,(constituency_id,state_id,polling_station_id))
                        rm = cur.fetchone()
                        if rm is None:
                            continue
                        else:

                            df1.loc[row[0], 'Polling Station No'] = str(polling_station_id)
                            df1.loc[row[0], 'Polling Station Name'] = str(rm[0])
            writer = pd.ExcelWriter(bio, engine='xlsxwriter')
            df1.to_excel(writer, sheet_name='Sheet1')
            writer.save()
            bio.seek(0)
            file_name="something.xlsx"
            upload_image_custom(bio, "excelsheets", file_name)

@celery_app.task
def save_requests(ip_address,request_url,hostname,api_name,query):
    
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("insert into janasena_requests_tracker(ipaddress,request_url,host,api_name,query)values(%s,%s,%s,%s,%s)",(ip_address,request_url,hostname,api_name,query))
        con.commit()
        con.close()
    except Exception as e:
        # print ip_address,request_url,hostname,api_name,query
        print(str(e)) +" in saving request"


@celery_app.task
def map_booth_with_voter_id(voter_id):
    con = psycopg2.connect(config.DB_CONNECTION)
    with con.cursor() as cur:
        cur.execute("""
            select state,ac_no,part_no from voters_2018
            where voter_id=%s
        """,(voter_id,))
        row = cur.fetchone()
        if row is not None:
            cur.execute("""
                update voter_mapping
                set state_id=%s,acid=%s,pbid=%s
                where voter_id = %s
            """,(row[0],
                 row[1],
                 row[2],

                 voter_id))
            con.commit()

    pass






