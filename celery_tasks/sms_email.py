import requests
from sqlalchemy import exc

import config
from appholder import celery_app, db,block_blob_service
from dbmodels import Janasena_Missedcalls,KriyaMembersDashboard
from main import shurl, sendmailmandrill, draw_image, get_pdf_receipt,get_nri_pdf_receipt, sendbulkemails,get_card_details
from utilities.sms import sendBULKSMS,sendBULKSMS_samemessage
from utilities.classes import encrypt_decrypt,Membership_details
from utilities.sms import send_bulk_sms_same_message
import psycopg2
from psycopg2.extras import RealDictCursor
from flask import url_for
from xlrd import open_workbook
from datetime import timedelta
#from shatagni_receipt import subs_receipt
from werkzeug.utils import secure_filename
from utilities.general import get_payment_details,id_generator
import tempfile
import os
import csv
import datetime
from sqlalchemy_filters import apply_filters
from sqlalchemy import desc
import json
import logging
logger = logging.getLogger(__name__)

def is_jsonable(x):
    try:
        json.dumps(x)
        return True
    except:
        return False
def getDBModel(report_id):
    return KriyaMembersDashboard

@celery_app.task
def sendExportResultsToEmail(name,email,filters,report_id):
    print("cp1")
    db_model = getDBModel(report_id)
    
    query = db.session.query(db_model)
    filtered_query = apply_filters(query, filters)

    count = filtered_query.count()
    filtered_query = filtered_query.order_by(desc(db_model.id))          

    rows =filtered_query.all() 
    UI_Columns = db_model.UI_Columns
    data = []
    print("cp2")
    for row in rows:   
        row = row.__dict__   
        d={}
        for k,v in row.items():
            
            if k in UI_Columns :
                try:
                    if isinstance(v, (float, int, str, list, dict, tuple)):
                        d[k] = v
                    elif isinstance(v,(datetime.datetime,datetime.date)):
                        d[k] = v.strftime("%d-%m-%Y")
                    else:                        
                        if v is not None:
                            d[k] = v
                        else:
                            d[k] = " "
                except Exception as ex:
                    print(ex,k,v)
            
        
        data.append(d)
       
    handle, filepath = tempfile.mkstemp(suffix='.csv')
    w_file = open(filepath, 'wb')
    dict_writer = csv.DictWriter(w_file, UI_Columns)
    dict_writer.writeheader()
    dict_writer.writerows(data)
    w_file.close()
    email_message =  " Hello "+name+", here is the export results. PFA"
    subject  = "Export Report"

    sendmailmandrill(name, email, email_message, subject,attachments=[filepath])




@celery_app.task
def sendNriDonationsEmail(transaction_id,donar_name,email):
    message = open("static/nri_donation_steps.html").read()

    try:
        
        email_message = message
        
        if email != '':
            
            subject = "Details on how to remit your donation to JANASENA PARTY"
            email_message = email_message.replace("{{name}}", str(donar_name))
            email_message = email_message.replace("{{transaction_id}}", str(transaction_id))
            name = "JANASENA PARTY"
            sendmailmandrill(name, email, email_message, subject)

    except Exception as e:
        print(str(e))
from werkzeug.datastructures import FileStorage
from io import StringIO
@celery_app.task
def upload_passport_file(stream, filename, name, content_length, content_type, headers,transaction_id):
    try:
        container = 'nripassports'
       
        passport_url=''
        
        
        passport_file = FileStorage(stream=StringIO(stream), filename=filename, name=name, content_type=content_type, content_length=content_length)
        filename = secure_filename(passport_file.filename)
        fileextension = filename.rsplit('.', 1)[1]
        Randomfilename = id_generator()
        filename = Randomfilename + '.' + fileextension
        try:
            block_blob_service.create_blob_from_stream(container, filename, passport_file)
            passport_url = 'https://janasenabackup.blob.core.windows.net/' + container + '/' + filename
            passport_url=encrypt_decrypt(passport_url,'E')
        except Exception as e:

            print(str(e))
        
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("update janasena_nri_donations set passport_url=%s where transaction_id=%s",(passport_url,transaction_id))
        con.commit()
        con.close()



    except Exception as e:
        print(str(e))
        return ""
@celery_app.task
def upload_visa_file(stream, filename, name, content_length, content_type, headers,transaction_id):
    try:
        container = 'nripassports'
       
        
        visa_url=''
        
        visa_file = FileStorage(stream=StringIO(stream), filename=filename, name=name, content_type=content_type, content_length=content_length)
        filename = secure_filename(visa_file.filename)
        fileextension = filename.rsplit('.', 1)[1]
        Randomfilename = id_generator()
        filename = Randomfilename + '.' + fileextension
        try:
            block_blob_service.create_blob_from_stream(container, filename, visa_file)
            visa_url = 'https://janasenabackup.blob.core.windows.net/' + container + '/' + filename
            visa_url=encrypt_decrypt(visa_url,'E')
        except Exception as e:

            print(str(e))
        
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)

        cur.execute("update janasena_nri_donations set visa_url=%s where transaction_id=%s",(visa_url,transaction_id))
        con.commit()
        con.close()

    except Exception as e:
        print(str(e))
        return ""
# @celery_app.task
# def upload_donation_files(passport_file, visa_file, transaction_id):
#     try:
#         container = 'nripassports'
#         # status=block_blob_service.create_container(container, metadata={'category':'files'}, public_access='blob', fail_on_exist=True,timeout=300)
#         # print status
#         # print  '/////////////////////////////////'
#         passport_url=''
#         visa_url=''
#         if passport_file:
#             filename = secure_filename(passport_file.filename)
#             fileextension = filename.rsplit('.', 1)[1]
#             Randomfilename = id_generator()
#             filename = Randomfilename + '.' + fileextension
#             try:
#                 block_blob_service.create_blob_from_stream(container, filename, passport_file)
#                 passport_url = 'https://janasenabackup.blob.core.windows.net/' + container + '/' + filename
                
#             except Exception as e:

#                 print(str(e))
#         if visa_file:
#             filename = secure_filename(visa_file.filename)
#             fileextension = filename.rsplit('.', 1)[1]
#             Randomfilename = id_generator()
#             filename = Randomfilename + '.' + fileextension
#             try:
#                 block_blob_service.create_blob_from_stream(container, filename, visa_file)
#                 visa_url = 'https://janasenabackup.blob.core.windows.net/' + container + '/' + filename
                
#             except Exception as e:

#                 print(str(e))  
#         con = psycopg2.connect(config.DB_CONNECTION)
#         cur = con.cursor(cursor_factory=RealDictCursor)
#         cur.execute("update janasena_nri_donations set passport_url=%s,visa_url=%s where transaction_id=%s",(passport_url,visa_url,transaction_id))
#         con.commit()
#         con.close()



#     except Exception as e:
#         print(str(e))
#         return ""

@celery_app.task
def send_donation_remainder_sms():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select distinct on (phone)phone,donar_name from janasena_donations where monthly_remainder='yes' and payment_status='S' ")
        rows=cur.fetchall()
        for row in rows:
            phone="91"+str(row['phone'])
            name=str(row['donar_name'])
        
        
            message="Dear "+name+", Thank you for your donation. JanaSena Party is committed to build a better society. We request your continued support to achieve our goals - Pawan Kalyan. tinyurl.com/y8qt586y"    
            
            sendSMS(phone,message)
            


    except Exception as e:
        print(str(e))

@celery_app.task
def send_missedcall_sms():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select phone as mobile,supporter_id from janasena_missedcalls where supporter_id not in (select membership_id from janasena_membership) and member_through='M' and phone like '%91%' order by date(create_dttm) desc limit 100")
        rows=cur.fetchall()
        placeholders=['supporter_id']
        message=""
        send_bulk_sms_operations(rows, message, placeholders, [])
    except Exception as e:
        print(str(e))





@celery_app.task
def sendVoiceSMS(numberToSend, msgToSend):
    try:

        url = "http://voiceapi.smscountry.com/api"

        querystring = {"api_key": "fXvgwWyK5zTB1k9F2POg", "access_key": "KYCajyY5GPjNvkT1L0SZ9s67sFYqlTEgqbyd25Wd",
                       "xml": "<request action='http://janasenaparty.org/voicecallstatus' method='POST'><to>" + numberToSend + "</to><play>" + msgToSend + "</play></request>"}

        headers = {}

        response = requests.request("GET", url, headers=headers, params=querystring)

    except Exception as e:
        print(str(e))

@celery_app.task
def send_bulk_email_operations(contacts, body, subject, placeholders, keys):
    try:
        temp_message = body
        name = "JanaSena Party"
        if len(placeholders):
            for placeholder in placeholders:
                temp_message = temp_message.replace("{{" + placeholder + "}}", r"%recipient." + str(placeholder) + "%")
                subject = subject.replace("{{" + placeholder + "}}", r"%recipient." + str(placeholder) + "%")

        if len(contacts) <= 1000:
            if len(placeholders):
                emails_object = {}

                for row in contacts:
                    to = str(row['email'])
                    if to is not None and to != '':
                        emails_object[to] = {}

                        for placeholder in placeholders:
                            emails_object[to][placeholder] = row[placeholder]
                emails = emails_object.keys()
                email_message=temp_message
                email_message = open("./notifications/email_template.html").read()
                email_message = email_message.replace("{{email_body}}", temp_message)
                if len(emails):
                    
                    sendbulkemails(name, emails, email_message, subject, emails_object)
            else:
                emails = []

                for row in contacts:
                    if str(row['email']) is not None and str(row['email']) != '':
                        emails.append(str(row['email']))
                email_message = open("./notifications/email_template.html").read()
                email_message = email_message.replace("{{email_body}}", temp_message)

                if len(emails):
                    
                    sendbulkemails(name, emails, email_message, subject, None)


        else:
            size = 1000
            modified_contacts = [contacts[i:i + size] for i in range(0, len(contacts), size)]
            for chunk in modified_contacts:

                if len(placeholders):
                    emails_object = {}

                    for row in chunk:
                        to = str(row['email'])

                        if to is not None and to != '':
                            emails_object[to] = {}

                            for placeholder in placeholders:
                                emails_object[to][placeholder] = row[placeholder]
                    email_message = open("./notifications/email_template.html").read()
                    email_message = email_message.replace("{{email_body}}", temp_message)
                    email_message=temp_message
                    emails = emails_object.keys()
                    if len(emails):
                        
                        sendbulkemails(name, emails, email_message, subject, emails_object)
                else:
                    emails = []

                    for row in chunk:
                        if str(row['email']) is not None and str(row['email']) != '':
                            emails.append(str(row['email']))
                    email_message = open("./notifications/email_template.html").read()
                    email_message = email_message.replace("{{email_body}}", temp_message)
                    email_message=temp_message
                    if len(emails):
                        
                        sendbulkemails(name, emails, email_message, subject, None)

    except Exception as e:
        print(str(e))
@celery_app.task
def send_bulk_sms_operations(contacts, message, placeholders, keys):
    try:
        print(len(contact))
        mno_message = ''
        keys = contacts[0].keys()
        if 'mobile' in keys:
            mobile = 'mobile'
        else:
            mobile = 'phone'
        if len(placeholders):
            if len(contacts)>10:
                size = 10
                modified_contacts = [contacts[i:i + size] for i in range(0, len(contacts), size)]
                for chunk in modified_contacts:
                    mno_message=''
                    for contact in chunk:
                        temp_message=message
                        for placeholder in placeholders:
                            temp_message = temp_message.replace("{{" + placeholder + "}}", str(contact[placeholder]))
                        if len(str(contact[mobile])) == 10:

                            numberToSend = "91" + str(contact[mobile])
                        else:
                            numberToSend = str(contact[mobile])

                        msgToSend = temp_message

                        mno_message += numberToSend + '^' + msgToSend + '~'
                    if mno_message != '':
                        mno_msg = mno_message.encode("utf-8")
                        # print mno_msg
                        sendBULKSMS(mno_msg)

                                
            else:

                for contact in contacts:

                    temp_message = message

                    for placeholder in placeholders:
                        temp_message = temp_message.replace("{{" + placeholder + "}}", str(contact[placeholder]))
                    if len(str(contact[mobile])) == 10:

                        numberToSend = "91" + str(contact[mobile])
                    else:
                        numberToSend = str(contact[mobile])

                    msgToSend = temp_message

                    mno_message += numberToSend + '^' + msgToSend + '~'
                if mno_message != '':
                    mno_msg = mno_message.encode("utf-8")
                    # print mno_msg
                    sendBULKSMS(mno_msg)
        else:

            m_nos = []
            for contact in contacts:
                if len(str(contact[mobile])) == 10:

                    m_nos.append("91" + str(contact[mobile]))
                elif len(str(contact[mobile])) == 12:

                    m_nos.append(str(contact[mobile]))
            u_mnos = list(set(m_nos))
            # print len(u_mnos)
            u_mnos.append('919676881991')
            if len(u_mnos) <= 1:
                
                sendBULKSMS_samemessage(message, u_mnos)
               ## send_bulk_sms_same_message(message,u_mnos)
                
            else:
                # print '/////////'
                size = 5

                modified_contacts = [u_mnos[i:i + size] for i in range(0, len(u_mnos), size)]
                for chunk in modified_contacts:
                    # print chunk
                    sendBULKSMS_samemessage(message, chunk,language='unicode')
                    #send_bulk_sms_same_message(message, chunk)

    except Exception as e:
        print(str(e))

@celery_app.task
def send_magazine_subscription_sms(data):
    transaction_id = data['transaction_id']
    numberToSend = data['phone']
    countries_allowed = ["91"]
    if numberToSend.startswith(tuple(countries_allowed)):
        msgToSend = "Hi "+str(data["name"])+",Thank you for subscribing Shatagni Magazine. your transaction id is:"+str(transaction_id)+" - Team JSP"
        sendSMS(numberToSend, msgToSend)
    if data['email'] is not None and data['email']!='':
        template="./static/shatagni_magazine_email.html"
        email_message=open(template).read()
        temp_text = str(email_message)
        temp_text = temp_text.replace("{{name}}", str(data['name']))
        temp_text = temp_text.replace("{{transaction_id}}", str(data['transaction_id']))
        if data['subscription_plan']==1:
            temp_text = temp_text.replace("{{plan}}", "1 Year")
        else:
            temp_text = temp_text.replace("{{plan}}", "3 Years")

        temp_text = temp_text.replace("{{quantity}}", str(data['quantity']))
        email=data["email"]
        #attachments=[subs_receipt(data)]
        subject="Shatagni Magazine Subscription Details"
        sendmailmandrill("JanaSena Party", email, temp_text, subject,attachments=[])



@celery_app.task
def send_membership_sms(data):
    jsp_membership_id = data['membership_id']
    numberToSend = data['phone_number']

    countries_allowed = ["91"]
    if numberToSend.startswith(tuple(countries_allowed)):
        # print "india"
        pagelink = "https://janasenaparty.org/getMembershipCard?id=" + str(jsp_membership_id)

        membershipcard_short_link = shurl(pagelink)

        msgToSend = "Hello , Your Janasena Party membership ID : " + str(
            jsp_membership_id) + " . click here to Download e-membership card " + str(membershipcard_short_link)

        sendSMS(numberToSend, msgToSend)


@celery_app.task
def send_instructionsmail_queue(email, membership_id):
    try:

        # pagelink="getMembershipCard?id="+str(membership_id)
        # membership_card_link=generateLink(str(membership_id),pagelink,'https://janasenaparty.org/',"&checksum")

        # membershipcard_short_link=shurl(membership_card_link)
        membership_card_link = "https://janasenaparty.org/getMembershipCard?id=" + str(membership_id)
        membershipcard_short_link = "<a href=" + str(membership_card_link) + ">CLick here</a>"
        if email != '':
            subject = " Janasena Party | Congratulations "
            name = "JANASENA PARTY"
            message = open("./instructions_template.html").read()
            temp_text = str(message)
            temp_text = temp_text.replace("{{membership_id}}", str(membership_id))
            temp_text = temp_text.replace("{{membership_card_link}}", str(membershipcard_short_link))

            sendmailmandrill(name, email, temp_text, subject)


    except Exception as e:
        print(str(e))
        # print "Exception occured in "

@celery_app.task
def jspMissedcallTask(phone_number, receivedon):
    try:

        existing = db.session.query(Janasena_Missedcalls.jsp_supporter_seq_id).filter(
            Janasena_Missedcalls.phone == phone_number).first()

        if existing is None:
            newRecord = Janasena_Missedcalls(phone=phone_number,
                                             member_through='M')
            db.session.add(newRecord)
            db.session.commit()

            countries_allowed = ["91", "1"]
            if phone_number.startswith(tuple(countries_allowed)):
                member_id = newRecord.jsp_supporter_seq_id

                membership_id = "JSP" + str(member_id).zfill(8)
                membership_link = "https://goo.gl/vwqBxR"

                data = {}
                data['phone_number'] = phone_number

                data['message_type'] = "missed_call"
                msgtoSend = "Thank you for becoming JanaSainik! Your Membership ID is: " + str(
                    membership_id) + ". Jai Hind- Pawan Kalyan. Get your e-membership card, from here " + membership_link
                data['msgToSend'] = msgtoSend

                sendSMS(phone_number, msgtoSend)

    except (exc.SQLAlchemyError, Exception) as e:
        print(str(e))
def currencyInIndiaFormat(n):

    try:
        n = str(n).split('.')[0]
        n = str(n)
        amount_length = len(n)
        if amount_length < 6:
            import locale
            n = int(n)
            locale.setlocale(locale.LC_ALL, '')
            cn = locale.format("%d", n, grouping=True)
            cn = str(cn) + ".00"
            return cn

        elif amount_length == 6:
            amount = ''

            amount = amount + n[0] + ',' + n[1:3] + "," + n[3:6]
            amount = str(amount) + ".00"
            return amount
        elif amount_length == 7:
            amount = ''

            amount = amount + n[0:2] + ',' + n[2:4] + "," + n[4:7]
            amount = str(amount) + ".00"
            return amount
        elif amount_length == 8:
            amount = ''
            amount = amount + n[0:1] + ',' + n[1:3] + "," + n[3:5] + "," + n[5:8]
            amount = str(amount) + ".00"
            return amount
        elif amount_length == 9:
            amount = ''
            amount = amount + n[0:2] + ',' + n[2:4] + "," + n[4:6] + "," + n[6:9]
            amount = str(amount) + ".00"
            return amount

        else:

            return str(n) + ".00"

    except Exception as e:
        print(str(e))
        return n

@celery_app.task
def senddonationsemail(data):
    message = open("static/thanks_note.html").read()

    try:

        email = data['email']
        mobile = data['phone']
        donar_name = data['donar_name']
        date = data['date']
        try:
            data['transaction_amount']=currencyInIndiaFormat(data['transaction_amount'])
        except Exception as e:
            print(str(e))
            pass
        amount = str(data['transaction_amount'])
        receipt_id = data['transaction_id']
        email_message = message
        sms_text = "Dear {{donar_name}},JanaSena Party will be grateful to people like you who make our work and mission possible through your support. We received with thanks the sum of Rs.{{amount}}"
        sms_text = sms_text.replace("{{donar_name}}", str(donar_name))
        msgToSend = sms_text.replace("{{amount}}", str(amount))
        if 'country' in data.keys():
            numberToSend =  str(mobile)
            sendSMS(numberToSend, msgToSend)
            
            attachments = []
            donation_receipt = get_nri_pdf_receipt(data)
            if donation_receipt:
                attachments.append(donation_receipt)
        else:
            numberToSend = "91"+str(mobile)
            sendSMS(numberToSend, msgToSend)
            
            attachments = []
            donation_receipt = get_pdf_receipt(data)
            if donation_receipt:
                attachments.append(donation_receipt)

        if email != '':
            name = "JANASENA PARTY"
            subject = "Thank you for donating to JANASENA PARTY"
            email_message = email_message.replace("{{donar_name}}", str(donar_name))
            email_message = email_message.replace("{{amount}}", str(amount))
            email_message = email_message.replace("{{receipt_id}}", str(receipt_id))
            email_message = email_message.replace("{{date}}", str(date))

            sendmailmandrill(name, email, email_message, subject, attachments=attachments)

    except Exception as e:
        print(str(e))

@celery_app.task
def send_recurring_payment_activation_email(details):
    try:
        # print details
        email=details['email']
        mobile=details['phone']
        plan_id=details['plan_id']
        plan_amount=details['plan_amount']
        donar_name=details['donar_name']
        plan_period=details['plan_period']
        subscription_id=details['subscription_id']
        cancel_link="https://nivedika.janasenaparty.org/cancel_subscription?pid="+str(plan_id)+"&sid="+str(subscription_id)
        body=open("notifications/email_template.html").read()
        sms_text = "Dear {{donar_name}},Thanks for activating the recurring payments by making a 5Rs payment. This amount will be refunded back to you soon,and your recurring payment amount {{plan_amount}}Rs will be charged {{plan_period}}. JanaSena Party"
        sms_text = sms_text.replace("{{donar_name}}", str(donar_name))
        sms_text = sms_text.replace("{{plan_period}}", str(plan_period))
        msgToSend = sms_text.replace("{{plan_amount}}", str(plan_amount))
        
        numberToSend = "91"+str(mobile)
        sendSMS(numberToSend, msgToSend)
            
        temp_text="""<p style='color: rgb(16, 16, 17);font-size: 18px;font-family:
         sans-serif;'>Dear {{name}}, </p><p style='font-size: 17px;font-family: sans-serif;
          font-weight: 500;'>Thanks for activating the recurring payments by making 
          a  5Rs payment. This amount will be refunded back to you soon,
           and your recurring payment amount {{plan_amount}}Rs
           will be charged {{plan_period}}.
            An email will be triggered to you every time your card is charged.</p>
             <p style='font-size: 17px;font-family: sans-serif;font-weight: 500;'>
             Donations from supporters and sympathisers help Janasena Party to run its operations and plan for future initiatives. Your on-going contributions help to build a political party that is working to bring transparency in government for the common man </p>
             <p>if you want to cancel your recurring donations,  <a href='{{cancel_link}}'>click here </a> 
             <p>Spread the news to fellow
         Janasainiks and party sympathizers by sharing the below links:</p>"""
        temp_text=temp_text.replace("{{name}}",donar_name)
        temp_text=temp_text.replace("{{plan_amount}}",str(plan_amount))
        temp_text=temp_text.replace("{{plan_period}}",plan_period)
        temp_text=temp_text.replace("{{cancel_link}}",cancel_link)
        body=body.replace("{{email_body}}",temp_text)
        
        subject=" Your "+str(plan_period)+" Recurring Donation Activated successfully "
        sendmailmandrill("JANASENA PARTY", email, str(body), subject)
    except Exception as e:
        print(str(e))
@celery_app.task
def send_recurring_donations_ack(data):
    message = open("notifications/email_template.html").read()
    # print "sending email"
    try:
        email=data['email']
        mobile=data['phone']
        plan_id=data['plan_id']
        plan_amount=data['plan_amount']
        donar_name=str(data['donar_name'])+" "+str(data["last_name"])
        plan_period=data['plan_period']
        subscription_id=data['subscription_id']
        cancel_link="https://nivedika.janasenaparty.org/cancel_subscription?pid="+str(plan_id)+"&sid="+str(subscription_id)
        
        date = str(data['date'])
        receipt_id=data["payment_id"]
        try:
            data['transaction_amount']=currencyInIndiaFormat(data['plan_amount'])
        except Exception as e:
            print(str(e))
            pass
        amount = str(data['transaction_amount'])
        
        temp_text="""<p>Dear {{donar_name}},
        </p><p>Thank you for donating to Janasena Party 
        and being part of the movement to bring Fair Politics & 
        Strong Civil Society.</p>
        <p>Please consider this letter as a receipt for your {{plan_period}} contribution of Rs.{{amount}}/- .
         Donation Receipt Number {{receipt_id}} on Date {{date}}.</p>
         <p>Thanks for Donating to Janasena Party.</p>
          <p>if you want to cancel your recurring donations,  <a href='{{cancel_link}}'>click here </a> 
         <p>Best Regards, </p><p>Janasena Party</p> 
          <p>1. The Registration number of the Party is '56/118/2014/PPS-I.'
           and the PAN number is AACAJ2057M. </p>
           <p>2. Donations to JSP, except those in cash, 
           are exempt under Income Tax Act 1961, u/s 80GGB/80GGC. </p>
           <p>4. For any queries regarding donation email us
            at donations@janasenaparty.org.</p>"""
        message=message.replace("{{email_body}}",temp_text)
        email_message = message
        sms_text = "Dear {{donar_name}} , We received your {{plan_period}} recurring payment of Rs.{{amount}}. JanaSena Party will be grateful to people like you who make our work and mission possible through your support. "
        sms_text = sms_text.replace("{{donar_name}}", str(donar_name))
        sms_text = sms_text.replace("{{plan_period}}", str(plan_period))
        msgToSend = sms_text.replace("{{amount}}", str(plan_amount))

        
        numberToSend = "91"+str(mobile)
        sendSMS(numberToSend, msgToSend)
        details={}
        details['donar_name']=donar_name
        details['transaction_id']=receipt_id
        details['transaction_amount']=plan_amount
        details['email']=email
        details['phone']=mobile
        details['address']=data['address']
        details['state']=data['state']
        details['payment_method']="Recurring Payment"
        details['date']=str(date).split(".")[0]
        attachments = []
        donation_receipt = get_pdf_receipt(details)
        if donation_receipt:
            attachments.append(donation_receipt)

        if email != '':
            name = "JANASENA PARTY"
            subject = "Thank you for donating to JANASENA PARTY"
            email_message = email_message.replace("{{donar_name}}", str(donar_name))
            email_message = email_message.replace("{{amount}}", str(plan_amount))
            email_message = email_message.replace("{{receipt_id}}", str(receipt_id))
            email_message = email_message.replace("{{date}}", str(date))
            email_message = email_message.replace("{{plan_period}}", str(plan_period))
            email_message = email_message.replace("{{cancel_link}}", str(cancel_link))

            sendmailmandrill(name, email, email_message, subject, attachments=attachments)
            
    except Exception as e:
        print(str(e))
@celery_app.task
def recurring_donations_remainder():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select concat(donar_name,' ',last_name) as name,payment_id,phone,subscription_id,subscription_next_due,plan_amount,plan_period from janasena_recurring_payments where subscription_status='authenticated' and  subscription_next_due between current_date+1 and current_date+2;")
        rows=cur.fetchall()
        # print rows
        sms_text = "Dear {{donar_name}}, your JanaSena Party {{plan_period}} recurring donation of Rs.{{amount}}.00 will be deducted from your {{card_name}} ending with {{last_four_digits}} on {{next_due}}. Please keep sufficient balance. Thanks -JanaSena Party "
        for row in rows:
            donar_name=row['name']
            mobile=row['phone']
            next_due=str(row['subscription_next_due'])
            plan_period=row['plan_period']
            plan_amount=row['plan_amount']
            payment_id=row['payment_id']
            card_details=get_card_details(payment_id)
            # print card_details
            # print type(card_details)
            if card_details is not None:
                card_details=json.loads(card_details)
                last_four_digits=card_details["last4"]
                try:
                    card_name=card_details["issuer"]+" "+card_details["type"]+" "+card_details["entity"]
                except Exception as e:
                    card_name=" "

                sms_text = sms_text.replace("{{donar_name}}", str(donar_name))
                sms_text = sms_text.replace("{{plan_period}}", str(plan_period))
                sms_text = sms_text.replace("{{next_due}}", str(next_due))
                sms_text = sms_text.replace("{{last_four_digits}}", str(last_four_digits))
                sms_text = sms_text.replace("{{card_name}}", str(card_name))
                msgToSend = sms_text.replace("{{amount}}", str(plan_amount))

                numberToSend = "91"+str(mobile)
                sendSMS(numberToSend, msgToSend)
            
        con.close()

    except Exception as e:
        print(str(e))

@celery_app.task
def send_subscription_cancel_email(details):
    try:
        body=open("notifications/email_template.html").read()
        temp_text="""<p style='color: rgb(16, 16, 17);font-size: 18px;font-family:
         sans-serif;'>Dear {{name}}, </p><p style='font-size: 17px;font-family: sans-serif;
          font-weight: 500;'>Sorry to know that you want to cancel your {{plan_period}} recurring donation of Rs. {{plan_amount}} to Janasena Party. Your donation will be stopped immediately and you will be no longer charged.
In future, if you want to subscribe again for the recurring donations,<a href="https://janasenaparty.org/donations">  click here </a> To continue to get updates on Janasena Party operations, please subscribe to the following links:</p>"""
        temp_text=temp_text.replace("{{name}}",details["donar_name"])
        temp_text=temp_text.replace("{{plan_amount}}",str(details["plan_amount"]))
        temp_text=temp_text.replace("{{plan_period}}",str(details["plan_period"]))
        body=body.replace("{{email_body}}",temp_text)
        subject=" JANASENA Recurring Donation cancelled successfully "
        sendmailmandrill("JANASENA PARTY", details["email"], str(body), subject)
    except Exception as e:
        print(str(e))


@celery_app.task
def senddonationsreceipt(data):
    message = open("static/thanks_note.html").read()

    try:

        email = data['email']
        mobile = data['phone']
        donar_name = data['donar_name']
        date = data['date']
        try:
            data['transaction_amount']=currencyInIndiaFormat(data['transaction_amount'])
        except Exception as e:
            print(str(e))
            pass
        amount = str(data['transaction_amount'])
        receipt_id = data['transaction_id']
        email_message = message
        sms_text = "Dear {{donar_name}}, we have sent the Donation Receipt to your email. JanaSena Party"
        sms_text = sms_text.replace("{{donar_name}}", str(donar_name))
        msgToSend = sms_text.replace("{{amount}}", str(amount))
        if 'country' in data.keys():
            numberToSend =  str(mobile)
            sendSMS(numberToSend, msgToSend)
            
            attachments = []
            donation_receipt = get_nri_pdf_receipt(data)
            if donation_receipt:
                attachments.append(donation_receipt)
        else:
            numberToSend = "91"+str(mobile)
            sendSMS(numberToSend, msgToSend)
            
            attachments = []
            donation_receipt = get_pdf_receipt(data)
            if donation_receipt:
                attachments.append(donation_receipt)

        if email != '':
            name = "JANASENA PARTY"
            subject = "Thank you for donating to JANASENA PARTY"
            email_message = email_message.replace("{{donar_name}}", str(donar_name))
            email_message = email_message.replace("{{amount}}", str(amount))
            email_message = email_message.replace("{{receipt_id}}", str(receipt_id))
            email_message = email_message.replace("{{date}}", str(date))

            sendmailmandrill(name, email, email_message, subject, attachments=attachments)

    except Exception as e:
        print(str(e))
  

@celery_app.task
def sendSMS(numberToSend, msgToSend):
    try:
        url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx"

        querystring = {"User": config.sms_country_user, "passwd": config.sms_country_password,
                       "mobilenumber": numberToSend, "message": msgToSend, "sid": "JAIJSP", "mtype": "LNG", "DR": "Y"}
        headers = {}

        response = requests.request("GET", url, headers=headers, params=querystring)
        # print response
        return response
    except Exception as e:
        print(str(e))


@celery_app.task
def send_bulk_sms_excel(contacts, message, placeholders, keys):
    # print "celery task"
    
    mno_message=''
    if len(contacts) <= 1:
        for contact in contacts:
            temp_message=message
            for key in contact.keys():
                if key in placeholders:
                    temp_message = temp_message.replace("{{" + key + "}}", str(contact[key]))
            mno_message += contact['mobile_number'] + '^' + temp_message + '~'
        
        mno_msg = mno_message.encode("utf-8")
        sendBULKSMS(mno_msg)
        # print mno_msg
        
    else:
        size = 1
        modified_contacts = [contacts[i:i + size] for i in range(0, len(contacts), size)]
        for chunk in modified_contacts:
            mno_message=''
            for contact in chunk:
                # print contact
                temp_message=message
                for key in contact.keys():
                    if key in placeholders:

                        temp_message = temp_message.replace("{{" + key + "}}", str(contact[key]))
                mno_message += contact['mobile_number'] + '^' + temp_message + '~'
            mno_msg = mno_message.encode("utf-8")
            # print mno_msg
            sendBULKSMS(mno_msg)


@celery_app.task
def send_bulk_sms_manual(contacts, message):
    mno_message = ''
    message =message.encode('utf-8')
    mno_s=[]
    for contact in contacts:
        if len(str(contact)) == 10:
            contact = "91" + str(contact)
        mno_s.append(contact)

    if len(mno_s):

        sendBULKSMS_samemessage(message,mno_s)
@celery_app.task
def update_manual_voter_count():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select voter_id,state_id,constituency_id from janasena_membership where membership_through='ME'")
        rows = cur.fetchall()
        data=[]
        count=0
        for row in rows:
            count=count+1
            # print count
            e_voter_id=row['voter_id']
            voter_id=encrypt_decrypt(e_voter_id,'D')
            ac_no=row['constituency_id']
            state=row['state_id']
            from utilities.classes import Voter_details
            voterdetails_instance=Voter_details()
            if voterdetails_instance.check_existence(voter_id):

                record = voterdetails_instance.get_voter_details(voter_id)

                state_id = record['state']
                constituency_id = record['ac_no']
                relation_type = record['relation_type']
                relationship_name = record['relation_name']
                polling_station_id = record['part_no']
                polling_station_name = record['section_name']
                address = record['house_no']

                membership_through = 'MU'

                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor(cursor_factory=RealDictCursor)
                cur.execute(
                    "select district_id from assembly_constituencies where state_id =%s and constituency_id=%s",
                    (state_id, constituency_id))
                row = cur.fetchone()
                if row is not None:
                    district_id = row['district_id']
                else:
                    district_id = 0
                try:
                    cur.execute(
                        "update janasena_membership set relation_type=%s,relationship_name=%s,address=%s,polling_station_id=%s,polling_station_name=%s,constituency_id=%s,district_id=%s,state_id=%s,membership_through=%s where voter_id=%s",
                        ( relation_type, relationship_name, address, polling_station_id,
                         polling_station_name, constituency_id, district_id, state_id, membership_through,
                          e_voter_id))


                    con.commit()
                except Exception as e:
                    print(str(e))

            else:
                d={}
                d['voter_id']=voter_id
                d['AC NO']=ac_no
                d['State']=state
                data.append(d)
        import json
        with open('./static/json/missed_voters.json', 'w') as outfile:
            json.dump(data, outfile)


    except Exception as e:
        print(str(e))
@celery_app.task
def update_country_task():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select phone,membership_id from janasena_membership where country is null ")
        rows = cur.fetchall()
        ind_count = 0
        usa_count = 0
        for row in rows:
            phone = row['phone']
            membership_id = row['membership_id']
            mobile = encrypt_decrypt(phone, 'D')
            countries_allowed = ["91"]

            if mobile.startswith(tuple(countries_allowed)):
                ind_count = ind_count + 1
                # print "ind-" + str(ind_count)
                cur.execute("update janasena_membership set country='india' where membership_id=%s ", (membership_id,))
                con.commit()
            else:
                usa_count = usa_count + 1
                # print  "ind-" + str(usa_count)
                cur.execute("update janasena_membership set country='nri' where membership_id=%s ", (membership_id,))
                con.commit()
        con.close()


    except Exception as e:
        print(str(e))

from utilities.general import get_payment_details,get_payment_request_details
import json

@celery_app.task(name="donations_check_task")
def donations_check_task():
    try:
        logger.info("executing the donations check task")
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)

        cur.execute("""select order_id from janasena_donations where payment_status='I' and date(update_dttm  AT TIME ZONE 'Asia/Calcutta' ) = current_date -1  and transaction_through='IM' order by create_dttm desc""")
        rows = cur.fetchall()
        # print len(rows)
        for row in rows:
            order_id=row['order_id']
            # print order_id
            response = get_payment_request_details(order_id)
            
            if response is not None:
                response = json.loads(response)
                
                payment_info=response['payment_request']['payments']
                if len(payment_info):
                    for i in range(len(payment_info)):
                        status = payment_info[i]['status']
                        # print status

                        if status == "Credit":
                            # print "hellooooo"
                            payment_id=payment_info[0]['payment_id']
                            payment_method = payment_info[0]["instrument_type"]
                            transaction_payment = payment_info[0]["amount"]
                            # print payment_id,payment_method,transaction_payment
                            cur.execute(
                                "update janasena_donations set payment_id=%s,payment_status=%s,payment_method=%s,transaction_payment=%s where order_id=%s returning seq_id,donar_name,relation_name,email,phone,address,transaction_id,transaction_payment as transaction_amount,to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD Mon YYYY') as date,state,payment_method ",
                                (payment_id, 'S', payment_method, transaction_payment, order_id))
                            details = cur.fetchone()

                            con.commit()
                            if details is not None:
                                payment = details
                                senddonationsemail(payment)
                            break
                            
            else:
                print('no response'   )
        
    except Exception as e:
        print(str(e))



@celery_app.task
def check_all_insta_mojo_transactions():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)

        cur.execute("""select order_id from janasena_donations where payment_status='I' and date(update_dttm  AT TIME ZONE 'Asia/Calcutta' )=current_date-1 and transaction_through='IM' order by create_dttm desc""")
        rows = cur.fetchall()
        # print len(rows)
        for row in rows:
            order_id=row['order_id']
            # print order_id
            response = get_payment_request_details(order_id)
            
            if response is not None:
                response = json.loads(response)
                
                payment_info=response['payment_request']['payments']
                if len(payment_info):
                    for i in range(len(payment_info)):
                        status = payment_info[i]['status']
                        # print status

                        if status == "Credit":
                            # print "hellooooo"
                            payment_id=payment_info[0]['payment_id']
                            payment_method = payment_info[0]["instrument_type"]
                            transaction_payment = payment_info[0]["amount"]
                            # print payment_id,payment_method,transaction_payment
                            cur.execute(
                                "update janasena_donations set payment_id=%s,payment_status=%s,payment_method=%s,transaction_payment=%s where order_id=%s returning seq_id,donar_name,relation_name,email,phone,address,transaction_id,transaction_payment as transaction_amount,to_char(update_dttm AT TIME ZONE 'Asia/Calcutta','DD Mon YYYY') as date,state,payment_method ",
                                (payment_id, 'S', payment_method, transaction_payment, order_id))
                            details = cur.fetchone()

                            con.commit()
                            if details is not None:
                                payment = details
                                senddonationsemail(payment)
                            break
                            
            else:
                print('no response'   )
        
    except Exception as e:
        print(str(e))


@celery_app.task(name="supporter_id_update")
def supporter_id_update():
    try:
        logger.info("executing periodic task supporter_id_update")
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)

        cur.execute("""update janasena_missedcalls set supporter_id='JSP'||lpad(cast(jsp_supporter_seq_id as text),8,'0') where supporter_id is null""")
        con.commit()
        cur.execute("update janasena_missedcalls set member_status='A' where member_status is null")
        con.commit()
        cur.execute("update missedcall_status_counts set count=(select count(*) as expired from janasena_missedcalls jm left join janasena_membership m on m.membership_id=jm.supporter_id where m.membership_id is null and jm.member_through in ('M','C') and jm.member_status ='E') where flag='E';")
        con.commit()
        cur.execute("update missedcall_status_counts set count=(select count(*)  from janasena_missedcalls jm left join janasena_membership m on m.membership_id=jm.supporter_id where m.membership_id is null and jm.member_through in ('M','C') and jm.member_status ='OS') where flag='OS';")
        con.commit()
        cur.execute("update missedcall_status_counts set count=(select count(*)  from janasena_missedcalls jm left join janasena_membership m on m.membership_id=jm.supporter_id where m.membership_id is null and jm.member_through in ('M','C') and jm.member_status ='NA') where flag='NA';")
        con.commit()
        cur.execute("update missedcall_status_counts set count=(select count(*) as to_be_converted from janasena_missedcalls jm left join janasena_membership m on m.membership_id=jm.supporter_id where m.membership_id is null and jm.member_through in ('M','C') and jm.member_status ='A') where flag='T';")
        con.commit()
        
        con.close()

    except psycopg2.Error as e:
        print(str(e))

@celery_app.task
def jspMissedcallTask2(phone_number):
    try:

        existing2 = db.session.query(Janasena_Missedcalls.jsp_supporter_seq_id).filter(
            Janasena_Missedcalls.phone == phone_number).first()
        if existing2 is None:
            
            newRecord = Janasena_Missedcalls(phone=phone_number,
                                              member_through='MI')

            db.session.add(newRecord)
            db.session.commit()
            countries_allowed = ["91"]
            if phone_number.startswith(tuple(countries_allowed)):
                member_id = newRecord.jsp_supporter_seq_id
                

                membership_id = "JSP" + str(member_id).zfill(8)
                membership_link = "https://goo.gl/vwqBxR"

                
                msgtoSend = "Thank you for becoming JanaSainik! Your Membership ID is: " + str(
                    membership_id) + ". Jai Hind- Pawan Kalyan. Get your e-membership card, from here " + membership_link
                

                sendSMS(phone_number, msgtoSend)

        else:
            member_id=existing2.jsp_supporter_seq_id
            membership_id = "JSP" + str(member_id).zfill(8)
            # check membership table
            membership_instance = Membership_details()
            details = membership_instance.get_membership_details(membership_id, None)
            if details is not None:
                msgtoSend = "Thank you for supporting Janasena"
            else:
                membership_link = "https://goo.gl/vwqBxR"

                
                msgtoSend = "Thank you for becoming JanaSainik! Your Membership ID is: " + str(
                    membership_id) + ". Jai Hind- Pawan Kalyan. Get your e-membership card, from here " + membership_link
                

            sendSMS(phone_number, msgtoSend)

    except (exc.SQLAlchemyError, Exception) as e:
        print(str(e))

from werkzeug.datastructures import FileStorage

@celery_app.task
def upload_supporters_data(stream, filename, name, content_length, content_type, headers,visitor_id,supporter):
    try:
        container = 'meetyourpresident'
       
        supporter_file = FileStorage(stream=StringIO(stream), filename=filename, name=name, content_type=content_type, content_length=content_length)
        filename = secure_filename(supporter_file.filename)
        fileextension = filename.rsplit('.', 1)[1]
        Randomfilename = id_generator()
        filename = Randomfilename + '.' + fileextension
        supporter_url=''
        try:
            block_blob_service.create_blob_from_stream(container, filename, supporter_file)
            supporter_url = 'https://janasenabackup.blob.core.windows.net/' + container + '/' + filename
            
        except Exception as e:

            print(str(e))
        
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        name=supporter['name']
        phone=supporter['phone']
        membership_status=supporter['membership_status']
        age=supporter['age']
        gender=supporter['gender']
        membership_id=supporter['memebershiP_id']
        cur.execute("""insert into visitor_supporting_persons
            (visitor_id,supporter_name,
supporter_phone,supporter_photo,supporter_membership_id,supporter_membership_status,supporter_gender,supporter_age)values(%s,%s,%s,%s,%s,%s,%s,%s)"""
        ,(visitor_id,name,phone,supporter_url,membership_id,membership_status,gender,age))
        con.commit()
        con.close()



    except Exception as e:
        print(str(e))
        return ""


@celery_app.task
def upload_mrpresident_file(stream, filename, name, content_length, content_type, headers,visitor_id):
    try:
        container = 'meetyourpresident'
       
        support_file = FileStorage(stream=StringIO(stream), filename=filename, name=name, content_type=content_type, content_length=content_length)
        filename = secure_filename(support_file.filename)
        fileextension = filename.rsplit('.', 1)[1]
        Randomfilename = id_generator()
        filename = Randomfilename + '.' + fileextension
        support_url=''
        try:
            block_blob_service.create_blob_from_stream(container, filename, support_file)
            support_url = 'https://janasenabackup.blob.core.windows.net/' + container + '/' + filename
            
        except Exception as e:

            print(str(e))
        
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("""insert into visitor_supporting_documents(visitor_id,document_url,document_type)values(%s,%s,%s)""",(visitor_id,support_url,fileextension))
        con.commit()
        con.close()
    except Exception as e:
        print(str(e))
        return ""

