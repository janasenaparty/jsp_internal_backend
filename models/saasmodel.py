from   appholder import db
import sqlalchemy
import logging
import datetime
from   sqlalchemy    import and_, or_
from   dateutil    import parser as dparser
import six
import decimal
from   copy import deepcopy
from   sqlalchemy import inspect
import simplejson as json

import enum

try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict

logger = logging.getLogger(__name__)


def validatecriteria(criteria, mapper=None):
    if criteria is None:
        return {
            "max-results":    25,
            "results-offset": 0,
            "original": {
                "max-results": 25,
                "results-offset": 0
            }
        }
    #We limit the default max return count to 25.
    maxResults    = criteria.get('max-results', None)
    offsetResults = criteria.get('results-offset', 0)
    
    if maxResults is not None and maxResults < 0:
        maxResults = None
    #default maxResults to 25 is not set.
    maxResults = maxResults or 25
    if offsetResults is not None and offsetResults < 0:
        offsetResults = None
    
    conditions      = criteria.get('rules', [])
    links = criteria.get('condition', [])
    condition_links =[]
    if not isinstance(links,list):
            condition_links.append(links)
    if len(conditions) > 1 and len(condition_links) == 0:
        condition_links.append('and')
        #raise Exception("Multiple conditions in the Criteria but no condition links between them")
    filters = []
    conditionDict = OrderedDict({})
    for condition in conditions:
        condName = condition.get('field', None)
        condAttr = condition.get('field', None)
        if mapper is not None:
            condAttr = mapper.get_alias(condAttr)
        condType = condition.get('operator', "in")
        condValues =[]
        value = condition.get('value', [])
        if not isinstance(value,list):
            condValues.append(value)
        else:
            condValues = value
        """
        if len(condValues) == 0:
            raise Exception("Values field is empty for condition {}".format(condName))
        """
        if condType == "is-null":
            conditionDict[condName] = (condAttr, "eq", None)
        elif condType == "is-not-null":
            conditionDict[condName] = (condAttr, "ne", None)
        elif condType == "like":
            conditionDict[condName] = (condAttr, "like", condValues[0])
        elif condType == "not-like":
            conditionDict[condName] = (condAttr, "not-like", condValues[0])
        elif condType == "contains":
            conditionDict[condName] = (condAttr, "like", condValues[0])
        elif condType == "search":
            conditionDict[condName] = (None, "search", condValues[0])
        elif condType == "not-contains":
            conditionDict[condName] = (condAttr, "not-like", condValues[0])
        elif condType == "equal" or condType=="=":
            conditionDict[condName] = (condAttr, "eq", condValues[0])
        elif condType == "not-equal":
            conditionDict[condName] = (condAttr, "ne", condValues[0])
        elif condType == "in":
            conditionDict[condName] = (condAttr, "in", condValues)
        elif condType == "not-in":
            conditionDict[condName] = (condAttr, "not-in", condValues)
        elif condType == "lesser-than":
            conditionDict[condName] = (condAttr, "lt", condValues[0])
        elif condType == "lesser-or-equal":
            conditionDict[condName] = (condAttr, "le", condValues[0])
        elif condType == "greater-than":
            conditionDict[condName] = (condAttr, "gt", condValues[0])
        elif condType == "greater-or-equal":
            conditionDict[condName] = (condAttr, "ge", condValues[0])
        elif condType == "starts-with":
            conditionDict[condName] = (condAttr, "starts-with", condValues[0])
        elif condType == "not-starts-with":
            conditionDict[condName] = (condAttr, "not-starts-with", condValues[0])
        elif condType == "ends-with":
            conditionDict[condName] = (condAttr, "ends-with", condValues[0])
        elif condType == "not-ends-with":
            conditionDict[condName] = (condAttr, "not-ends-with", condValues[0])
        elif condType == "between":
            conditionDict[condName] = (condAttr, "between", condValues)
        elif condType == "not-between":
            conditionDict[condName] = (condAttr, "not-between", condValues)
        else:
            msg = "Operand is not supported: '{}'".format(condType)
            raise Exception(msg)
    for key in conditionDict:
        filters.append(conditionDict[key])
    filters.append("and")
    # if len(condition_links):
    #     links = condition_links[0].get('link')
    #     for a in links.split():
    #         a = a.strip()
    #         if a in list(conditionDict.keys()):
    #             filters.append(conditionDict[a])
    #         elif a.lower() == "and":
    #             filters.append("and")
    #         elif a.lower() == "or":
    #             filters.append("or")
    #         else:
    #             logger.error("Unknown condition/operator '%s' in links" % (a))
    #             raise Exception("Unknown condition/operator '%s' in links" % (a))
    # else:
    #     # condition links is not specified. So only pick the last condition.
    #     for key in conditionDict:
    #         filters.append(conditionDict[key])
    
    orders = criteria.get('order-by', [])
    for order in orders:
        if order.get('attribute', None) and order.get('order', 'asc'):
            if order.get('order', 'asc') not in ['asc', 'desc']:
                msg = "Order by is not supported: {}".format(order.get('order', 'asc'))
                raise Exception(msg)
    
    return {"filters":        filters,
            "max-results":    maxResults,
            "results-offset": offsetResults,
            "orders":         orders,
            "is_timebased":   False,
            "original":       criteria}
    
def to_float(val):
    if val is None:
        return None
    try:
        return float(val)
    except:
        raise


def to_text(val):
    if val is None:
        return None
    try:
        return str(val)
    except:
        raise


def to_date(val):
    if val is None:
        return None
    if isinstance(val, datetime.datetime):
        return val
    try:
        val_float = float(val)
        if val_float < 0:
            return None
        if val_float > 253402300800000000:  # Time in nanoseconds
            return datetime.datetime.utcfromtimestamp(val_float / 1000000000.0).replace(tzinfo=None)
        elif val_float > 253402300800000:  # Time in microseconds
            return datetime.datetime.utcfromtimestamp(val_float / 1000000.0).replace(tzinfo=None)
        elif val_float > 253402300800:  # Time in milliseconds
            return datetime.datetime.utcfromtimestamp(val_float / 1000.0).replace(tzinfo=None)
        elif val_float > 4223371680:  # Time in seconds.
            return datetime.datetime.utcfromtimestamp(val_float).replace(tzinfo=None)
        elif val_float > 70389528:  # Time in minutes.
            return datetime.datetime.utcfromtimestamp(val_float * 60).replace(tzinfo=None)
        elif val_float > 2932897:  # Time in Hours.
            return datetime.datetime.utcfromtimestamp(val_float * 60 * 60).replace(tzinfo=None)
        elif val_float > 418985:  # Time in days.
            return datetime.datetime.utcfromtimestamp(val_float * 24 * 60 * 60).replace(tzinfo=None)
    except:
        try:
            return dparser.parse(val).replace(tzinfo=None)
        except:
            return None


def convert_value(value, column):
    # Auto detect types and do the necessary value conversions.
    if isinstance(value, list):
        if issubclass(column.type.python_type, (datetime.datetime)):
            value = [to_date(_val) for _val in value]
        if issubclass(column.type.python_type, (int, float, decimal.Decimal)):
            value = [to_float(_val) for _val in value]
        if issubclass(column.type.python_type, six.string_types):
            value = [to_text(_val) for _val in value]
    else:
        if issubclass(column.type.python_type, (datetime.datetime)):
            value = to_date(value)
        if issubclass(column.type.python_type, (int, float, decimal.Decimal)):
            value = to_float(value)
        if issubclass(column.type.python_type, six.string_types):
            value = to_text(value)
    return value

def build(table, filter_condition):
    if isinstance(filter_condition, list):
        filter_condition = filter_condition[0]
    try:
        key, op, value = filter_condition
    except ValueError:
        raise Exception('Invalid filter spec: %s' % filter_condition)
    logger.info("Key: {}, Op: {}, Value: {}".format(key, op, value))
    if op == 'search':
        search_query = None
        for column in table.c:
            search_query = or_(search_query, column.contains(value)) if search_query is not None else column.contains(value)
        return search_query

    column = getattr(table.c, key, None)
    if column is None:
        raise Exception("Invalid/non-existent table column '{}' for filter.".format(key))

    value = convert_value(value, column)
    
    if op == 'in':
        return column.in_(value) if isinstance(value, list) else column.in_(value.split(','))
    
    if op == 'not-in':
        return column.notin_(value) if isinstance(value, list) else column.in_(value.split(','))
    
    if op == 'eq':
        return column.__eq__(value)
    
    if op == 'ne':
        return column.__ne__(value)
    
    if op == 'le':
        return column.__le__(value)
    
    if op == 'lt':
        return column.__lt__(value)
    
    if op == 'ge':
        return column.__ge__(value)
    
    if op == 'gt':
        return column.__gt__(value)
    
    if op == 'between':
        value = value if isinstance(value, list) else value.split(',')
        if len(value) < 2:
            raise Exception("Need 2 values for between query")
        return column.between(value[0], value[1])
    
    if op == 'not-between':
        value = value if isinstance(value, list) else value.split(',')
        if len(value) < 2:
            raise Exception("Need 2 values for not-between query")
        return sqlalchemy.not_(column.between(value[0], value[1]))
    
    if op == 'starts-with':
        return column.startswith(value)
    
    if op == 'ends-with':
        return column.endswith(value)
    
    if op == 'not-starts-with':
        return sqlalchemy.not_(column.startswith(value))
    
    if op == 'not-ends-with':
        return sqlalchemy.not_(column.endswith(value))
    
    if op == 'like':
        return column.ilike(value)
    
    if op == 'contains':
        return column.contains(value)
    
    if op == 'not-like':
        return sqlalchemy.not_(column.contains(value))
    
    if op == 'not-contains':
        return sqlalchemy.not_(column.contains(value))
    
    raise Exception("Operation '{}' is not supported.".format(op))


def applyFilters(query, tblname, filters):
    operator = query_string = None
    for filter in filters:
        if isinstance(filter, (tuple, list)):
            query_string = operator(query_string,
                                    build(tblname, filter)) if operator is not None else build(tblname,filter)
            operator = and_
        else:
            operator = or_ if filter == "or" else and_
    return query.filter(query_string) if query_string is not None else query


def asdict(item):
    dict_ = {}
    for key in inspect(item).mapper.c.keys():
        value = getattr(item, key)
        if isinstance(value, enum.Enum):
            value = value.name
        if isinstance(value, datetime.datetime):
            value = int(value.timestamp() * 1000)
        dict_[key] = value
    return item.addComputedColumns(dict_)


def asuidict(item):
    ui_fields = getattr(type(item),'UI_FIELDS',
                        inspect(item).mapper.c.keys())
    dict_ = {}
    for key in inspect(item).mapper.c.keys():
        if key in ui_fields:
            value = getattr(item, key)
            if isinstance(value, enum.Enum):
                value = value.name
            if isinstance(value, datetime.datetime):
                value = int(value.timestamp() * 1000)
            dict_[key] = value
    return item.addComputedColumns(dict_)


class SAASModel():
    def asdict(self):
        dict_ = {}
        for key in self.__mapper__.c.keys():
            value = getattr(self, key)
            if isinstance(value, enum.Enum):
                value = value.name
            if isinstance(value, datetime.datetime):
                value = int(value.timestamp() * 1000)
            dict_[key] = value
        return self.addComputedColumns(dict_)

    def asuidict(self):
        ui_fields = getattr(type(self),'UI_FIELDS',
                            self.__mapper__.c.keys())
        ui_exclude_fields = getattr(type(self),'UI_EXCLUDE_FIELDS', [])

        dict_ = {}
        for key in self.__mapper__.c.keys():
            if key in ui_exclude_fields:
                continue
                
            if key in ui_fields:
                value = getattr(self, key)
                if isinstance(value, enum.Enum):
                    value = value.name
                if isinstance(value, datetime.datetime):
                    value = int(value.timestamp() * 1000)
                dict_[key] = value
        return self.addComputedColumns(dict_)

    @classmethod
    def criteria(cls, criteria, subquery=None, return_objects=False):
        criteria = validatecriteria(criteria)
        print(criteria)
        max     = criteria.get('max-results', None)
        offset  = criteria.get('results-offset') or 0
        filters = criteria.get('filters', [])
        orders  = criteria.get('orders', [])
        nextBatchCriteria = None
        
        #subquery is used to do pre filter of some data if needed.
        #For example when we want to support criteria for auditlogs, the
        #criteria should always act on filterd auditlogs based on workspace.
        query = applyFilters(cls.query if subquery is None else subquery,
                             cls.__table__,
                             filters)
        total = query.count()
        for order in orders:
            if order.get('order') == 'desc':
                query = query.order_by(getattr(cls.__table__.c,
                                               order.get('attribute')).desc())
            else:
                query = query.order_by(getattr(cls.__table__.c,
                                               order.get('attribute')))
        # if max is not set we get everything.
        if max is not None:
            query = query.limit(max)
        if offset is not None:
            query = query.offset(offset)
    
        if return_objects:
            result_dict = [u for u in query.yield_per(250)]
        else:
            result_dict = [asuidict(u) for u in query.yield_per(250)]
            
        if offset + len(result_dict) < total:
            nextBatchCriteria = deepcopy(criteria.get('original', {}))
            nextBatchCriteria['results-offset'] = offset + len(result_dict)
        return {
            "sourceCriteria":    criteria.get('original', {}),
            "nextBatchCriteria": nextBatchCriteria,
            "results":           result_dict,
            "hasMoreResults":    True if offset + len(result_dict) < total else False,
            "maxResults":        max,
            "totalResults":      total
        }
    def addComputedColumns(self, dict_):
        return dict_


class CFXMapper(object):
    @staticmethod
    def isWorkspaceAdmin(usergroups):
        if 'WORKSPACE_ADMINISTRATORS' in usergroups.split(','):
            return True
        return False

    def isPortalAdmin(usergroups):
        if 'PORTAL_ADMINISTRATORS' in usergroups.split(','):
            return True
        return False


