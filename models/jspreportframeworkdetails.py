
from appholder import db
from .saasmodel import SAASModel
import logging
from sqlalchemy import *
logger = logging.getLogger(__name__)


class JSPReportFrameworkDetails(SAASModel,db.Model):	
    __table__ = db.Model.metadata.tables['jsp_report_framework_details']   
    
    def __repr__(self):
        return '<jsp_report_framework_details %r>' % self.id


