from appholder import db
from .saasmodel import SAASModel
import logging

logger = logging.getLogger(__name__)


class Districts(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['districts']    
    
    def __repr__(self):
        return '<Districts {}-{}>'.format(self.seq_id,
                                            self.district_id)
        
    