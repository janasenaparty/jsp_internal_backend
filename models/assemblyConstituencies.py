from appholder import db
from .saasmodel import SAASModel
import logging

logger = logging.getLogger(__name__)


class AssemblyConstituencies(SAASModel, db.Model):
    __table__ = db.Model.metadata.tables['assembly_constituencies']    
    
    def __repr__(self):
        return '<assembly_constituencies {}-{}>'.format(self.seq_id,
                                            self.constituency_id)
        
    