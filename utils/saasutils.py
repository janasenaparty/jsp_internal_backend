
# python2/3 Compatibility
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import (bytes, dict, int, list, object, range, str,
                      ascii, chr, hex, input, next, oct, open,
                      pow, round, super, filter, map, zip)
# End of Python2/3 Compatibility
import simplejson as json
import os
import six
import collections
import time
import re
import copy
import uuid
import flask 
import logging
from functools import wraps
import datetime
from yaml import load, dump, Loader
import logging.config
import tldextract
import string
import random
from datetime import timedelta
from ast import literal_eval
from functools import wraps, update_wrapper
from flask import url_for, request, session, make_response, current_app, render_template

logger = logging.getLogger(__name__)

IsString = lambda a: True if a is not None and isinstance(a, six.string_types) else False
IsList   = lambda a: True if isinstance(a, collections.Sequence) and not isinstance(a, six.string_types) else False
MakeList = lambda a: a if IsList(a) else [item.strip() for item in a.split(',')]
IsDict   = lambda a: True if isinstance(a, collections.Mapping) else False
GetJSON       = lambda a: json.loads(a) if a is not None and IsString(a) else a
GetJSONString = lambda a: json.dumps(a) if not IsString(a) else a
GetPrettyJSON = lambda a: json.dumps(a, indent=4, default=str) if not IsString(a) else a
GetList  = lambda a: a if IsList(a) else a.split(',')
IsNone   = lambda a: True if a is None else False
NotNone  = lambda a: True if a is not None else False
IsEmpty  = lambda a: True if a is None or len(a if IsList(a) or IsDict(a) else str(a)) == 0 else False
NotEmpty = lambda a: True if a is not None and len(a if IsList(a) or IsDict(a) else str(a)) > 0 else False
TRUE  = lambda a: bool(a) if a is not None else False
FALSE = lambda a: not bool(a) if a is not None else False
MillisecondTimestamp = lambda: int(time.time() * 1000)
SecondsTimestamp     = lambda: int(time.time())
UniqueId = lambda: str(uuid.uuid4())
IN    = lambda a, b: True if NotNone(a) and NotNone(b) and (a in b) else False
NOTIN = lambda a, b: True if NotNone(a) and NotNone(b) and (a not in b) else False
GE    = lambda a, b: True if NotNone(a) and NotNone(b) and (a >= b) else False
GT    = lambda a, b: True if NotNone(a) and NotNone(b) and (a > b) else False
LE    = lambda a, b: True if NotNone(a) and NotNone(b) and (a <= b) else False
LT    = lambda a, b: True if NotNone(a) and NotNone(b) and (a < b) else False
Contains = lambda a, b: True if NotNone(a) and NotNone(b) and IsString(a) and IsString(b) and a.find(b) != -1 else False
Search   = lambda a, b: True if NotNone(a) and NotNone(b) and IsString(a) and IsString(b) and re.search(a, b) is not None \
    else \
    False
ListCopy = lambda a: copy.deepcopy(a) if a is not None and IsList(a) else None
DictCopy = lambda a: copy.deepcopy(a) if a is not None and IsDict(a) else None



# This is primarily used for UI.


def GetCurrentSession():
    session = flask.session.get('jsp_session', {})   
    
    return session



def generate_random_salt(length):
    char_types = string.ascii_letters + string.digits
    return ''.join(random.choice(char_types) for i in range(length))

# Request
# www.curry.devfabrix.com - should be changed to curry.devfabrix.com
# and this would become your workspace.
def GetRequestHost():
    extract   = tldextract.extract(flask.request.host)
    subdomain = extract.subdomain.replace('www.', '')
    if len(subdomain):
        return "{}.{}.{}".format(subdomain,
                                 extract.domain,
                                 extract.suffix)
    else:
        return "{}.{}".format(extract.domain,
                              extract.suffix)


"""
This assumes that folks would accessing workspace domain like below



"""




def GetRequestDomain():
    extract = tldextract.extract(flask.request.host)
    return "{}.{}".format(extract.domain,
                          extract.suffix)


"""
Suppress www from the subdomain.
"""


def GetRequestSubDomain():
    extract = tldextract.extract(flask.request.host)
    return extract.subdomain.replace('www.', '')


GetRequestPostData = lambda: flask.request.get_json()
GetRequestURL = lambda: flask.request.host_url.rstrip('/')
GetRequestScheme = lambda: flask.request.scheme
GetRequestArgs = lambda: flask.request.args
GetRequestWorkspace = lambda: GetRequestHost()


def SendResponse(code=200, error=None, result=None):
    message = {
        "serviceError": error,
        "serviceResult": result,
        "serviceName": "Janasena Party"
    }
    resp = flask.jsonify(message)
    resp.status_code = code
    return resp




class UIGenericError(Exception):
    status_code = 200
    
    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload
    
    def to_dict(self):
        rv = {
            "serviceError": self.message,
            "serviceResult": self.payload,
            "serviceName": "Janasena Portal"
        }
        return rv


"""
UI Expects errors as part of the below response object with Rest Code 200.
"""


class PostDataMissing(Exception):
    status_code = 200
    
    def __init__(self, message="POST data missing in the request.", status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload
    
    def to_dict(self):
        rv = {
            "serviceError": self.message,
            "serviceResult": self.payload,
            "serviceName": "Janasena Portal"
        }
        return rv


"""
UI Expects errors as part of the below response object with Rest Code 200.
"""


class ArgumentsMissing(Exception):
    status_code = 200
    
    def __init__(self, message="Arguments missing in the request.", status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload
    
    def to_dict(self):
        rv = {
            "serviceError": self.message,
            "serviceResult": self.payload,
            "serviceName": "Janasena Portal"
        }
        return rv


"""
"""


class SystemGenericError(Exception):
    status_code = 500
    
    def __init__(self,
                 message="System Error. Please contact tech@janasenaparty.org",
                 status_code=None,
                 payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload
    
    def to_dict(self):
        rv = {
            "serviceError": "System Error. Please contact tech@janasenaparty.org",
            "serviceResult": self.payload,
            "serviceName": "Janasena Portal"
        }
        return rv


class PermissionDenied(Exception):
    status_code = 401
    def __init__(self, payload=None):
        Exception.__init__(self)
        self.message = "User Login required. Permission denied."
        self.payload = payload
    
    def to_dict(self):
        rv = {
            "serviceError": self.message,
            "serviceResult": self.payload,
            "serviceName": "Janasena Portal"
        }
        return rv







def initialize_logging(default_level=logging.INFO):
    LogConfigFile = os.path.join(os.path.dirname(os.path.realpath(__file__)), "../logging.yaml")

    if os.path.exists(LogConfigFile):
        try:
            with open(LogConfigFile, 'rt') as f:
                config = load(f.read(), Loader=Loader)
            
            logging.config.dictConfig(config)
            logging.info("Initialized logging framework using: {}".format(LogConfigFile))
        except Exception as ex:
            logging.basicConfig(level=default_level)
    else:
        logging.basicConfig(level=default_level)


def getConfigurationFile():
    
    ServicesConfigFile = os.path.join("./config.json")
    
    if not os.path.exists(ServicesConfigFile):
        logger.error("Portal Configuration {} is missing.".format(ServicesConfigFile))
        raise Exception("Portal Configuration {} is missing.".format(ServicesConfigFile))
    
    logger.info("Portal Configuration: {}".format(ServicesConfigFile))
    return ServicesConfigFile



def GetURLDomain(url):
    extract = tldextract.extract(url)
    return "{}.{}".format(extract.domain,
                          extract.suffix)


def GetURLSubDomain(url):
    extract = tldextract.extract(url)
    return extract.subdomain.replace('www.', '')
def getServiceConfig(serviceconfigfile):
    config = {}
    with open(serviceconfigfile) as json_file:
        config = json_file.read()
    
    return config

from six import string_types
def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    
    
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, string_types):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, string_types):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            
            if automatic_options and flask.request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()

            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and flask.request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)

    return decorator
