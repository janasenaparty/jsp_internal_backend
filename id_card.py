#!/usr/local/bin/python
# coding: utf-8
import os
import tempfile
from copy import deepcopy



from reportlab.graphics import renderPDF
from reportlab.graphics.barcode import qr
from reportlab.graphics.shapes import Drawing
from reportlab.lib.colors import red, HexColor, blue, white
from reportlab.lib.enums import TA_CENTER
from reportlab.lib.pagesizes import letter, landscape, A4
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen.canvas import Canvas
import numpy as np
from reportlab.platypus import SimpleDocTemplate, Image, Paragraph

from PIL import Image as Img
#from wand.image import Image as imgw

def id_card_func(user_name,membership_id):
    # print "generating card"
    # font = r"fonts/BebasNeue-Bold.ttf"
    # pdfmetrics.registerFont(TTFont("BebasNeue", font))
    handle, pdffilepath = tempfile.mkstemp(suffix='.pdf')
    c = Canvas(pdffilepath,pagesize=landscape(A4))
    c.setFillColor(white)
    c.rect(0,0,A4[1],A4[0], fill=True, stroke=False)

    front = Image('static/img/membership/offline/front.jpg', 12*cm, 17*cm)
    front.drawOn(c,40,70)
    front = Image('static/img/membership/offline/back.jpg', 12*cm, 17*cm)
    front.drawOn(c,120+10*cm,70)

    styleSheet = getSampleStyleSheet()
    nameStyle = deepcopy(styleSheet['BodyText'])
    idStyle = deepcopy(styleSheet['BodyText'])

    nameStyle.alignment = TA_CENTER
    nameStyle.wordWrap ='LTR' 

    nameStyle.fontSize = 18
    # nameStyle.fontName = 'BebasNeue'
    nameStyle.leading = 20 * 1


    idStyle.alignment = TA_CENTER
    idStyle.textColor = red
    idStyle.fontSize = 20
    # idStyle.fontName = 'BebasNeue'

    # membership_id = "JSP18533077"
    name = Paragraph("<b>"+user_name+"</b>",nameStyle)
    jspid = Paragraph('<b>'+membership_id+'</b>',idStyle)

    name.wrapOn(c,260,50)
    name.drawOn(c,80,A4[0]/2)
    jspid.wrapOn(c,A4[1]/5,A4[0]/5)
    jspid.drawOn(c,130,A4[0]/2-20)

    url = "https://janasenaparty.org/getQRCodeDetails?id="+membership_id
    qr_code = qr.QrCodeWidget(url)
    bounds = qr_code.getBounds()
    width = bounds[2] - bounds[0]
    height = bounds[3] - bounds[1]
    d = Drawing(90, 90,transform=[90./width,0,0,90./height,0,0])
    d.add(qr_code)
    renderPDF.draw(d, c, A4[1]/2+110, 410)
    # d = Drawing(45, 45, transform=[45./width,0,0,45./height,0,0])

    c.save()
    # sleep(20)
    file = pdffilepath
    handle, to = tempfile.mkstemp(suffix='.png')


    p = imgw()
    # p.density('300')
    p.read(filename=os.path.abspath(file))

    p.save(filename=os.path.abspath(to))

    w_file = open(to, 'r')
    file_size = len(w_file.read())
    return to
# with imgw(filename=r'C:\Users\janasena\PycharmProjects\do-func/f1.pdf') as img:
#     img.save(filename="temp.jpg")
