

FROM jsptech/jspdocker:ubuntu-20.04
#Maintaned by
MAINTAINER Janasena

ENV TZ=Asia/Kolkata \
        DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install tzdata


RUN     apt-get install -y wget && \
        mkdir -p /opt/python && \
        wget https://www.python.org/ftp/python/3.7.4/Python-3.7.4.tgz -O /tmp/Python-3.7.4.tgz && \
        cd /opt/python/ && \
        tar -xf /tmp/Python-3.7.4.tgz && \
        rm -rf /tmp/Python-3.7.4.tgz

RUN     wget http://archive.ubuntu.com/ubuntu/pool/main/l/linux/linux-libc-dev_5.4.0-97.110_amd64.deb && \
        apt-get update && \
        apt-get install -y linux-libc-dev

RUN     apt-get install -y nginx libxmlsec1-dev  build-essential zlib1g-dev  python-is-python3 \
        libncurses5-dev \
        libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev vim curl && \
        cd /opt/python/Python-3.7.4 && \
        ./configure --enable-optimizations --prefix=/usr --enable-shared LDFLAGS="-Wl,-rpath /usr/lib" && \
        make -j8 build_all && \
        make -j8 install && \
        cd /opt && \
        rm -rf /opt/python

RUN    apt-get -y install libpq-dev gcc && \
       pip3 install psycopg2   && \
       pip3 install uwsgi==2.0.18    
	


COPY requirements.txt /opt/portal_app/requirements.txt
WORKDIR /opt/portal_app
RUN   python3 -m pip install --upgrade pip && pip3 install -r requirements.txt


RUN     mkdir -p           /opt
ADD     portalui.tar.gz    /opt/portal_ui

#Copy the portal app source code.
COPY    . /opt/portal_app
#Below is the volume for the portal.log file. THis volume needs to be mounted to the disk for host
#persistence.
VOLUME  /var/log/jspportal
##

COPY    nginx.conf /etc/nginx/nginx.conf
COPY    supervisord.conf /etc/supervisord.conf

VOLUME /var/log/nginx
RUN    mkdir -p /var/log/nginx && \
       touch /var/log/nginx/access.log && \
       touch /var/log/nginx/error.log

ADD     certs              /etc/nginx/certs
EXPOSE 80


CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]