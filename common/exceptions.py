__author__      = "Rambabu Vucha"
__copyright__   = "Copyright 2021, JanaSena Party."
__maintainer__  =  "Rambabu Vucha"

import argparse as ap

class CFXPortalException(Exception):
    def __init__(self, message, code):
        self.message = message
        self.code    = code

codes = {
         'AUTH0_USER_EXISTS': ap.Namespace(**{
                      'code': 200,
                      'message': "User already exists with provided email. Please choose another email."
                      }),
         'SERVICE_NOT_FOUND': ap.Namespace(**{
                      'code': 200,
                      'message': "Requested service is not available for provisioning."
                      }),
         'WORKSPACE_SERVICEINSTANCE_EXISTS': ap.Namespace(**{
                      'code': 200,
                      'message': "Service is already provisioned for the workspace."
                      }),
         'WORKSPACE_SERVICEINSTANCE_NOT_EXISTS': ap.Namespace(**{
                      'code': 200,
                      'message': "Service is not provisioned for the workspace."
                      }),
         'WORKSPACE_ALREADY_EXISTS': ap.Namespace(**{
                      'code': 200,
                      'message': "Workspace Domain already exists. Please choose another domain."
                      }),
         'WORKSPACE_DOESNT_EXIST': ap.Namespace(**{
                      'code': 200,
                      'message': "Workspace Domain doesnt exist. Please contact support team."
                      }),
         'WORKSPACE_DOMAIN_NOT_VALID': ap.Namespace(**{
                      'code': 200,
                      'message': "Workspace name is not valid. Name can only be alpha numeric, length between 2 to 32 characters, cannot start with a number."
                      }),
         'POST_DATA_MISSING': ap.Namespace(**{
                      'code': 200,
                      'message': "HTTP POST request data missing."
                      }),
         'API_NOT_IMPLEMENTED': ap.Namespace(**{
                      'code': 200,
                      'message': "API not implemented."
                      }),
         'AUTH0_USER_DOESNT_EXIST': ap.Namespace(**{
                      'code': 200,
                      'message': "User doesnt exist."
                      }),
         'GENERIC_ERROR': ap.Namespace(**{
                      'code': 200,
                      'message': "System Error. Please contact customer support."
                      }),
         'SUCCESS': ap.Namespace(**{
                      'code': 200,
                      'message': {'success': True}
                      }),
        'WORKSPACE_AVAILABLE': ap.Namespace(**{
            'code': 200,
            'message': {'available': True}
        }),
        'WORKSPACE_NOTAVAILABLE': ap.Namespace(**{
            'code': 200,
            'message': "Workspace is either part of the reserved name or is already being used."
        })
}
CFXPortalResponse = ap.Namespace(**codes)