#     For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user root;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$http_x_forwarded_for - $http_host [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$remote_addr"';

    access_log  /var/log/nginx/access.log  main;
    server_tokens           off;
    ignore_invalid_headers  on;
    server_name_in_redirect off;
    server_names_hash_bucket_size 256;
    server_names_hash_max_size    1024;
    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    types_hash_max_size 2048;
    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;
    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

    ##buffer policy
    client_body_buffer_size       2K;
    client_header_buffer_size     2k;
    client_max_body_size          1M;
    large_client_header_buffers 4 2k;
    ##end buffer policy

     ## Start: Timeouts ##
    client_body_timeout   10;
    client_header_timeout 10;
    keepalive_timeout     75 20;
    send_timeout          10;
    ## End: Timeouts ##
    connection_pool_size 256;
    request_pool_size 4k;

    gzip on;
    gzip_min_length 1100;
    gzip_buffers 4 8k;
    gzip_disable "msie6";
    gzip_types text/plain text/css application/json application/x-javascript text/xml
                application/xml application/xml+rss text/javascript application/pdf
                application/postscript application/rtf application/vnd.ms-powerpoint
                application/msword application/vnd.ms-excel application/vnd.wap.xhtml+xml;

    
    server {
        listen 80 ;
        server_name ~^www\.([a-zA-Z0-9_-]+)\.janasenaparty\.org$;
        return 301 https://$1.janasenaparty.org$request_uri;
    }
    server {
        listen 80 ;
        server_name ~^([a-zA-Z0-9_-]+)\.janasenaparty\.org$;
        return 301 https://$host$request_uri;
    }
     server {
        listen 443 ssl ;
        server_name ~^www\.([a-zA-Z0-9_-]+)\.janasenaparty\.org$;
        # SSL
        ssl_stapling             on;
        ssl_stapling_verify      on;
        ssl_certificate          /etc/nginx/certs/certificate.pem;        
        ssl_certificate_key      /etc/nginx/certs/private.key;
        ssl_protocols            TLSv1.2;
        ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';
        ssl_prefer_server_ciphers on;
        ssl_session_cache shared:SSL:10m;
        #End of SSL

        return 301 https://$1.janasenaparty.org$request_uri;
    }
   server {
        listen 443 ssl;
        server_name  ~^([a-zA-Z0-9_-]+)\.janasenaparty\.org$;
        # SSL
        ssl_stapling             on;
        ssl_stapling_verify      on;
        ssl_certificate          /etc/nginx/certs/certificate.pem;        
        ssl_certificate_key      /etc/nginx/certs/private.key;
        ssl_protocols            TLSv1.2;
        ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';
        ssl_prefer_server_ciphers on;
        ssl_session_cache shared:SSL:10m;
        #End of SSL
        include /etc/nginx/default.d/*.conf;
        #Additional headers for NGINX Security.
        #Inject HTTP Header with X-XSS protection to mitigate Cross-Site scripting attack
        add_header X-XSS-Protection "1; mode=block";
        add_header Allow "GET, POST, HEAD" always;
        #X-FRAME-OPTIONS in HTTP Header to prevent clickjacking attack
        add_header X-Frame-Options "SAMEORIGIN";
        #End of Headers
        #Strict Security Checks. Might need to relax based on need in future.
        #Limit Available Methods
        if ( $request_method !~ ^(GET|POST|HEAD)$ ) {
            return 405;
        }
        ## Block download agents ##
        if ($http_user_agent ~* LWP::Simple|BBBike|wget) {
            return 403;
        }
        ## Block some robots ##
        if ($http_user_agent ~* msnbot|scrapbot) {
            return 403;
        }
        ## Deny certain Referers ###
        if ( $http_referer ~* (babes|forsale|girl|jewelry|love|nudit|organic|poker|porn|sex|teen) )
        {
            return 403;
        }
        ##
        location /elb-status {
            return 200 'OK';
            add_header Content-Type text/plain;
        }
       
        location /admin {
            include uwsgi_params;
            uwsgi_pass 127.0.0.1:8080;
        }
        location / {
            root  /opt/portal_ui ;
            index index.html;
            try_files $uri $uri/ /index.html;
        }
       
        
    }
  
    server {
        listen      80 default_server;
        server_name _;

        #Additional headers for NGINX Security.
        #Inject HTTP Header with X-XSS protection to mitigate Cross-Site scripting attack
        add_header X-XSS-Protection "1; mode=block";
        add_header Allow "GET, POST, HEAD" always;
        #X-FRAME-OPTIONS in HTTP Header to prevent clickjacking attack
        add_header X-Frame-Options "SAMEORIGIN";
        #End of Headers
        #Strict Security Checks. Might need to relax based on need in future.
        #Limit Available Methods
        if ( $request_method !~ ^(GET|POST|HEAD|DELETE|PUT)$ ) {
            return 405;
        }
        ## Block download agents ##
        if ($http_user_agent ~* LWP::Simple|BBBike|wget) {
            return 403;
        }
        ## Block some robots ##
        if ($http_user_agent ~* msnbot|scrapbot) {
            return 403;
        }
        ## Deny certain Referers ###
        if ( $http_referer ~* (babes|forsale|girl|jewelry|love|nudit|organic|poker|porn|sex|teen) )
        {
            return 403;
        }
        ##
        ##
        location /elb-status {
            return 200 'OK';
            add_header Content-Type text/plain;
        }
        location /admin {
            include uwsgi_params;
            uwsgi_pass 127.0.0.1:8080;
        }
        location / {
            root  /opt/portal_ui ;
            index index.html;
            try_files $uri $uri/ /index.html;
        }
        
        
    }
}
