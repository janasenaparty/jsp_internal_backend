import flask_principal
import logging
from logging.handlers import RotatingFileHandler
import os

#
from appholder             import application as app
from appholder             import db, user_login_required
from saas.saasutils   import *
import uuid
import datetime

# protect a view with a principal for that need
@app.route('/portal/api/admin')
def do_admin_index():
	print("Here I am....{}".format(flask.session))
	return flask.Response('Only if you are an admin')

def SendResponse(error=None, result=None):
    message = {
        "serviceError":  error,
        "serviceResult": result,
        "serviceName":   "JanaSena Portal"
    }
    resp = flask.jsonify(message)
    resp.status_code = 200
    return resp


@app.route('/api/portal/isUserAuthenticated', methods=["POST", "GET"])
@user_login_required
def is_user_authenticated(profile):
    if IsNone(profile):
        return flask.jsonify({'authenticated': False})
    profile.update({'authenticated': True})
    return flask.jsonify(profile)

@app.route('/api/portal/getLoggedInUser', methods=["POST", "GET"])
@user_login_required
def get_user_profile(profile):
    if IsNone(profile):
        return flask.jsonify({'authenticated': False})
    profile.update({'authenticated': True})
    return flask.jsonify(profile)



@app.route('/api/portal/checkworkspace', methods=["POST"])
def do_workspace_exists(profile):
    requestData = flask.request.data
    name        = requestData.get('name',None)
    print("Checking for workspace: {}".format(name))
    workspace = Workspace.query.filter(Workspace.fulldomain == name).first()
    if IsNone(workspace):
        return flask.jsonify(None)
    else:
        print("Workspace Exists: {}".format(workspace))
    return flask.jsonify(workspace)
    
@app.route('/api/portal/createworkspace', methods=["POST"])
def create_workspace(profile):
    requestData = flask.request.data
    print(requestData)
    if IsEmpty(requestData):
        return SendResponse("Workspace details missing in the POST data")
    try:
        requestData = json.loads(requestData)
    except:
        return SendResponse("Invalid JSON Request")
    
    #check for mandatory arguments....
    workspace = Workspace.query.filter(Workspace.fulldomain == requestData.get('fulldomain')).first()
    if IsNone(workspace):
        workspace = Workspace(id=str(uuid.uuid4()),
                              created_at=datetime.datetime.now(),
                              created_by=requestData.pop('user'),
                              name="Workspace for {}".format(requestData.get('fulldomain')),
                              domain=requestData.get('fulldomain').split('.')[0],
                              **requestData)
        print("Creating workspace: {}".format(workspace.asdict()))
        db.session.add(workspace)
        db.session.commit()
        workspace = Workspace.query.filter(Workspace.fulldomain == workspace.fulldomain).first()
        print("created workspace: {}".format(workspace.asdict()))
    else:
        print(workspace.asdict())
        return SendResponse("Workspace {} already exists. Cannot be created.".format(workspace.fulldomain))
    return flask.jsonify(workspace.asdict())


@app.errorhandler(flask_principal.PermissionDenied)
def handle_permission_denied(error):
    deny = 'Permission Denied', 403
    if isinstance(flask.g.identity,
                  flask_principal.AnonymousIdentity):
        print("Permission Denied...Redirecting....")
        return flask.redirect(flask.url_for('login',
                                            next=flask.request.url))
    else:
        print("Permission Denied...Denied.......")
        return deny


@app.route('/api/', methods=['GET', 'POST'])
def index():
    errors        = []
    not_auth_warn = False
    success_slo   = False
    attributes    = False
    paint_logout  = False

    return flask.render_template(
        'index.html',
        errors=errors,
        not_auth_warn=not_auth_warn,
        success_slo=success_slo,
        attributes=attributes,
        paint_logout=paint_logout
    )

if __name__ == "__main__":
    formatter = logging.Formatter("%(asctime)-15s [%(name)-6s] %(levelname)-8s - %(message)s")
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    ch.setFormatter(formatter)
    if not os.path.isdir('logs'):
        os.makedirs('logs')
    handler = RotatingFileHandler('logs/mdr.log',
                                  maxBytes=1024,
                                  backupCount=5)
    handler.setLevel(logging.DEBUG)
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)
    app.logger.addHandler(ch)
    app.logger.setLevel(logging.DEBUG)
    app.run(host='0.0.0.0',
            port=80,
            debug=False)