import flask
import logging

#App Specific Imports
from utils.saasutils          import *
from common.exceptions        import CFXPortalResponse

from models.user              import User
from models.userdetails       import UserDetails
from models.districts         import Districts      
from models.assemblyConstituencies         import AssemblyConstituencies
from flask_jwt_extended       import create_access_token
from flask import render_template

from appholder import db
from datetime import datetime
logger = logging.getLogger(__name__)
users  = flask.Blueprint('kriyadashboard', __name__)

