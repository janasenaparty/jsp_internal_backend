from flask import request,Blueprint
import logging

#App Specific Imports
from utils.saasutils          import *
from common.exceptions        import CFXPortalResponse

from flask_jwt_extended       import create_access_token
from flask import render_template

from appholder import db
from datetime import datetime
from utils.saasutils import *

from models.kriyamembersdashboard import KriyaMembersDashboard
from models.jspreportframeworkdetails import JSPReportFrameworkDetails
logger = logging.getLogger(__name__)
report_builder  = Blueprint('reports', __name__)


def getDBModel(report_id):
    try:
        query = JSPReportFrameworkDetails.query.filter(JSPReportFrameworkDetails.id == report_id).first()   
        if query is not None:
            db_model =eval(query.db_model)      
            return eval(query.db_model)
        return None
    except Exception as ex:
        logger.error(str(ex))
        return None

def getTableMetadata(report_id):
    try:
        query = JSPReportFrameworkDetails.query.filter(JSPReportFrameworkDetails.id == report_id).first()   
        if query is not None:
            return GetJSON(query.table_metadata)
        return None
    except Exception as ex:
        logger.error(str(ex))
        return None

def getTableFilters(report_id):
    try:
        query = JSPReportFrameworkDetails.query.filter(JSPReportFrameworkDetails.id == report_id).first()   
        if query is not None:
            return GetJSON(query.filters)
        return None
    except Exception as ex:
        logger.error(str(ex))
        return None

@report_builder.route('/getFilters/<report_id>', methods=['POST','GET'])
@crossdomain(origin="*", headers="Content-Type")
#@admin_login_required
#@required_roles(["AD", "AC","PK"])

def get_table_filters(report_id):
    try:
        if report_id is not None:
            metadata = getTableFilters(report_id)
            return json.dumps({"errors": [], 'data': {'status': 1,"filtets":metadata}})
        return json.dumps({"errors": ['report_id missing'], 'data': {'status': 0}})
    except Exception as e:
        print(str(e))
        return json.dumps({"errors": ['Exception Occured'], 'data': {'status': 0}})


@report_builder.route('/getTablemetadata/<report_id>', methods=['POST','GET'])
@crossdomain(origin="*", headers="Content-Type")
#@admin_login_required
#@required_roles(["AD", "AC","PK"])

def get_table_metadata(report_id):
    try:
        if report_id is not None:
            metadata = getTableMetadata(report_id)
            return json.dumps({"errors": [], 'data': {'status': 1,"metadata":metadata}})
        return json.dumps({"errors": ['report_id missing'], 'data': {'status': 0}})
    except Exception as e:
        print(str(e))
        return json.dumps({"errors": ['Exception Occured'], 'data': {'status': 0}})



import datetime
@report_builder.route('/getDatabyquery/<report_id>', methods=['POST'])
#@crossdomain(origin="*", headers="Content-Type")
#@admin_login_required
#@required_roles(["AD", "AC","PK"])
def getDatabyquery(report_id):
    try:
        post_data =GetRequestPostData()
        print(post_data.get('rules'))
        results = getDBModel(report_id).criteria(post_data, None)
        print(results)
        return SendResponse(CFXPortalResponse.SUCCESS.code, None,  results)  
     
    except  Exception as e:    
        logger.exception(str(e))    
        return json.dumps({"errors": ['Exception Occured'], 'data': {'status': 0}})


@report_builder.route('/exportReport', methods=['POST','GET'])
#@crossdomain(origin="*", headers="Content-Type")
# @admin_login_required
# @required_roles(["AD", "AC","PK"])
def exportReport():
    try:
        filters = GetRequestPostData()
        report_id = filters.get('report_id',None)
        name= current_user.fname
        email = current_user.email
        if report_id is None:
            return json.dumps({"errors": ['report_id missing'], 'data': {'status': 0}})
        
        if config.ENVIRONMENT_VARIABLE != 'local':
            
            sendExportResultsToEmail.delay(name,email,filters,report_id)
        else:    
            
            sendExportResultsToEmail(name,email,filters,report_id)
        return json.dumps({"errors": [], "data": {"status": "1","message":"ecport data will be sent to your EmailID shortly"}})

    except (psycopg2.Error, Exception) as e:
        
        return json.dumps({"errors": ['Exception Occured'], 'data': {'status': 0}})


@report_builder.route('/updateReportMetadata', methods=['POST','GET'])
@crossdomain(origin="*", headers="Content-Type")
#@admin_login_required
#@required_roles(["AD", "AC","PK"])

def updateReportMetadata():
    try:
        metadata = GetRequestPostData()
        description = metadata.get('description',"")
        name= metadata.get('name',None)
        db_model = metadata.get('db_model',None)
        filters = GetJSONString(metadata.get('filters',{}))
        table_metadata = GetJSONString(metadata.get('table_metadata',{}))
        req_metadata = GetJSONString(metadata.get('metadata',{}))
        report_id = metadata.get("id",None)
        print(name,report_id,db_model)
        if id is None or name is None or db_model is None :            
            return SendResponse(CFXPortalResponse.SUCCESS.code, "postdata missing ",  None)
        existing = JSPReportFrameworkDetails.query.filter(JSPReportFrameworkDetails.db_model==db_model).first() 
        if existing is not None:  
            
            existing.description = description
            existing.name = name
            existing.filters = filters
            existing.table_metadata = table_metadata
            existing.metadata = req_metadata            
            db.session.commit()
            return SendResponse(CFXPortalResponse.SUCCESS.code, None,  "updated successfully")
        return SendResponse(CFXPortalResponse.SUCCESS.code, "report metadata not exist for this ",  None)
        
    except Exception as ex:
        print(str(ex))
        return json.dumps({"errors": ['Exception Occured'], 'data': {'status': 0}})


@report_builder.route('/generate_report_metadata/<table_name>', methods=['POST','GET'])
@crossdomain(origin="*", headers="Content-Type")
#@admin_login_required
#@required_roles(["AD", "AC","PK"])

def generate_report_metadata(table_name):
    try:

        columns = db.Model.metadata.tables[table_name].columns.keys()
        table_metadata = [{"field":column,"headerName":column} for column in columns]
        filters = generate_filters(table_name)
        existing = JSPReportFrameworkDetails.query.filter(JSPReportFrameworkDetails.db_model==db_model).first() 
        if existing is  None:   
            reportDetails = JSPReportFrameworkDetails(description="metadata of "+str(table_name),
                                    name=table_name,
                                    db_model=table_name,
                                    filters=filters,
                                    table_metadata=table_metadata                               
                                    
                                    )
            db.session.add(reportDetails)
            db.session.commit()
            return SendResponse(CFXPortalResponse.SUCCESS.code, None,  "added successfully")
        return SendResponse(CFXPortalResponse.SUCCESS.code, "report metadata already exists for this ",  None)
    except Exception as ex:
        print(str(ex))
        return json.dumps({"errors": [str(ex)], 'data': {'status': 0}}) 

    

@report_builder.route('/generate_table_metadata/<table_name>', methods=['POST','GET'])
@crossdomain(origin="*", headers="Content-Type")
#@admin_login_required
#@required_roles(["AD", "AC","PK"])

def generate_table_metadata(table_name):

    columns = db.Model.metadata.tables[table_name].columns.keys()
    table_metadata = [{"field":column,"headerName":column} for column in columns]
    filters = generate_filters(table_name)
    results ={"table_metadata":table_metadata,"filters":filters}
    return flask.jsonify(results)

def generate_filters(table_name):
    columns = db.Model.metadata.tables[table_name].columns
    result = []
    for column in columns:  
        print(column.type)
        column_type =str(column.type)
        if column_type == "INTEGER":
            print("int filter") 
            filter_def =intFilter(column.name)
        elif column_type=="JSON":
            print("json filter")    
            filter_def= jsonFilter(column.name)
        elif column_type=="TIMESTAMP WITH TIME ZONE" or column.type =="DATE":
            print("date filter")    
            filter_def = dateFilter(column.name)
        else:
            print("string filter")  
            filter_def = stringFilter(column.name)

        result.append(filter_def)
    return result

def stringFilter(id):
    return {"id":id,"name":id,"label":id,"type":"string","input":"text","operators":["equal","contains"]}

def intFilter(id):
    return {"id":id,"name":id,"label":id,"type":"number","input":"text","operators":["equal","between","less","greater","less_or_equal","greater_or_equal"]}

def dateFilter(id):
    return {"id":id,"name":id,"label":id,"type":"date","validation":{"format":"YYYY-MM-DD"},"plugin":"datepicker","plugin_config":{"format":"yyyy-mm-dd","todayBtn":"linked","todayHighlight":True,"autoclose":True},"operators":["between","equal","less_or_equal","greater","less","greater_or_equal"]}

def jsonFilter(id):
    return {"id":id,"name":id,"label":id,"type":"string","input":"text","operators":["equal","contains"]}

def boolFilter(id):
    return {"id":id,"name":id,"label":id,"type":"string","input":"text","operators":["equal","contains"]}