import base64
import hashlib

from Crypto.Cipher import AES
from flask import json
from flask_login import current_user
from sqlalchemy import or_, and_, literal

import config
from appholder import db
from dbmodels import MembershipPending, Voters, Memberships, MembersThroughSMS, AssemblyConstituencies, VoterStatus,MembershipTargets,PollingStations,Mandals,Janasena_Missedcalls
from utilities.dbrel import object_as_dict
from sqlalchemy.sql import select


#

class Membership_details():

    def check_voterid_existence(self, voter_id):
        
        voter_details = Memberships.query.filter_by(voter_id=voter_id).first()
        if voter_details is None:
            return True
        return False

    def check_membership_existence(self, membership_id):

        member_details = Memberships.query.filter_by(membership_id=str(membership_id).upper()).first()
        print member_details
        if member_details is not None:
            return True
        return False

    def check_phonenumber_existence(self, phone):

        membership_id = Memberships.query.filter_by(phone=phone).first()

        if membership_id is None:
            return True

        else:
            return False

    def check_edit_voterid_existence(self, voter_id,membership_id):
        
        voter_details = db.session.query(Memberships.membership_id).filter(
            or_(Memberships.voter_id == voter_id, Memberships.membership_id != membership_id)).first()
        if voter_details is None:
            return True
        return False
    
    def check_edit_phonenumber_count(self, phone,membership_id):
        phone_details = db.session.query(Memberships.membership_id).filter(
            or_(Memberships.phone == phone, Memberships.membership_id != membership_id)).count()
        
        
        if phone_details is None:
            return True
        elif phone_details <5:
            return True
        else:
            return False
    def check_phone_or_email(self, ephone, e_email):

        membership_id = db.session.query(Memberships.membership_id).filter(
            or_(Memberships.phone == ephone, Memberships.email == e_email)).first()

        if membership_id is None:
            return True

        else:
            return False

    def check_phonenumber_count(self, phone):

        phone_details = Memberships.query.filter_by(phone=phone).count()
        print phone_details
        if phone_details is None:
            return True
        elif phone_details <5:
            return True
        else:
            return False

    def check_manual_phonenumber_count(self, phone):

        phone_details = Memberships.query.filter_by(phone=phone).count()

        if phone_details is None:
            return True
        elif phone_details < 1:
            return True
        else:
            return False
    def get_members_basic_details(self, data_type=None, value=None, is_volunteer=None, membership_through=None,
                                    state=None, page_no=None):

        limit = 500000
        if page_no < 1:
            page_no = 1
        offset = (int(page_no) - 1) * limit
        limit = offset + limit
        count = 0

        base_query = db.session.query(Memberships.membership_id, Memberships.phone,  Memberships.name,
                                      Memberships.age, Memberships.sex,Memberships.email, AssemblyConstituencies.constituency_name,
                                      AssemblyConstituencies.district_name, Memberships.polling_station_id,
                                            ).join(AssemblyConstituencies, and_(
            Memberships.constituency_id == AssemblyConstituencies.constituency_id,
            Memberships.state_id == AssemblyConstituencies.state_id)).join(Janasena_Missedcalls,Janasena_Missedcalls.supporter_id==Memberships.membership_id).join(MembershipTargets,
                                                                           MembershipTargets.membership_id == Memberships.membership_id).outerjoin(
            PollingStations, and_(Memberships.state_id == PollingStations.state_id,
                                  Memberships.constituency_id == PollingStations.constituency_id,
                                  Memberships.polling_station_id == PollingStations.ps_id)).outerjoin(Mandals,PollingStations.mandal_id == Mandals.seq_id)


        if data_type is not None:
            if data_type == 'state':

                query = base_query.filter(
                    Memberships.state_id == value)



            elif data_type == 'district':

                query = base_query.filter(
                    AssemblyConstituencies.district_name == value)


            elif data_type == 'assembly':

                if state is None or state == '':
                    return []
                else:

                    query = base_query.filter(
                        and_(Memberships.constituency_id == value, Memberships.state_id == state))


            elif data_type == 'mandal':
                query = base_query.filter(
                    Mandals.seq_id == value)
            else:
                return []
        else:
            query = base_query

        if is_volunteer is not None:
            if is_volunteer == 'T':
                query = query.filter(Memberships.is_volunteer == 'T')
            else:
                query = query.filter(Memberships.is_volunteer == None)
        if membership_through is not None:
            if membership_through == 'W':
                query = query.filter(Memberships.membership_through.in_(tuple(['M', 'W'])))
            elif membership_through == 'ME':
                query = query.filter(Memberships.membership_through.in_(tuple(['ME'])))
            else:
                query = query.filter(Memberships.membership_through.in_(tuple(['S'])))

        query = query.order_by(Memberships.membership_id)
        print query
        details = query.all()
        if len(details):
            details = [r._asdict() for r in details]
        return details
    def get_members_profile_details(self, data_type=None, value=None, is_volunteer=None, membership_through=None,
                                    state=None, page_no=None):

        limit = 500000
        if page_no < 1:
            page_no = 1
        offset = (int(page_no) - 1) * limit
        limit = offset + limit
        count = 0

        base_query = db.session.query(Memberships.membership_id, Janasena_Missedcalls.phone,  Memberships.name,
                                      Memberships.age, Memberships.sex,Memberships.email,AssemblyConstituencies.constituency_name,
                                      AssemblyConstituencies.district_name, Memberships.polling_station_id,
                                      Memberships.polling_station_name, MembershipTargets.target,
                                      MembershipTargets.achievement).join(AssemblyConstituencies, and_(
            Memberships.constituency_id == AssemblyConstituencies.constituency_id,
            Memberships.state_id == AssemblyConstituencies.state_id)).join(Janasena_Missedcalls,Janasena_Missedcalls.supporter_id==Memberships.membership_id).join(MembershipTargets,
                                                                           MembershipTargets.membership_id == Memberships.membership_id).outerjoin(
            PollingStations, and_(Memberships.state_id == PollingStations.state_id,
                                  Memberships.constituency_id == PollingStations.constituency_id,
                                  Memberships.polling_station_id == PollingStations.ps_id)).outerjoin(Mandals,PollingStations.mandal_id == Mandals.seq_id)


        if data_type is not None:
            if data_type == 'state':

                query = base_query.filter(
                    Memberships.state_id == value)



            elif data_type == 'district':

                query = base_query.filter(
                    AssemblyConstituencies.district_name == value)


            elif data_type == 'assembly':

                if state is None or state == '':
                    return []
                else:

                    query = base_query.filter(
                        and_(Memberships.constituency_id == value, Memberships.state_id == state))


            elif data_type == 'mandal':
                query = base_query.filter(
                    Mandals.seq_id == value)
            else:
                return []
        else:
            query = base_query

        if is_volunteer is not None:
            if is_volunteer == 'T':
                query = query.filter(Memberships.is_volunteer == 'T')
            else:
                query = query.filter(Memberships.is_volunteer == None)
        if membership_through is not None:
            if membership_through == 'W':
                query = query.filter(Memberships.membership_through.in_(tuple(['M', 'W'])))
            elif membership_through == 'ME':
                query = query.filter(Memberships.membership_through.in_(tuple(['ME'])))
            else:
                query = query.filter(Memberships.membership_through.in_(tuple(['S'])))

        query = query.order_by(Memberships.membership_id)
        print query
        details = query.all()
        if len(details):
            details = [r._asdict() for r in details]
        return details
    def get_members_count(self, data_type=None, value=None, is_volunteer=None, membership_through=None,
                                    state=None):
        base_query = db.session.query(Memberships.phone).join(AssemblyConstituencies, and_(
        Memberships.constituency_id == AssemblyConstituencies.constituency_id,
        Memberships.state_id == AssemblyConstituencies.state_id)).outerjoin(
        PollingStations, and_(Memberships.state_id == PollingStations.state_id,
                              Memberships.constituency_id == PollingStations.constituency_id,
                              Memberships.polling_station_id == PollingStations.ps_id)).outerjoin(Mandals,PollingStations.mandal_id == Mandals.seq_id)

        

        if data_type is not None:
            if data_type == 'state':

                query = base_query.filter(
                    Memberships.state_id == value)



            elif data_type == 'district':

                query = base_query.filter(
                    AssemblyConstituencies.district_name == value)


            elif data_type == 'assembly':

                if state is None or state == '':
                    return []
                else:

                    query = base_query.filter(
                        and_(Memberships.constituency_id == value, Memberships.state_id == state))


            elif data_type == 'mandal':
                query = base_query.filter(
                    Mandals.seq_id == value)
            else:
                return []
        else:
            query = base_query

        if is_volunteer is not None:
            if is_volunteer == 'T':
                query = query.filter(Memberships.is_volunteer == 'T')
            else:
                query = query.filter(Memberships.is_volunteer == None)
        if membership_through is not None:
            if membership_through == 'W':
                query = query.filter(Memberships.membership_through.in_(tuple(['M', 'W'])))
            elif membership_through == 'ME':
                query = query.filter(Memberships.membership_through.in_(tuple(['ME'])))
            else:
                query = query.filter(Memberships.membership_through.in_(tuple(['S'])))

        
        
        count = query.count()

        return count


    def get_members_details_campaigns(self, data_type=None, value=None, is_volunteer=None, membership_through=None,
                                    state=None, keys=None,c_type=None):
        missed_call_columns=[]
        
        if len(keys):
            if 'name' in keys:
                if c_type=='sms':
                    base_query = db.session.query(Memberships.name,Janasena_Missedcalls.phone)


                else:
                    base_query = db.session.query(Memberships.name,Memberships.email)

            else:
                if c_type=='sms':
                    base_query = db.session.query(Memberships.membership_id,Janasena_Missedcalls.phone)


                else:
                    base_query = db.session.query(Memberships.membership_id,Memberships.email)

        else:
            if c_type=='sms':
                base_query = db.session.query(Memberships.membership_id,Janasena_Missedcalls.phone)

            else:
                base_query = db.session.query(Memberships.membership_id,Memberships.email)


        query_string=base_query.join(AssemblyConstituencies, and_(
        Memberships.constituency_id == AssemblyConstituencies.constituency_id,
        Memberships.state_id == AssemblyConstituencies.state_id)).join(Janasena_Missedcalls,Janasena_Missedcalls.supporter_id==Memberships.membership_id).outerjoin(
        PollingStations, and_(Memberships.state_id == PollingStations.state_id,
                              Memberships.constituency_id == PollingStations.constituency_id,
                              Memberships.polling_station_id == PollingStations.ps_id)).outerjoin(Mandals,PollingStations.mandal_id == Mandals.seq_id)

        if data_type is not None:
            if data_type == 'state':

                query = query_string.filter(
                    Memberships.state_id == value)


            elif data_type == 'district':

                query = query_string.filter(
                    AssemblyConstituencies.district_name == value)


            elif data_type == 'assembly':

                if state is None or state == '':
                    return []
                else:

                    query = query_string.filter(
                        and_(Memberships.constituency_id == value, Memberships.state_id == state))


            elif data_type == 'mandal':
                query = query_string.filter(
                    Mandals.seq_id == value)
            else:
                return []
        else:
            query = query_string

        if is_volunteer is not None:
            if is_volunteer == 'T':
                query = query.filter(Memberships.is_volunteer == 'T')
            else:
                query = query.filter(Memberships.is_volunteer == None)
        if membership_through is not None:
            if membership_through == 'W':
                query = query.filter(Memberships.membership_through.in_(tuple(['M', 'W'])))
            elif membership_through == 'ME':
                query = query.filter(Memberships.membership_through.in_(tuple(['ME'])))
            else:
                query = query.filter(Memberships.membership_through.in_(tuple(['S'])))

        
        if c_type=='sms':
           query=query.distinct(Janasena_Missedcalls.phone)
        else:
            query=query.distinct(Memberships.email)

        details = query.all()
        if len(details):
            details = [r._asdict() for r in details]
        return details
    def get_members_details(self, data_type=None, value=None, state=None, page_no=None):
        value = json.loads(value)
        limit = 1000
        if page_no < 1:
            page_no = 1
        offset = (int(page_no) - 1) * limit
        limit = offset + limit
        count = 0
        if data_type == 'state':

            query = db.session.query(Memberships.membership_id, Memberships.phone,
                                     Memberships.user_gcm_id.label('fcm_id')).filter(
                Memberships.state_id.in_(tuple(value))).order_by(Memberships.membership_id).slice(offset, limit)

            details = query.all()

        elif data_type == 'district':

            query = db.session.query(Memberships.membership_id, Memberships.phone,
                                     Memberships.user_gcm_id.label('fcm_id')).filter(
                Memberships.district_id.in_(tuple(value))).order_by(Memberships.membership_id).slice(offset, limit)
            details = query.all()

        elif data_type == 'assembly':

            if state is None or state == '':
                details = []
            else:

                query = db.session.query(Memberships.membership_id, Memberships.phone,
                                         Memberships.user_gcm_id.label('fcm_id')).filter(
                    and_(Memberships.constituency_id.in_(tuple(value)), Memberships.state_id == state)).order_by(
                    Memberships.membership_id).slice(offset, limit)

                details = query.all()

        else:
            details = []

        if len(details):
            details = [r._asdict() for r in details]
        return details
    # def get_members_details(self, data_type=None, value=None, state=None):
    #     value = json.loads(value)
    #
    #     if data_type == 'state':
    #         query = db.session.query(Memberships.membership_id, Memberships.phone).filter(
    #             Memberships.state_id.in_(tuple(value)))
    #
    #         details = query.all()
    #
    #     elif data_type == 'district':
    #         query = db.session.query(Memberships.membership_id, Memberships.phone).filter(
    #             Memberships.district_id.in_(tuple(value)))
    #         details = query.all()
    #
    #     elif data_type == 'assembly':
    #
    #         if state is None or state == '':
    #             details = []
    #         else:
    #             query = db.session.query(Memberships.membership_id, Memberships.phone).filter(
    #                 and_(Memberships.constituency_id.in_(tuple(value)), Memberships.state_id == state))
    #
    #             details = query.all()
    #
    #     else:
    #         details = []
    #
    #     if len(details):
    #         details = [r._asdict() for r in details]
    #     return details

    def get_membership_details(self, membership_id=None, voter_id=None):
        if membership_id is not None:
            member_details = Memberships.query.filter_by(membership_id=membership_id.upper()).first()
            if member_details is not None:
                d = {}
                try:
                    d['name'] = str(member_details.name).rstrip()
                except Exception as e:
                    d['name'] = member_details.name
                if member_details.voter_id != "" and member_details.voter_id is not None:
                    d['voter_id'] = encrypt_decrypt(str(member_details.voter_id).rstrip())

                else:
                    d['voter_id'] = ''
                try:
                    d['relationship_name'] = str(member_details.relationship_name).rstrip()
                except Exception as e:
                    d['relationship_name'] = member_details.relationship_name
                d['relation_type'] = str(member_details.relation_type).rstrip()
                try:
                    d['address'] = str(member_details.address).rstrip()
                except Exception as e:
                    d['address'] = ''
                d['age'] = str(member_details.age).rstrip()
                d['sex'] = str(member_details.sex).rstrip()
                d['language'] = member_details.language
                if member_details.email is not None and member_details.email != '':
                    d['email'] = encrypt_decrypt(member_details.email)
                else:
                    d['email'] = ''
                d['state'] = member_details.state
                d['ac_no'] = member_details.constituency_id
                d['ps_no'] = member_details.polling_station_id
                d['phone'] = encrypt_decrypt(member_details.phone)
                d['membership_id'] = member_details.membership_id
                d['polling_station_name'] = str(member_details.polling_station_name).rstrip()
                d['membership_through'] = member_details.membership_through
                d['img_url'] = 'https://janasenaparty.org/static/img/default-avatar.png'
                if member_details.image_url is not None and member_details.image_url != '':
                    old_img_container = str(member_details.image_url).split(".")[0]
                    if old_img_container.lower() != "https://janasenalive":
                        d['img_url'] = member_details.image_url

                d['state_id'] = member_details.state_id
                d['district_id'] = member_details.district_id
                return d
        if voter_id is not None:
            member_details = Memberships.query.filter_by(voter_id=voter_id).first()
            if member_details is not None:
                data = object_as_dict(member_details)
                return data
        return None

    def check_volunteer_status(self, membership_id=None, phone=None):

        if membership_id is None:
            ephone = encrypt_decrypt(phone, 'E')
            volunteer_details = Memberships.query.filter_by(phone=ephone, is_volunteer='T').first()


        else:
            ephone = encrypt_decrypt(phone, 'E')
            print  ephone
            volunteer_details = Memberships.query.filter_by(membership_id=membership_id.upper(), phone=ephone,
                                                            is_volunteer='T').first()

        if volunteer_details is not None:
            return True

        return False

    def check_association_status(self, membership_id=None):
        association_status = None

        association_status = Memberships.query.filter_by(membership_id=membership_id, member_associations=None).first()
        print association_status

        if association_status is not None:
            return False

        return True

    def check_image_status(self, membership_id=None):
        image_status = None

        image_status = Memberships.query.filter_by(membership_id=membership_id, image_url=None).first()

        if image_status is not None:
            return image_status

        return False

    def get_volunteer_membership_id(self, phone):
        volunteer_details = Memberships.query.filter_by(phone=phone, is_volunteer='T').first()
        if volunteer_details is not None:
            data = object_as_dict(volunteer_details)
            print data
            return data['membership_id']
        return None

    def check_and_update_volunteer_status(self, membership_id=None, email=None):
        volunteer_details = Memberships.query.filter_by(membership_id=membership_id.upper()).first()

        if volunteer_details is not None:
            phone = volunteer_details.phone
            print phone
            check_existence = Memberships.query.filter_by(phone=phone, is_volunteer='T').first()
            print check_existence
            if check_existence is None:
                voluteer_record = Memberships.query.filter_by(membership_id=membership_id.upper(), phone=phone).first()
                print voluteer_record
                if voluteer_record is not None:
                    voluteer_record.is_volunteer = 'T'
                    print email
                    email = encrypt_decrypt(email, 'E')
                    print email
                    voluteer_record.email = email
                    db.session.commit()
                    return
        return

    def get_memberReferrals(self):
        membership_id = str(current_user.membership_id).upper()
        referrals_details = Memberships.query.filter_by(referral_id=membership_id).all()
        data = []
        if referrals_details is not None:
            for referal in referrals_details:
                d = {}
                try:
                    d['name'] = str(referal.name).rstrip()

                    d['relation_name'] = str(referal.relationship_name).rstrip()
                except Exception as e:
                    d['name'] = referal.name
                    d['relation_name'] = referal.relationship_name
                d['relation_type'] = str(referal.relation_type).rstrip()
                d['sex'] = str(referal.sex).rstrip()
                d['age'] = str(referal.age).rstrip()
                data.append(d)

        return data

    def get_memberships_count(self):
        memberships_count = Memberships.query.count()
        print memberships_count
        return memberships_count

class Voter_details():

    def check_existence(self, voter_id):
        voter_details = Voters.query.filter_by(voter_id=voter_id).first()
        if voter_details is not None:
            return True
        return False

    def get_polling_station_voter_count(self):
        print 'hello'
        voter_list = db.session.query(Voters.state, Voters.ac_no, Voters.part_no).group_by(Voters.state, Voters.ac_no,
                                                                                           Voters.part_no).all()
        print 'hellowoowwo'
        print voter_list
        print '/////////////'
        if voter_list is not None:
            data = list(voter_list)
            print data
            return data
        return []

    def get_polling_station_details(self, voter_id):
        voter_details = db.session.query(Voters).filter_by(voter_id=voter_id).first()
        if voter_details is not None:
            data = {}
            part_no = str(voter_details.part_no).rstrip()
            ac_no = str(voter_details.ac_no).rstrip()
            st = voter_details.state
            data['polling_station_address'] = str(voter_details.section_name).rstrip()
            data['polling_station_id'] = part_no
            data['constituency_id'] = ac_no
            if st == 1:

                data['state'] = 'Andhra Pradesh'

            else:

                data['state'] = 'Telangana'

            voter_list_count = db.session.query(Voters).filter(
                and_(Voters.state == st, Voters.ac_no == ac_no, Voters.part_no == part_no)).count()
            data['voters_count'] = voter_list_count
            details = AssemblyConstituencies.query.filter_by(constituency_id=ac_no, state_id=st).first()
            if details is not None:
                data['constituency_name'] = str(details.constituency_name).rstrip()
                data['district'] = str(details.district_name).rstrip()

            return data
        return None

    def get_polling_booth_voters(self, voter_id, page_no=1):

        voter_details = db.session.query(Voters).filter_by(voter_id=voter_id).first()
        if voter_details is not None:
            part_no = voter_details.part_no
            ac_no = voter_details.ac_no
            st = voter_details.state
            subquery = db.session.query(VoterStatus.voter_id)
            voter_list = db.session.query(Voters.voter_name.label('name'), Voters.relation_name, Voters.relation_type,
                                          literal('F').label("status"), Voters.state, Voters.house_no,
                                          Voters.section_name).outerjoin(VoterStatus,
                                                                         VoterStatus.voter_id == Voters.voter_id).filter(
                ~Voters.voter_id.in_(subquery)).filter(
                and_(Voters.state == st, Voters.ac_no == ac_no, Voters.part_no == part_no
                     )).order_by(Voters.house_no).all()

            return [u._asdict() for u in voter_list]

        return None

    def get_constituency_details(self, voter_id):
        voter_details = Voters.query.filter_by(voter_id=voter_id).first()
        if voter_details is not None:
            data = object_as_dict(voter_details)
            return data['ac_no']
        return None

    def update_voter_status(self, voter_id):
        voter_details = Voters.query.filter_by(voter_id=voter_id.lower()).first()
        if voter_details is not None:
            voter_details.status = 'T'
            db.session.commit()
        return

    def get_voter_details(self, voter_id):
        voter_details = Voters.query.filter_by(voter_id=voter_id.lower()).first()
        if voter_details is not None:
            data = object_as_dict(voter_details)

            return data
        return None
    # commented on 25/04/2018
    # def get_polling_booth_voters(self, voter_id, page_no=1):

    #     voter_details = db.session.query(Voters).filter_by(voter_id=voter_id).first()
    #     if voter_details is not None:

    #         part_no = voter_details.part_no
    #         ac_no = voter_details.ac_no
    #         st = voter_details.state

    #         voter_list = db.session.query(Voters.voter_name.label('name'),Voters.relation_name,Voters.relation_type,literal('F').label("status"),Voters.state,Voters.house_no,Voters.section_name).filter(
    #             and_(Voters.state == st, Voters.ac_no == ac_no, Voters.part_no == part_no,
    #                  Voters.status == None)).order_by(Voters.house_no).all()

    #         return [u._asdict() for u in voter_list]

    #     return None

    # def get_polling_booth_voters(self,voter_id,page_no=1):

    #     voter_details = db.session.query(Voters).filter_by(voter_id =voter_id).first()
    #     if  voter_details is not None:

    #         part_no=voter_details.part_no
    #         ac_no=voter_details.ac_no
    #         st=voter_details.state
    #         limit=50
    #         if page_no<1:
    #             page_no=1
    #         offset=(int(page_no)-1)*limit
    #         limit=offset+limit

    #         print offset,limit
    #         print 'ap'
    #         voter_list = db.session.query(Voters).filter(and_(Voters.state==st,Voters.ac_no==ac_no,Voters.part_no==part_no,Voters.status==None)).order_by(Voters.house_no).slice(offset,limit).all()

    #         if voter_list is not None:
    #             data=[]
    #             for voter in voter_list:
    #                 d={}
    #                 try:
    #                     d['name']=str(voter.voter_name).rstrip()
    #                 except Exception as e:
    #                     d['name']=voter.voter_name
    #                 try:
    #                     d['relation_name']=str(voter.relation_name).rstrip()
    #                 except Exception as e:
    #                     d['relation_name']=voter.voter_name

    #                 d['relation_type']=str(voter.relation_type).rstrip()
    #                 if voter.status is None:
    #                     d['status']='F'
    #                 else:
    #                     d['status']='T'
    #                 d['house_no']=str(voter.house_no).rstrip()
    #                 d['section_name']=str(voter.section_name).rstrip()
    #                 data.append(d)
    #             return data

    #     return None

