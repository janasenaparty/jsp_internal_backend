from datetime import timedelta
from functools import wraps, update_wrapper

from flask import url_for, request, session, make_response, current_app, render_template
from flask_login import logout_user, current_user
from werkzeug.exceptions import abort
from werkzeug.utils import redirect

from appholder import db
from dbmodels import member_session, BisSession, JspsurveySession,AdminIPAddress


def janasainyam_login_required(f):
    @wraps(f)
    def wrapped(*args, **kwargs):
        host=request.headers.get("HOST",None)
        if host!="127.0.0.1:5000":
            
            request_ip=request.headers.get("Cf-Connecting-Ip",None)
            
            # ip_existing=db.session.query(AdminIPAddress).filter_by(ip_address=request_ip).first()
            # #ips=["202.62.73.170","202.62.85.238","183.82.126.214","183.83.196.181","124.123.60.231","124.123.74.195","124.123.58.255","115.98.197.253",'183.83.197.151','103.213.221.135']
            # if ip_existing is None:
            #      abort(401)
            

        if current_user is not None:
                
                if current_user.is_authenticated:
                    

                        if session['t'] != 'J':
                            return redirect(session['current'])
                    

                else:

                    session['current'] = request.url

                    return redirect(url_for('offline_memberships.janasainyamLogin'))
       

        return f(*args, **kwargs)

    return wrapped

def ip_validation_required(f):
    @wraps(f)
    def wrapped(*args, **kwargs):
        host=request.headers.get("HOST",None)
        if host!="127.0.0.1:8000":
       
            request_ip=request.headers.get("Cf-Connecting-Ip",None)
            ip_existing=db.session.query(AdminIPAddress).filter_by(ip_address=request_ip).first()
            #ips=["202.62.73.170","202.62.85.238","183.82.126.214","183.83.196.181","124.123.60.231","124.123.74.195","124.123.58.255","115.98.197.253",'183.83.197.151','103.213.221.135']
            #if ip_existing is None:

                 #abort(401)
                

        return f(*args, **kwargs)

    return wrapped


def required_roles(roles):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            from urlparse import urlparse
            url_obj=urlparse(request.url)
        
            path=str(url_obj.path) 
            
            if current_user.urole not in roles:
                role = current_user.urole
                return render_template('403.html')

            return f(*args, **kwargs)

        return wrapped

    return wrapper


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization

        if not auth:  # no header set

            abort(401)

        if auth.username != 'smscountry' or auth.password != 'misscalljspbysms':
            abort(401)
        return f(*args, **kwargs)

    return decorated

def requires_api_token(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization

        if not auth:  # no header set

            abort(401)

        if auth.username != 'kuladeep' or auth.password != 'jspmembersdata#@!123':
            abort(401)
        return f(*args, **kwargs)

    return decorated

def requires_authToken(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization

        if not auth:  # no header set

            abort(401)

        membership_id = auth.username
        token = auth.password
        
        status = 'L'
        status = member_session.query.filter_by(membership_id=membership_id, session_id=token, action=status).first()
        if status is None:
            abort(401)
        return f(*args, **kwargs)

    return decorated


def requires_manAuth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization

        if not auth:  # no header set

            abort(401)

        membership_id = auth.username
        token = auth.password
        
        status = 'MAL'
        status = member_session.query.filter_by(membership_id=membership_id, session_id=token, action=status).first()
        if status is None:
            abort(401)
        return f(*args, **kwargs)

    return decorated
def requires_bisToken(f):

    @wraps(f)
    def decorated(*args, **kwargs):
        
        auth = request.authorization
        

        if not auth:  # no header set

            abort(401)

        membership_id = auth.username
        token = auth.password
        
        status = 'L'
        status = db.session.query(BisSession).filter(BisSession.admin_id==membership_id, BisSession.session_val==token, BisSession.status==status).first()
        if status is None:
            abort(401)
        return f(*args, **kwargs)

    return decorated


def requires_jspsurveyToken(f):

    @wraps(f)
    def decorated(*args, **kwargs):
        
        auth = request.authorization
        

        if not auth:  # no header set

            abort(401)

        membership_id = auth.username
        token = auth.password
        
        status = 'L'
        status = db.session.query(JspsurveySession).filter(JspsurveySession.admin_id==membership_id, JspsurveySession.session_val==token, JspsurveySession.status==status).first()
        if status is None:
            abort(401)
        return f(*args, **kwargs)

    return decorated




def requires_EventsauthToken(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
       
        if not auth:  # no header set

            abort(401)
        membership_id = auth.username
        token = auth.password
       
        status = 'EL'
        status = member_session.query.filter_by(membership_id=membership_id, session_id=token, action=status).first()
       
        if status is None:
            abort(401)
        return f(*args, **kwargs)

    return decorated


def member_login_required(f):
    @wraps(f)
    def wrapped(*args, **kwargs):

        if current_user is not None:

            if current_user.is_authenticated:

                if session['t'] != 'M':
                    return redirect('/memberlogin')

            else:
                if 'current' in session.keys():

                    return redirect(session['current'])
                else:
                    return redirect('/memberlogin')

        return f(*args, **kwargs)

    return wrapped

def shatagni_login_required(f):
    @wraps(f)
    def wrapped(*args, **kwargs):
        
        host=request.headers.get("HOST",None)
        if host!="127.0.0.1:5000":
       
            request_ip=request.headers.get("Cf-Connecting-Ip",None)
            
            ips=["202.62.73.170","202.62.85.126"]
            if request_ip not in ips:
                 abort(401)


        if current_user is not None:

            if current_user.is_authenticated:

                if session['t'] != 'SM':
                    return redirect('/shatagni_magazine')

            else:
                if 'current' in session.keys():

                    return redirect(session['current'])
                else:
                    return redirect('/shatagni_magazine')

        return f(*args, **kwargs)

    return wrapped
def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    
    
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, str):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, str):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()

            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)

    return decorator


def frontdesk_login_required(f):
    @wraps(f)
    def wrapped(*args, **kwargs):

        
        if current_user is not None:

            if current_user.is_authenticated:

                if session['t'] != 'F':
                    return redirect(session['current'])

            else:

                session['current'] = request.url

                return redirect(url_for('flogin'))

        return f(*args, **kwargs)

    return wrapped

def admin_login_required(f):
    @wraps(f)
    def wrapped(*args, **kwargs):

        if current_user is not None:

            if current_user.is_authenticated:
              
                    
                if session['t'] != 'A':
                    return redirect(url_for('admin_dash.admin'))
            
            else:

                session['current'] = request.url

                return redirect(url_for('admin_dash.admin'))
        
        return f(*args, **kwargs)

    return wrapped


def constituency_login_required(f):
    @wraps(f)
    def wrapped(*args, **kwargs):
        print(current_user)
        if current_user is not None:

            if current_user.is_authenticated:

                if session['t'] not in ['CL','CLA','CLD'
                                        ]:
                    return redirect(url_for('admin_dash.constituency_member_login'))

            else:

                session['current'] = request.url

                return redirect(url_for('admin_dash.constituency_member_login'))
        
        return f(*args, **kwargs)

    return wrapped

def local_admin_login_required(f):
    @wraps(f)
    def wrapped(*args, **kwargs):
       
        if current_user is not None:
           
            if current_user.is_authenticated:
               
                if session['t'] not in ['LA','A','MB']:
                    return redirect(session['current'])

            else:
                if 'current' in session.keys():

                    return redirect(session['current'])
                else:
                    return redirect('/group/lalogin ')

        return f(*args, **kwargs)

    return wrapped