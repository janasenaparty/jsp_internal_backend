# coding= utf-8
# -*- coding: utf-8 -*-
import datetime

from dateutil.parser import parse
from reportlab.lib.colors import purple, red, black
from reportlab.lib.enums import TA_CENTER, TA_RIGHT, TA_JUSTIFY, TA_LEFT
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle
from reportlab.pdfgen import canvas
import os

# def generate_receipt(order_number, name, address, amount):
from reportlab.platypus import Paragraph, Image

styles = {}
styles['title'] = ParagraphStyle(
    'title',

    fontName='Times',
    fontSize=24,
    leading=42,
    alignment=TA_CENTER,
    textColor=red,
)
styles['rupee'] = ParagraphStyle(
    'rupee',

    fontName='Times',
    fontSize=20,
    leading=42,
    alignment=TA_LEFT,
    textColor=black,
)

styles['address_header'] = ParagraphStyle(
    'address_header',
    fontName='Helvetica-Bold',
    fontSize=9,
    alignment=TA_RIGHT,
    textColor=black,
    spaceAfter=6
)
styles['receipt'] = ParagraphStyle(
    'receipt',

    fontName='Helvetica-Bold',
    fontSize=16,
    alignment=TA_CENTER,
    textColor=black,
    spaceAfter=6
)

styles['text'] = ParagraphStyle(
    'text',

    fontName='Helvetica',
    fontSize=10,
    alignment=TA_JUSTIFY,
    textColor=black,
    spaceAfter=6,
    wordWrap=True
)

styles['cond'] = ParagraphStyle(
    'cond',

    fontName='Helvetica',
    fontSize=8,
    alignment=TA_LEFT,
    textColor=black,
    spaceAfter=6,
    wordWrap=True
)


def generate_receipt(fileobj, ordno='', date=str(datetime.datetime.now().date().strftime("%d-%m-%Y")),
                     name='', address='',
                     amount='', payment_type='', ref_id='', ref_date='',
                     ref_bank='',amount_in_words=''):
    dup_const = 380
    c = canvas.Canvas(fileobj, pagesize=A4)

    if payment_type == 'Cash':
        line_4_string = 'Vide ' + payment_type + '  Dated <u>' + ref_date + '</u>'
        line_5_string = 'towards Voluntary Contribution/ Donation to the party.'
    else:
        ref_date = parse(ref_date)
        ref_date = ref_date.strftime("%d-%m-%Y")

        line_4_string = 'Vide ' + payment_type + ' No: <u>' + ref_id + '</u> Dated <u>' + ref_date + '</u> of <u>' + ref_bank + '</u>'
        line_5_string = 'towards Voluntary Contribution/ Donation to the party.'
    # print c.getAvailableFonts()
    party_name = Paragraph(text="<font fontSize=28>J</font>ANASENA PARTY", style=styles['title'])
    party_name.wrapOn(canv=c, aW=A4[0], aH=A4[1])
    party_name.drawOn(canvas=c, x=10, y=772)

    # draw lines under Janasena
    # Top thin line
    c.setStrokeColor(red)
    c.setLineWidth(1)
    lp = c.beginPath()
    lp.moveTo(10, 783)
    lp.lineTo(A4[0] - 10, 783)
    lp.close()
    c.drawPath(lp)

    # middle thick line
    c.setStrokeColor(red)
    c.setLineWidth(5)
    lp = c.beginPath()
    lp.moveTo(10, 779)
    lp.lineTo(A4[0] - 10, 779)
    lp.close()
    c.drawPath(lp)

    # bottom thin line
    c.setStrokeColor(red)
    c.setLineWidth(1)
    lp = c.beginPath()
    lp.moveTo(10, 775)
    lp.lineTo(A4[0] - 10, 775)
    lp.close()
    c.drawPath(lp)

    # bottom black line
    c.setStrokeColor(black)
    c.setLineWidth(1)
    lp = c.beginPath()
    lp.moveTo(10, 772)
    lp.lineTo(A4[0] - 10, 772)
    lp.close()
    c.drawPath(lp)

    # Janasena logo
    # cwd = os.getcwd()
    # print cwd
    I = Image('static/img/logo.png', width=75, height=75)
    I.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    I.drawOn(canvas=c, x=30, y=740)

    # Address under lines
    address_name = Paragraph(
        text="# 8-2-293/82/HH/91/A/201, Huda Heights, MLA Colony, Road No.12, Banjara Hills, Hyderabad-500034",
        style=styles['address_header'])
    address_name.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    address_name.drawOn(canvas=c, x=10, y=760)
    # c.drawString(100,750,"JANASENA PARTY")

    # Receipt
    receipt = Paragraph(
        text="<u>RECEIPT</u>",
        style=styles['receipt'])
    receipt.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    receipt.drawOn(canvas=c, x=10, y=743)

    # copy type
    copy_type = Paragraph(text="DONOR COPY", style=styles['text'])
    copy_type.wrapOn(canv=c, aW=A4[0], aH=A4[1])
    copy_type.drawOn(canvas=c, x=A4[0] - 150, y=743)
    # date
    rdate = Paragraph(
        text="Date: ",
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=A4[0] - 150, y=708)

    # date value
    rdate = Paragraph(
        text=date,
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=A4[0] - 120, y=708)

    # order number
    rdate = Paragraph(
        text="No:",
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=40, y=708)

    # order value
    rdate = Paragraph(
        text=ordno,
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=60, y=708)

    # line 1
    rdate = Paragraph(
        text="Received with thanks from ____________________________________________________________________",
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=40, y=688)

    # line 1 value
    rdate = Paragraph(
        text=name,
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=160, y=689)

    # line 2
    rdate = Paragraph(
        text="Address __________________________________________________________________________________",
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=40, y=668)

    # line 2 value
    rdate = Paragraph(
        text=address,
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=90, y=669)

    # line 3
    rdate = Paragraph(
        text="The sum of Rupees_________________________________________________________________________",
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=40, y=648)

    # line 3 value
    rdate = Paragraph(
        text=amount_in_words,
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=160, y=649)

    # line 4
    rdate = Paragraph(
        text=line_4_string,
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=40, y=628)

    # line 5
    rdate = Paragraph(
        text=line_5_string,
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=40, y=608)

    # money box
    c.setStrokeColor(black)
    c.rect(40, 548, A4[0] / 5, 35, 1)

    # rupee sig
    rdate = Paragraph(
        text="Rs. " + amount,
        style=styles['rupee'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=45, y=540)

    # Auth sig
    rdate = Paragraph(
        text="Authorised Signatory",
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=A4[0] - 120, y=548)

    # bottom end line
    c.setStrokeColor(black)
    c.setLineWidth(1)
    lp = c.beginPath()
    lp.moveTo(10, 538)
    lp.lineTo(A4[0] - 10, 538)
    lp.close()
    c.drawPath(lp)

    # condition text
    rdate = Paragraph(
        text="1. A Political Party Registered U/S 29A of the Representation of Peoples Act, 1951.<br/>"
             "2. No Cash in excess of Rs. 2,000/- will be accepted.<br/>"
             "3. The said Voluntary Contribution / Donation is eligible for deduction U/S. 80GGB in case of Companies "
             "and U/S. 80GGC in case of Individuals from their Gross Total Income subject to the conditions as mentioned in the respetive sections.",
        style=styles['cond'])
    rdate.wrapOn(canv=c, aW=A4[0] - 60, aH=A4[1])
    rdate.drawOn(canvas=c, x=40, y=488)

    # ---------------------------------------------------
    # ------Separator line
    # ---------------------------------------------------

    # Separator line
    c.setStrokeColor(black)
    c.setLineWidth(1)
    lp = c.beginPath()
    lp.moveTo(0, 475)
    lp.lineTo(A4[0], 475)
    lp.close()
    c.drawPath(lp)

    # --------------------------------------------------------------------------
    # -------------------------Duplicate---------------------------------------
    # ----------------------------------------------------------------------------
    party_name = Paragraph(text="<font fontSize=28>J</font>ANASENA PARTY", style=styles['title'])
    party_name.wrapOn(canv=c, aW=A4[0], aH=A4[1])
    party_name.drawOn(canvas=c, x=10, y=772 - dup_const)

    # draw lines under Janasena
    # Top thin line
    c.setStrokeColor(red)
    c.setLineWidth(1)
    lp = c.beginPath()
    lp.moveTo(10, 783 - dup_const)
    lp.lineTo(A4[0] - 10, 783 - dup_const)
    lp.close()
    c.drawPath(lp)

    # middle thick line
    c.setStrokeColor(red)
    c.setLineWidth(5)
    lp = c.beginPath()
    lp.moveTo(10, 779 - dup_const)
    lp.lineTo(A4[0] - 10, 779 - dup_const)
    lp.close()
    c.drawPath(lp)

    # bottom thin line
    c.setStrokeColor(red)
    c.setLineWidth(1)
    lp = c.beginPath()
    lp.moveTo(10, 775 - dup_const)
    lp.lineTo(A4[0] - 10, 775 - dup_const)
    lp.close()
    c.drawPath(lp)

    # bottom black line
    c.setStrokeColor(black)
    c.setLineWidth(1)
    lp = c.beginPath()
    lp.moveTo(10, 772 - dup_const)
    lp.lineTo(A4[0] - 10, 772 - dup_const)
    lp.close()
    c.drawPath(lp)

    # Janasena logo
    # cwd = os.getcwd()
    # print cwd
    I = Image('static/img/logo.png', width=75, height=75)
    I.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    I.drawOn(canvas=c, x=30, y=740 - dup_const)

    # Address under lines
    address_name = Paragraph(
        text="# 8-2-293/82/HH/91/A/201, Huda Heights, MLA Colony, Road No.12, Banjara Hills, Hyderabad-500034",
        style=styles['address_header'])
    address_name.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    address_name.drawOn(canvas=c, x=10, y=760 - dup_const)

    # Receipt
    receipt = Paragraph(
        text="<u>RECEIPT</u>",
        style=styles['receipt'])
    receipt.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    receipt.drawOn(canvas=c, x=10, y=743 - dup_const)
    # copy type
    copy_type = Paragraph(text="OFFICE COPY", style=styles['text'])
    copy_type.wrapOn(canv=c, aW=A4[0], aH=A4[1])
    copy_type.drawOn(canvas=c, x=A4[0] - 150, y=743 - dup_const)

    # date
    rdate = Paragraph(
        text="Date: ",
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=A4[0] - 150, y=708 - dup_const)

    # date value
    rdate = Paragraph(
        text=date,
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=A4[0] - 120, y=708 - dup_const)

    # order number
    rdate = Paragraph(
        text="No:",
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=40, y=708 - dup_const)

    # order value
    rdate = Paragraph(
        text=ordno,
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=60, y=708 - dup_const)

    # line 1
    rdate = Paragraph(
        text="Received with thanks from ___________________________________________________________________",
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=40, y=688 - dup_const)

    # line 1 value
    rdate = Paragraph(
        text=name,
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=160, y=689 - dup_const)

    # line 2
    rdate = Paragraph(
        text="Address __________________________________________________________________________________",
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=40, y=668 - dup_const)

    # line 2 value
    rdate = Paragraph(
        text=address,
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=90, y=669 - dup_const)

    # line 3
    rdate = Paragraph(
        text="The sum of Rupees _________________________________________________________________________",
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=40, y=648 - dup_const)

    # line 3 value
    rdate = Paragraph(
        text=amount_in_words,
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=160, y=649 - dup_const)

    # line 4
    rdate = Paragraph(
        text=line_4_string,
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=40, y=628 - dup_const)

    # line 5
    rdate = Paragraph(
        text=line_5_string,
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=40, y=608 - dup_const)

    # money box
    c.setStrokeColor(black)
    c.rect(40, 548 - dup_const, A4[0] / 5, 35, 1)

    # rupee sig
    rdate = Paragraph(
        text="Rs. " + amount,
        style=styles['rupee'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=45, y=540 - dup_const)

    # Auth sig
    rdate = Paragraph(
        text="Authorised Signatory",
        style=styles['text'])
    rdate.wrapOn(canv=c, aW=A4[0] - 20, aH=A4[1])
    rdate.drawOn(canvas=c, x=A4[0] - 120, y=548 - dup_const)

    # bottom end line
    c.setStrokeColor(black)
    c.setLineWidth(1)
    lp = c.beginPath()
    lp.moveTo(10, 538 - dup_const)
    lp.lineTo(A4[0] - 10, 538 - dup_const)
    lp.close()
    c.drawPath(lp)

    # condition text
    rdate = Paragraph(
        text="Donor Signature:____________________________________________",
        style=styles['cond'])
    rdate.wrapOn(canv=c, aW=A4[0] - 60, aH=A4[1])
    rdate.drawOn(canvas=c, x=40, y=495 - dup_const)


    rdate = Paragraph(
        text="Receipt Sent By: 1.Email   2. Handover   3. Other   _____________",
        style=styles['cond'])
    rdate.wrapOn(canv=c, aW=A4[0] - 60, aH=A4[1])
    rdate.drawOn(canvas=c, x=40, y=470 - dup_const)
    
    c.save()
    return True
