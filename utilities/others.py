# -*- coding: utf-8 -*-

import time

import pandas as pd
import psycopg2
from flask import render_template, request, json
from numpy import NaN
from psycopg2.extras import RealDictCursor
from xlrd import open_workbook

from appholder import celery_app, db
import config
from appholder import db, application as app

from main import sendbulkemails,sendmailmandrill
from celery_tasks.others import clear_and_update_voter
from celery_tasks.sms_email import  send_instructionsmail_queue,sendSMS
from dbmodels import Janasena_Missedcalls,Memberships
from utilities.classes import Membership_details, encrypt_decrypt,Voter_details
import requests
from utilities.sms import send_bulk_sms_same_message,sendBULKSMS_samemessage
from id_card import id_card_func
from utilities.general import create_hashid
import logging
logger = logging.getLogger(__name__)

@celery_app.task
def insert_new_mem():

    con = psycopg2.connect(config.DB_CONNECTION)
    df = pd.read_excel("static/assets/After_2019-01-04_JSPVZM.xls")



    with con.cursor() as cur:
        for index,item in df.iterrows():
            print(index)
            name = (str(item['FirstName']) if str(item['FirstName']).lower() != 'nan' else '') +' '+(str(item['LastName']) if str(item['LastName']).lower() != 'nan' else '')
            phone = str(item['PhoneNo']) if str(item['PhoneNo']).lower() != 'nan' else None
            voter_id = str(item['VoterId']) if str(item['VoterId']).lower() != 'nan' else None

            address = ''
            town = str(item['Village']) if str(item['Village']).lower() != 'nan' else None
            age = str(item['Age']) if str(item['Age']) != 'NaN' else None
            gender = str(item['Gender']) if str(item['Gender']).lower() != 'nan' else None
            assembly_id = str(item['Constituency']) if str(item['Constituency']).lower() != 'nan' else None
            email = str(item['Email']) if str(item['Email']).lower() != 'nan' else None
            referral_id = str(item['Refered/Added by JSP_ID']) if str(item['Refered/Added by JSP_ID']).lower() != 'nan' else None
            membership_through = 'O'
            state=1

            cur.execute("""
                select part_no,section_name,relation_name,relation_type
                from voters
                where voter_id=%s
            """,(str(item['VoterId']) if str(item['VoterId']) != 'NaN' else '',))
            row = cur.fetchone()
            if row is not None:
                booth = row[0]
                booth_name = row[1]
                relation_name = row[2]
                relation_type = row[3]
                membership_through = 'O'
            else:
                booth = 0
                booth_name= ''
                relation_name = ''
                relation_type = ''
                membership_through = 'O'
            print("hello")
            generate_membershipid(name, phone, voter_id, relation_name, relation_type, address, town, age, gender, booth,
                                  assembly_id, state, email, booth_name, referral_id, membership_through,
                                  country_name='india')
@celery_app.task
def send_membership_sms1(data):
    jsp_membership_id = data['membership_id']
    numberToSend = data['phone_number']
   
    countries_allowed = ["91"]
    if numberToSend.startswith(tuple(countries_allowed)):
        seq_id=data['seq_id']
        hashed_id=create_hashid(seq_id)
        msgToSend = "Hi,Your JanaSena Membership ID is " + str(jsp_membership_id) + ". Download Membership Card using this link https://janasenaparty.org/card/"+str(hashed_id)+ " or using app tiny.cc/9auvty"
        
        response=send_offline_SMS(numberToSend, msgToSend)
        res_text=str(response.text).split(":")
        if len(res_text)>1:
            job_id=res_text[1]
            try:
                con = psycopg2.connect(config.DB_CONNECTION)
                cur = con.cursor(cursor_factory=RealDictCursor)
                cur.execute("update member_fields set job_id=%s where membership_id=%s",(job_id,jsp_membership_id))
                con.commit()
                con.close()
            except Exception as e:
                print(str(e))


@celery_app.task
def send_membership_card_email(email, jsp_membership_id,member_name):
    try:

        membership_card_link = "https://janasenaparty.org/getMembershipCard?id=" + str(jsp_membership_id)
        membershipcard_short_link = "<a href=" + str(membership_card_link) + ">CLick here</a>"
        if email != '':
            subject = " Janasena Party | Congratulations "
            name = "JANASENA PARTY"
            message = message = open("./instructions_template.html").read()
            temp_text = str(message)
            temp_text = temp_text.replace("{{membership_id}}", str(jsp_membership_id))
            temp_text = temp_text.replace("{{membership_card_link}}", str(membershipcard_short_link))
            id_card_file=[id_card_func(member_name,jsp_membership_id)]
            sendmailmandrill(name, email, temp_text, subject,attachments=id_card_file)


    except Exception as e:
        print(str(e))
        # print "Exception occured in "
def send_offline_SMS(numberToSend, msgToSend):
    try:
        url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx"

        querystring = {"User": "JSPOFFLINE", "passwd": "JSP@@789",
                       "mobilenumber": numberToSend, "message": msgToSend, "sid": "JAIJSP", "mtype": "N", "DR": "Y"}
        headers = {}

        response = requests.request("GET", url, headers=headers, params=querystring)
        return response
    except Exception as e:
        print(str(e))



def generate_membershipid(name, phone, voter_id, relation_name, relation_type, address, town, age, gender, booth,
                          assembly_id, state, email, booth_name, referral_id, membership_through, country_name='india'):
    try:
        con = psycopg2.connect(app.config["DB_CONNECTION"])
        cur = con.cursor(cursor_factory=RealDictCursor)
        try:
            n_voter_id = voter_id
            n_phone = phone
            phone = encrypt_decrypt(phone, 'E')
            if voter_id != '':
                voter_id = encrypt_decrypt(voter_id, 'E')
            e_email = ''
            # print email
            if email is not None and email != '':
                e_email = encrypt_decrypt(email, 'E')
        except Exception as e:
            print(str(e))

        user_status = ''

        existing = db.session.query(Janasena_Missedcalls.jsp_supporter_seq_id).filter(
            Janasena_Missedcalls.phone == n_phone).first()

        if existing is None:
            newRecord = Janasena_Missedcalls(phone=n_phone, member_through='W')
            db.session.add(newRecord)
            db.session.commit()

            member_id = newRecord.jsp_supporter_seq_id
            user_status = 'F'

        else:

            membership_instance = Membership_details()
            if membership_instance.check_phonenumber_existence(phone):

                user_status = 'E'
                member_id = existing[0]

            else:
                newRecord = Janasena_Missedcalls(phone=n_phone, member_through='W')
                db.session.add(newRecord)
                db.session.commit()
                member_id = newRecord.jsp_supporter_seq_id
                user_status = 'F'
        jsp_membership_id = "JSP" + str(member_id).zfill(8)

        if state == 'AP':
            state_id = 1
        elif state == 'TS':
            state_id = 2
        elif state=='KA':
            state_id = 3
        elif state=='TN':
            state_id=4
        else:
            state_id=5

        cur.execute("select district_id from assembly_constituencies where state_id =%s and constituency_id=%s",
                    (state_id, assembly_id))
        row = cur.fetchone()

        district_id = 0
        
        if row is not None:
            district_id = row['district_id']
        cur.execute(
            "insert into janasena_membership (name,phone,voter_id,relationship_name,relation_type,address,village_town,age,gender,polling_station_id,constituency_id,state,email,polling_station_name,referral_id,membership_through,membership_id,state_id,district_id,country)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) returning jsp_membership_seq_id ",
            (name, phone, voter_id, relation_name, relation_type, address, town, age, gender, booth, assembly_id, state,
             e_email, booth_name, str(referral_id).upper(), membership_through, jsp_membership_id, state_id,
             district_id, country_name))
        seq_row=cur.fetchone()
        seq_id=seq_row['jsp_membership_seq_id']
        con.commit()
        con.close()

        if config.ENVIRONMENT_VARIABLE == 'local':
            clear_and_update_voter(n_voter_id, n_phone, jsp_membership_id, str(referral_id).upper())
        else:

            clear_and_update_voter.delay(n_voter_id, n_phone, jsp_membership_id, str(referral_id).upper())
        
        try:
            numberToSend = str(n_phone)

            if config.ENVIRONMENT_VARIABLE == 'local':
                
                pass
                # send_membership_sms1(
                #     {"phone_number": numberToSend, "membership_id": jsp_membership_id, 'email': email,'seq_id':seq_id})
                

            else:
                
                send_membership_sms1.delay({"phone_number": numberToSend, "membership_id": jsp_membership_id,'seq_id':seq_id})



        except Exception as e:
            print(str(e))
        if email is not None and email != '':
            if config.ENVIRONMENT_VARIABLE == 'local':
                pass
                
              # send_membership_card_email(email, jsp_membership_id,name)
            else:
                
                send_membership_card_email.delay(email, jsp_membership_id,name)
        return (True, jsp_membership_id)
    except Exception as e:
        print(str(e))
        return (False, 'Exception')


def generate_membershipid1(name, phone, voter_id, relation_name, relation_type, address, town, age, gender, booth,
                          assembly_id, state, email, booth_name, referral_id, membership_through, country_name='india'):
    try:
        con = psycopg2.connect(app.config["DB_CONNECTION"])
        cur = con.cursor(cursor_factory=RealDictCursor)
        try:
            n_voter_id = voter_id
            n_phone = phone
            phone = encrypt_decrypt(phone, 'E')
            if voter_id != '':
                voter_id = encrypt_decrypt(voter_id, 'E')
            e_email = ''
            # print email
            if email is not None and email != '':
                e_email = encrypt_decrypt(email, 'E')
        except Exception as e:
            print(str(e))

        user_status = ''

        existing = db.session.query(Janasena_Missedcalls.jsp_supporter_seq_id).filter(
            Janasena_Missedcalls.phone == n_phone).first()

        if existing is None:
            newRecord = Janasena_Missedcalls(phone=n_phone, member_through='W')
            db.session.add(newRecord)
            db.session.commit()

            member_id = newRecord.jsp_supporter_seq_id
            user_status = 'F'

        else:

            membership_instance = Membership_details()
            if membership_instance.check_phonenumber_existence(phone):

                user_status = 'E'
                member_id = existing[0]

            else:
                newRecord = Janasena_Missedcalls(phone=n_phone, member_through='W')
                db.session.add(newRecord)
                db.session.commit()
                member_id = newRecord.jsp_supporter_seq_id
                user_status = 'F'
        jsp_membership_id = "JSP" + str(member_id).zfill(8)

        if state == 'AP':
            state_id = 1
        elif state == 'TS':
            state_id = 2
        elif state=='KA':
            state_id = 3
        elif state=='TN':
            state_id=4
        else:
            state_id=5

        cur.execute("select district_id from assembly_constituencies where state_id =%s and constituency_id=%s",
                    (state_id, assembly_id))
        row = cur.fetchone()

        district_id = 0
        
        if row is not None:
            district_id = row['district_id']
        cur.execute(
            "insert into janasena_membership (name,phone,voter_id,relationship_name,relation_type,address,village_town,age,gender,polling_station_id,constituency_id,state,email,polling_station_name,referral_id,membership_through,membership_id,state_id,district_id,country)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) returning jsp_membership_seq_id ",
            (name, phone, voter_id, relation_name, relation_type, address, town, age, gender, booth, assembly_id, state,
             e_email, booth_name, str(referral_id).upper(), membership_through, jsp_membership_id, state_id,
             district_id, country_name))
        seq_row=cur.fetchone()
        seq_id=seq_row['jsp_membership_seq_id']
        con.commit()
        con.close()

        if config.ENVIRONMENT_VARIABLE == 'local':
            clear_and_update_voter(n_voter_id, n_phone, jsp_membership_id, str(referral_id).upper())
        else:

            clear_and_update_voter(n_voter_id, n_phone, jsp_membership_id, str(referral_id).upper())
        
        try:
            numberToSend = str(n_phone)

            if config.ENVIRONMENT_VARIABLE == 'local':
                pass
                
                # send_membership_sms1(
                #     {"phone_number": numberToSend, "membership_id": jsp_membership_id, 'email': email,'seq_id':seq_id})
                

            else:
                
                send_membership_sms1.delay({"phone_number": numberToSend, "membership_id": jsp_membership_id,'seq_id':seq_id})



        except Exception as e:
            print(str(e))
        if email is not None and email != '':
            if config.ENVIRONMENT_VARIABLE == 'local':
                
               send_membership_card_email(email, jsp_membership_id,name)
            else:
                
                send_membership_card_email.delay(email, jsp_membership_id,name)
        return (True, jsp_membership_id)
    except Exception as e:
        print(str(e))
        return (False, 'Exception')



def generate_membershipid_whatsappbot(name, phone, voter_id, relation_name, relation_type, address, town, age, gender, booth,
                          assembly_id, state, email, booth_name, referral_id, membership_through, country_name='india'):
    try:
        con = psycopg2.connect(app.config["DB_CONNECTION"])
        cur = con.cursor(cursor_factory=RealDictCursor)
        try:
            n_voter_id = voter_id
            n_phone = phone
            phone = encrypt_decrypt(phone, 'E')
            if voter_id != '':
                voter_id = encrypt_decrypt(voter_id, 'E')
            e_email = ''
            # print email
            if email is not None and email != '':
                e_email = encrypt_decrypt(email, 'E')
        except Exception as e:
            print(str(e))

        user_status = ''

        existing = db.session.query(Janasena_Missedcalls.jsp_supporter_seq_id).filter(
            Janasena_Missedcalls.phone == n_phone).first()

        if existing is None:
            newRecord = Janasena_Missedcalls(phone=n_phone, member_through='W')
            db.session.add(newRecord)
            db.session.commit()

            member_id = newRecord.jsp_supporter_seq_id
            user_status = 'F'

        else:

            membership_instance = Membership_details()
            if membership_instance.check_phonenumber_existence(phone):

                user_status = 'E'
                member_id = existing[0]

            else:
                newRecord = Janasena_Missedcalls(phone=n_phone, member_through='W')
                db.session.add(newRecord)
                db.session.commit()
                member_id = newRecord.jsp_supporter_seq_id
                user_status = 'F'
        jsp_membership_id = "JSP" + str(member_id).zfill(8)

        if state == 'AP':
            state_id = 1
        elif state == 'TS':
            state_id = 2
        else:
            state_id = 3
        cur.execute("select district_id from assembly_constituencies where state_id =%s and constituency_id=%s",
                    (state_id, assembly_id))
        row = cur.fetchone()

        district_id = 0
        
        if row is not None:
            district_id = row['district_id']
        cur.execute(
            "insert into janasena_membership (name,phone,voter_id,relationship_name,relation_type,address,village_town,age,gender,polling_station_id,constituency_id,state,email,polling_station_name,referral_id,membership_through,membership_id,state_id,district_id,country)values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ",
            (name, phone, voter_id, relation_name, relation_type, address, town, age, gender, booth, assembly_id, state,
             e_email, booth_name, str(referral_id).upper(), membership_through, jsp_membership_id, state_id,
             district_id, country_name))

        con.commit()
        con.close()

        if config.ENVIRONMENT_VARIABLE == 'local':
            clear_and_update_voter(n_voter_id, n_phone, jsp_membership_id, str(referral_id).upper())
        else:

            clear_and_update_voter(n_voter_id, n_phone, jsp_membership_id, str(referral_id).upper())
        
        return (True, jsp_membership_id)
    except Exception as e:
        print(str(e))
        return (False, 'Exception')


def makeResponse(content, statuscode, mimetipe=None):
    return app.response_class(response=content,
                              status=statuscode,
                              mimetype=mimetipe)


def give_cursor_html():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
    except psycopg2.OperationalError as e:
        # print e.pgerror
        return render_template('500.html')
    try:
        cur = con.cursor()
        if request.method =='POST':
            pass

    except psycopg2.Error as e:
        # print e.message
        return render_template('500.html')
    finally:
        con.close()

def give_cursor_json(temp):
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
    except psycopg2.OperationalError as e:
        # print e.pgerror
        return json.dumps({'errors':['CONNECTION ERROR OCCURED']})
    try:
        cur = con.cursor()
        return temp(con, cur)

    except psycopg2.Error as e:
        # print e.message,"Exception"
        # print 'I m here'
        return json.dumps({'errors': ['EXCEPTION OCCURED']})
    except Exception as e:
        # print e.message, "Exception"
        return json.dumps({'errors':['EXCEPTION OCCURRED']})
    finally:
        con.close()



@celery_app.task
def random_membership():
    con = psycopg2.connect(config.DB_CONNECTION)
    cur = con.cursor(cursor_factory=RealDictCursor)
    
    book = open_workbook('Vizianagaram.xlsx')
    sheet = book.sheet_by_index(0)
    
    district_ids = {}
    count=0
    # print sheet.nrows
    for row_index in xrange(1, sheet.nrows):

        try:
            mobile = sheet.cell(row_index, 3).value
            
            assembly_name = sheet.cell(row_index, 1).value
            name = str(sheet.cell(row_index, 2).value)

            tn = int(mobile)
        except Exception as e:
            # print e
            tn = 0
            continue
        tn = str(tn)
       
        if tn != '0':
            # print tn
            encrypted_mobile = encrypt_decrypt(str(tn), 'E')
            cur.execute('select phone,name from janasena_membership where phone=%s', (encrypted_mobile,))
            mobile_row = cur.fetchone()
            # print mobile_row
            
            if  mobile_row is None:
                count=count+1
                # print count

                assembly_name=assembly_name.lower()
                cur.execute(
                    'select constituency_id,district_id,state_id from assembly_constituencies where lower(constituency_name)=%s',
                    (assembly_name,))
                row = cur.fetchone()
                
                if row:
                   
                    constituency_id = row['constituency_id']
                else:
                   constituency_id=1 
                
                state = 'AP'
                
                voter_id=''
                relation_name=''
                relation_type=''
                address=''
                town=''
                age=00
                gender=''
                booth=0
                assembly_id=constituency_id
                state='AP'
                email=''
                booth_name=''
                referral_id=''
                membership_through='S'

                # print str(name)
                status,membership_id=generate_membershipid(name, tn, voter_id, relation_name, relation_type, address, town, age, gender, booth,
                          assembly_id, state, email, booth_name, referral_id, membership_through, country_name='india')
                cur.execute("update janasena_membership set member_associations=%s,language=%s where membership_id=%s",
                        ('Youth Wing', 'Telugu', membership_id))
                con.commit()
                # print membership_id

    con.close()

@celery_app.task
def missedcalls_membership_sms():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        
        for offset in range(1,200000,10000):
            cur.execute("select mi.phone,m.membership_id from janasena_membership m join janasena_missedcalls mi on mi.supporter_id=m.membership_id where m.membership_through='MC' order by m.create_dttm limit 10000 offset %s",(offset,))
            rows=cur.fetchall()
            for row in rows:
                numberToSend=row['phone']
                membership_id=row['membership_id']
                message="Hello, Your JanaSena Party Membership ID:" + str(membership_id) + ". Download your e-membership card from here bit.ly/2IvvYhG"
                # print numberToSend
                # print message
                #send_bulk_sms_same_message(msgToSend, [numberToSend])

    except (psycopg2.Error,Exception) as e:
        print(str(e))        

@celery_app.task
def missedcalls_membership():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        offsets=[30000,50000,70000,90000,110000,130000,150000,170000,190000]
        for offset in offsets:
            cur.execute("select phone,name,address,relation_name,state_id,constituency_id from missedcalls_data where constituency_id is not null order by seq_id limit 20000 offset %s",(offset,))
            rows=cur.fetchall()
            for row in rows:

                phone=row['phone']
                name=row['name']
                address=row['address']
                relationship_name=row['relation_name']
                state_id=row['state_id']
                constituency_id=row['constituency_id']
                encrypted_mobile = encrypt_decrypt(str(phone), 'E')
                cur.execute('select phone,name from janasena_membership where phone=%s', (encrypted_mobile,))
                mobile_row = cur.fetchone()
                   
                if  mobile_row is None:
                    state='AP'

                    if state_id ==1:

                    
                        state = 'AP'
                    else:
                        state='TS'
                    
                    voter_id=''
                    relation_name=relationship_name
                    relation_type=''
                    address=address
                    town=''
                    age=00
                    gender=''
                    booth=0
                    assembly_id=constituency_id
                    
                    email=''
                    booth_name=''
                    referral_id=''
                    membership_through='MC'
                    try:
                        
                        status,membership_id=generate_membershipid(name, phone, voter_id, relation_name, relation_type, address, town, age, gender, booth,
                                  assembly_id, state, email, booth_name, referral_id, membership_through, country_name='india')
                        
                        # print membership_id
                    except Exception as e:
                        print(str(e))
                        continue

        con.close()
    except (psycopg2.Error,Exception) as e:
        print(str(e))
def write_voters_count():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()

        cur.execute("select state_id,constituency_name,constituency_id from assembly_constituencies where state_id<=2 order by constituency_id ")
        a_rows =cur.fetchall()
        
        for row in a_rows:
            assembly=row[2]
            state_id=row[0]
            
            cur.execute(
                "select state,ac_no,sex,count (*) from voters group by sex,ac_no,state having  ac_no=%s and state=%s",
                (assembly, state_id))
            rows = cur.fetchall()
            total_voters = 0
            male_voters = 0
            female_voters = 0

            for v_row in rows:
                total_voters = total_voters + v_row[3]
                
                if str(v_row[2]).rstrip() == 'Male':
                    male_voters = male_voters + v_row[3]
                elif str(v_row[2]).rstrip() == 'Female':
                    female_voters = female_voters + v_row[3]
            # print total_voters, male_voters, female_voters
            cur.execute(
                "update assembly_constituencies set total_votes=%s,male_votes=%s,female_votes=%s where constituency_id=%s and state_id=%s",
                (total_voters, male_voters, female_voters, assembly, state_id))
            con.commit()

        con.commit()
        con.close()
    except (psycopg2.Error, Exception) as e:
        con.close()
        print(str(e))
def write_district_count():
    try:

        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()

        cur.execute("select district_name from districts where state_id<=2 ")
        prows = cur.fetchall()
        for row in prows:
            district_name = str(row[0]).rstrip()

            # print district_name
            cur.execute(
                "select total_votes,male_votes,female_votes from assembly_constituencies where district_name=%s",
                (district_name,))
            crows = cur.fetchall()
            # print crows
            total_voters = 0
            male_voters = 0
            female_voters = 0

            for item in crows:
                total_voters = total_voters + item[0]

                male_voters = male_voters + item[1]

                female_voters = female_voters + item[2]
            # print total_voters, male_voters, female_voters
            cur.execute(
                "update districts set total_votes=%s,male_votes=%s,female_votes=%s where district_name=%s ",
                (total_voters, male_voters, female_voters, district_name))
            con.commit()

        con.commit()
        con.close()


    except Exception as e:
        print(str(e))
    return 'True'

@celery_app.task(name="missedcalls_mapping_task")
def missedcalls_mapping_task():
    try:
        logger.info("executing missedcalls_mapping_task")
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("""update janasena_missedcalls set supporter_id='JSP'||lpad(cast(jsp_supporter_seq_id as text),8,'0') where supporter_id is null""")    
        con.commit()
        cur.execute("update janasena_membership set mandal_id=polling_booths_2018_jan.mandal_id from polling_booths_2018_jan where janasena_membership.state_id=polling_booths_2018_jan.state_id and janasena_membership.constituency_id=polling_booths_2018_jan.constituency_id and janasena_membership.polling_station_id=polling_booths_2018_jan.part_no and janasena_membership.state_id=1  and janasena_membership.mandal_id is null and  janasena_membership.polling_station_id!=0 ")
        con.commit()
        cur.execute("update janasena_membership set mandal_id=cast(member_fields.mandal as int) from member_fields where member_fields.membership_id=janasena_membership.membership_id and member_fields.mandal!='' and janasena_membership.mandal_id is null")
        con.commit()
        cur.execute("select distinct(jm.phone) from janasena_missedcalls  jm left join janasena_membership m on m.membership_id=jm.supporter_id where m.membership_id is null and jm.member_through in ('M','JT') and date(jm.create_dttm AT TIME ZONE 'Asia/Calcutta')=current_date ")
        missed_rows=cur.fetchall()
        mapped_count= map_existing(missed_rows)
              
        con.close()
        #send_active_link()
        
        
        
        
        return "success"
    except (psycopg2.Error,Exception) as e:
        print(str(e))
        return str(e)
# def send_active_link():
#     try:
#         print "hello"
#         con = psycopg2.connect(config.DB_CONNECTION)
#         cur = con.cursor(cursor_factory=RealDictCursor)
#         cur.execute("select seq_id,name,mobile from resource_persons_data")
#         rows=cur.fetchall()
#         print len(rows)
#         template="""ప్రియమైన జనసైనికులారా 

# నయ వంచనల ప్రభుత్వాన్ని కూలదోసే సమయం ఆసన్నమైంది, 

# అవినీతి మకిలి అంటి, బాధ్యత మరచిన విపక్షాన్ని నిలదీసే సమయం ఆసన్నమైంది ,

# పేదల జీవితాల్లో వెలుగులు నింపే సమయం ఆసన్నమైంది,
#  సమాజంలో ఒక గొప్ప మార్పు తీసుకు వచ్చే సమయం ఆసన్నమైంది, 

# ఎన్నికల రణక్షేత్రంలో యోధుడిలా పోరాడుతున్నా... నాతో కలిసి నడిచే సమయం ఆసన్నమైంది.                 

#                             మీ జనసేనాని -పవన్ కళ్యాణ్

# రా సైనికా .. కదలిరా సైనికా .. జనసేనానితో అడుగువేయ్..
# మీరు కూడా జనసేనాని తో పోరాడటానికి సిద్ధంగావుంటే ఈ క్రింది ఫారం ని ఫిల్ చెయ్యండి {{link}}
#              """
#         for row in rows:
#             try:
                
#                 mobile = row['mobile']
                
#                 seq_id=create_hashid(row['seq_id'])
                
#                 if len(mobile)==10:
                   
#                     numberToSend = "91" + str(mobile)
#                 else:
#                     numberToSend=mobile

#                 print mobile   
#                 msgToSend=template
#                 link="https://janasenaparty.org/jsp_active_volunteer/"+str(seq_id)
#                 msgToSend=msgToSend.replace("{{link}}",link)
#                 contacts=[]
#                 contacts.append(numberToSend)
                
#                 print msgToSend
#                 sendBULKSMS_samemessage( msgToSend,contacts,language='unicode')   
#                 #sendSMS(numberToSend, msgToSend)
#             except Exception as e:
#                 print(str(e))
#                 continue
#         sendBULKSMS_samemessage( msgToSend,["919676881991"],language='unicode')
#         return "sent successfully"

#     except (psycopg2.Error, Exception) as e:
#         print(str(e))
#         return str(e)

@celery_app.task
def send_memebrship_update_msg(pc_id):
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("""select m.jsp_membership_seq_id,m.phone from janasena_membership m join assembly_constituencies ac
 on ac.state_id=m.state_id and ac.constituency_id=m.constituency_id 
 where ac.state_id=1 and m.country='india' and m.polling_station_id=0 and ac.parliament_constituency_id=%s order by m.jsp_membership_seq_id """,(pc_id,))
        rows=cur.fetchall()
       
        template="""జనసేన పార్టీకి సంబంధించిన సమాచారం కొరకు మీ జనసేన సభ్యత్వ వివరాలు ఈ లింక్ {{link}} లో పొందుపరచండి """
        count=0
        data=[]
        for row in rows:
            try:
                d={}
               
                mobile = encrypt_decrypt(row['phone'],'D')

                seq_id=create_hashid(row['jsp_membership_seq_id'])
                if len(mobile)==10:
                   
                    numberToSend = "91" + str(mobile)
                else:
                    numberToSend=mobile
                long_url="janasenaparty.org/memb/"+str(seq_id)
                url=shurl(long_url)
                
                msgToSend=template.encode()
              
                msgToSend=msgToSend.replace("{{link}}",str(url))
                
                count=count+1
                # print count
                
                sendSMS(numberToSend, msgToSend)
            except Exception as e:
                continue
        
        
        sendSMS("919676881991", msgToSend)
        return "sent successfully"

    except (psycopg2.Error, Exception) as e:
        print(str(e))
        return str(e)
    # con = psycopg2.connect(config.DB_CONNECTION)
    # cur = con.cursor(cursor_factory=RealDictCursor)
    # try:
        
    #     cur.execute("select jsp_supporter_seq_id,phone from janasena_missedcalls where supporter_id is null and date(create_dttm)=current_date ")
    #     rows=cur.fetchall()
    #     count=0
    #     for row in rows:
    #         seq_id=row['jsp_supporter_seq_id']
    #         phone=row['phone']
    #         membership_id= "JSP" + str(seq_id).zfill(8)
    #         try:
    #             cur.execute("update janasena_missedcalls set supporter_id=%s where phone=%s and jsp_supporter_seq_id=%s ",(membership_id,phone,seq_id))
    #             con.commit()
    #             count=count+1
    #             print count
    #         except (psycopg2.Error,Exception) as e:
    #             print(str(e))
    #             cur.execute("delete from janasena_missedcalls where jsp_supporter_seq_id=%s",(seq_id,))
    #             con.commit()
    #             print "deleted"
    #             continue
    #     con.close()
    # except (psycopg2.Error,Exception) as e:
    #     con.close()
    #     print(str(e))
    #     return str(e)

def remove_duplicates():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select distinct(jm.phone),jm.supporter_id from janasena_missedcalls  jm left join janasena_membership m on m.membership_id=jm.supporter_id where m.membership_id is null and jm.member_through='M'")
        rows=cur.fetchall()
        count=0
        for row in rows:

            phone=row['phone']
            membership_id=row['supporter_id']
            ephone=encrypt_decrypt(phone,'E')
            cur.execute("select membership_id,membership_through from janasena_membership where phone=%s ",(ephone,))
            existing=cur.fetchone()
            if existing is not None:
                count=count+1
                cur.execute("update janasena_missedcalls set member_status='D' where supporter_id=%s",(membership_id,))

                con.commit()
                # print count
                # print phone,existing['membership_through']
        return "done"
    except Exception as e:
        print(str(e))
        return ''



def map_existing(missed_rows):
    try:
        
        import urllib.parse as urlparse
        DATABASE_LIVE_URL = 'postgresql://postgres:JDatabase)(passwe@10.139.72.246:6432/janasena_test'
        url = urlparse(DATABASE_LIVE_URL)
        
        APP_NAME = url.path[1:]
        DB_USER = url.username
        DB_PASSWORD = url.password
        DB_HOST = url.hostname
        DB_PORT = url.port
        
        DB_CONNECTION = "host='{0}' port='{1}' dbname='{2}' user='{3}' password='{4}'".format(DB_HOST, DB_PORT, APP_NAME,DB_USER, DB_PASSWORD)
        
        test_con = psycopg2.connect(DB_CONNECTION)
        test_cur = test_con.cursor(cursor_factory=RealDictCursor)
        
        mapped_count=0
       
        for m_row in missed_rows:
            phone=m_row['phone']
            
            if len(str(phone))==12:
              
                mobile=phone[2:12]
            else:
                mobile=str(phone)

            test_cur.execute("select c.name,c.address,c.pincode from contacts c  where c.phone_number=%s",(mobile,))
            result=test_cur.fetchone()
            
            
            if result is not None:
                try:


                    pincode=str(result['pincode']).strip()
                    test_cur.execute("select distinct on (pincode)state_id as state,constituency_id from pincodes  where pincode=%s",(pincode,))
                    mapped_row=test_cur.fetchone()
                    if mapped_row is not None:
                        mapped_count=mapped_count+1
                        name=result['name']
                        
                        constituency_id=mapped_row['constituency_id']
                        state=mapped_row['state']
                        address=result['address']
                        if state==1:
                            state='AP'
                        else:
                            state='TS'
                        
                        status,membership_id=generate_membershipid(name=name,voter_id='',state=state,
                              age=0,assembly_id=constituency_id,membership_through='MC',
                              gender='',address=address,town='',booth=0,booth_name=None,
                              email=None,phone=phone,relation_name=None,relation_type=None,referral_id='')
                        # print status,membership_id
                except (psycopg2.Error,Exception) as e:
                    print(str(e))
        test_con.close()
        return mapped_count
        

    except Exception as e:
        print(str(e))
        return False


# def map_existing(missed_rows):
#     try:
        
#         from urlparse import urlparse
#         DATABASE_LIVE_URL = 'postgresql://postgres:JDatabase)(passwe@10.139.72.246:6432/janasena_test'
#         url = urlparse(DATABASE_LIVE_URL)
        
#         APP_NAME = url.path[1:]
#         DB_USER = url.username
#         DB_PASSWORD = url.password
#         DB_HOST = url.hostname
#         DB_PORT = url.port
        
#         DB_CONNECTION = "host='{0}' port='{1}' dbname='{2}' user='{3}' password='{4}'".format(DB_HOST, DB_PORT, APP_NAME,DB_USER, DB_PASSWORD)
        
#         test_con = psycopg2.connect(DB_CONNECTION)
#         test_cur = test_con.cursor(cursor_factory=RealDictCursor)
        
#         mapped_count=0
#         test_cur.execute("select c.phone_number,c.name,c.address,c.state_id as state,c.constituency_id from contacts c  where c.district_id is not null order by c.district_id")
#         missed_rows=test_cur.fetchall()
            
#         for mapped_row in missed_rows:
           
#             try:

#                 phone="91"+mapped_row['phone_number']
#                 mapped_count=mapped_count+1
#                 name=mapped_row['name']
                
#                 constituency_id=mapped_row['constituency_id']
#                 state=mapped_row['state']
#                 address=mapped_row['address']
#                 if state==1:
#                     state='AP'
#                 else:
#                     state='TS'

#                 status,membership_id=generate_membershipid1(name=name,voter_id='',state=state,
#                       age=0,assembly_id=constituency_id,membership_through='MC',
#                       gender='',address=address,town='',booth=0,booth_name=None,
#                       email=None,phone=phone,relation_name=None,relation_type=None,referral_id='')
#                 print status,membership_id
#             except (psycopg2.Error,Exception) as e:
#                 print(str(e))
#         test_con.close()
#         return mapped_count
        

#     except Exception as e:
#         print(str(e))
#         return False

import xlrd

# from offline_memberships.views import update_member_other_details
# from utilities.classes import Membership_details, encrypt_decrypt
# from utilities.others import generate_membershipid

def update_excel1():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        json_file = "./active_members.json"

        json_data = open(json_file).read()

        data = json.loads(json_data)
        count=0
        for row in data:
            
            phone=row['From']
            message_body=row['Message']
            try:
                cur.execute("select * from active_volunteers where phone=%s",(phone,))
                existing=cur.fetchone()
                if existing is None:
                    cur.execute("insert into active_volunteers(phone,message_body)values(%s,%s)",(phone,message_body))
                    con.commit()
                    count=count+1
                    # print count
            except (psycopg2.Error,Exception) as e:
                print(str(e))
                continue
        con.close()
        
    except Exception  as e:
        print(str(e))
from main import shurl        
def memberships_upload():

    
    try:
        json_file = "./mydukuru.json"

        json_data = open(json_file).read()

        data = json.loads(json_data)
        #data=data['data']
        # print len(data)
        count=0
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        missed=[]
        constituency_missed_count=0
        missed_constittuencies=[]
        for record in data:
            count=count+1
            
            phone=str(record['phone'])
            name=record['name']
            gender=record['gender']
            address=''
            state='AP'
            constituency_id=133
            voter_id=record['voter_id']
            age=record['age']
                
                
            referral_id=record['referral_id']
            email=''
            relation_name=record['relation_name']

            house_no=record['h_no']
            state_id="1"
            fb_id=''
            village=''
            mandal=''
            education=''
            occupation=''
            pincode=''
            receipt_no=0
            submitted_by=0
            if len(phone)==10:
                mobile="91"+str(phone)
            emobile = encrypt_decrypt(phone, 'E')
            membership_instance = Membership_details()
            emobile=encrypt_decrypt(phone,'E')
            booth_id=record['polling_station_id']
            if booth_id is None or booth_id=='':
                booth_id=0
            # print emobile
            if membership_instance.check_phonenumber_count(emobile):
                if voter_id is not None and voter_id!='':
                    evoter_id=encrypt_decrypt(voter_id,'E')
                    if membership_instance.check_voterid_existence(evoter_id):

                        voterdetails_instance = Voter_details()
                        if voterdetails_instance.check_existence(str(voter_id).lower()):
                            membership_through='OFV'

                        else:
                            membership_through='OFNF' 

                    else:
                        

                        continue

                            
                else:
                    membership_through='OFW'

                if referral_id is not None and referral_id!='':
                    referral_id=referral_id.upper()
                    if not membership_instance.check_membership_existence(referral_id):
                        continue

                if str(state_id) == '1':
                    state = "AP"
                elif str(state_id) == '2':
                    state = "TS"
                else:
                    state = "KA"
                # print 'cp1111'
                status, membership_id = generate_membershipid1(name, phone, voter_id, relation_name, '', house_no, address, age, gender, booth_id,constituency_id, state, email, '', referral_id, membership_through, country_name='india')
                if status:
                    # print membership_id
                    update_member_other_details(membership_id,fb_id,village,mandal,education,occupation,pincode,receipt_no,submitted_by)
                
                # print status,membership_id
            
        
        return "success"
    except Exception as e:
        print(str(e))

def update_booth_ids():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select voter_id from janasena_membership where state_id=1 and district_id!=10 and voter_id !=''")
        rows=cur.fetchall()
        # print len(rows)
        count=0
        for row in rows:
            count=count+1
            # print count
            e_voter_id=row['voter_id']
            voter_id=encrypt_decrypt(str(e_voter_id),'D')
            cur.execute("select part_no from voters where voter_id=%s",(voter_id,))
            v_row=cur.fetchone()
            if v_row is not None:
                polling_station_id=v_row['part_no']
                cur.execute("update janasena_membership set polling_station_id=%s where voter_id=%s",(polling_station_id,e_voter_id,))
                con.commit()
        con.close()
        return 'success'

    except (psycopg2.Error,Exception )as e:
        print(str(e))
        return 'false'



def update_empty_booth_names():
    try:
        empty_booths = "./empty_booths.json"

        json_data = open(empty_booths).read()

        data = json.loads(json_data)
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        count=0
        for record in data:
            count=count+1
            # print count
            state_id=record['state_id']
            constituency_id=record['constituency_id']
            booth_id=record['polling_station_no']
            ps_name=record['polling_station_name']
            try:
                cur.execute("update polling_station_details set polling_station_name=%s where state_id=%s and constituency_id=%s and  polling_station_no=%s",(ps_name,state_id,constituency_id,booth_id))
                con.commit()
                con.close()
            except Exception as e:
                continue

        return 'success'
    except (psycopg2.Error,Exception )as e:
        print(str(e))
        return 'false'

def update_mapped_members():
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor(cursor_factory=RealDictCursor)
        cur.execute("select phone,member,age,constituency_id from mandal_wise_cadre where membership_id is null")
        members_rows=cur.fetchall()

       
        
        for record in members_rows:
            phone=record['phone']
            name=record['member']
            gender=''
            address=''
            state_id=1
            if state_id==1:
                state='AP'
            else:
                state='TS'
            constituency=record['constituency_id']
            voter_id=''
            age=record['age']
            if age=='' or age is None:
                age=0
            relation_name=''
            booth_id=0
            referral_id=0
            email=''
            house_no=''
            membership_through='OFNF'
            emobile = encrypt_decrypt(phone, 'E')
            membership_instance=Membership_details()
            if membership_instance.check_phonenumber_existence(emobile):
                status, membership_id = generate_membershipid1(name, phone, voter_id, relation_name, '', house_no, address,
                                                                   age, gender, booth_id, constituency, state, email, '',
                                                                   referral_id, membership_through, country_name='india')
                # print status,membership_id
    except Exception as e:
        print(str(e))

def update_paderu(filename):
    try:
        
        receipt_no = 0
        education = ''
        voter_id = ''
        email = ''
        gender = ''
        house_no = ''
        address = ''
        pincode = ''
        membership_through = 'OFNF'
        membership_instance = Membership_details()
        booth_id= 0
        
        count=0

        for current_row in range(6, num_rows):
             state = worksheet.cell_value(current_row, 1)
             district_name = worksheet.cell_value(current_row, 2)
             constituency = worksheet.cell_value(current_row, 3)
             mandal = int(worksheet.cell_value(current_row,4))
             village = worksheet.cell_value(current_row, 5)
             name = worksheet.cell_value(current_row, 6)
             relation_name = worksheet.cell_value(current_row, 7)
             age = int(worksheet.cell_value(current_row, 8))
             occupation = worksheet.cell_value(current_row, 9)
             phone = worksheet.cell_value(current_row, 10)
             phone = '91' + str(int(phone))

             # print state,district_name,constituency,mandal,village,name,relation_name,age,occupation,phone

             emobile = encrypt_decrypt(phone, 'E')
             if membership_instance.check_phonenumber_count(emobile):
                status, membership_id = generate_membershipid(name, phone, voter_id, relation_name, '', house_no, address,
                                                               age, gender, booth_id, constituency, state, email, '',
                                                               referral_id, membership_through, country_name='india')
                # print status
                if status:
                     count=count+1
                     # print count
                     # print membership_id
                     update_member_other_details(membership_id, '', village, mandal, education, occupation, pincode,
                                                 receipt_no, 0)
        # print count    
    except Exception as e:
        print(str(e)) 
    
    return ""


def update_missedcalls_numbers_status():
    book = open_workbook('./expired.xlsx')
    sheet = book.sheet_by_index(0)
    
    count=0
    con = psycopg2.connect(config.DB_CONNECTION)
    cur = con.cursor(cursor_factory=RealDictCursor)
    for row_index in xrange(1, sheet.nrows):

        try:
            mobile = sheet.cell(row_index, 0).value

            mobile = str(int(mobile))
            # print mobile
            cur.execute("update janasena_missedcalls set member_status='E' where phone=%s",(mobile,))
            con.commit()
            count=count+1
            # print count

        except Exception as e:
            print(str(e))
            tn = 0
            continue
    con.close()
    return 'done'
    