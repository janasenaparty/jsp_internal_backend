# -*- coding: utf-8 -*-
import json

import requests

import config

def sendBULKSMS(mno_msg):

    try:
        url = "http://api.smscountry.com/SMSCWebservice_MultiMessages.asp"

        querystring = {"User": config.sms_country_user, "passwd": config.sms_country_password, "mno_msg": mno_msg,
                       "sid": "JAIJSP", "mtype":"LNG", "DR": "N"}
        headers = {}
        
        response = requests.request("POST", url, headers=headers, params=querystring)
        # print response.text

    except Exception as e:
        print(str(e))
        return ''

## pride sms bulksms
def sendBULKSMS_samemessage(message, contacts,language=None):
    
    try:
        # mobilenumbers = ','.join(map(str, contacts))
        # print 'sending'
        # url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx"

        # querystring = {"User": config.sms_country_user, "passwd": config.sms_country_password,
        #                "mobilenumber": mobilenumbers, "message": message, "sid": "JAIJSP", "mtype": "LNG", "DR": "N"}
        

        if language=='unicode':
           language=3
        else:
            language=1
        url="http://api.pridesms.in/smpp/sendsms"
        mobilenumbers = ','.join(map(str, contacts))
        querystring={"username":config.pridesms_username,"password":config.pridesms_password,"to":mobilenumbers,"from":"JAIJSP","text":message,"category":"bulk","coding":language}
        headers = {}

        response = requests.request("GET", url, headers=headers, params=querystring)
        # print response.text

    except Exception as e:
        print(str(e))
        return ''

## smstake bulksms
def send_bulk_sms_same_message(message, contacts):
    #mobilenumbers = ','.join(map(str, contacts))
    try:

        url = "https://49.50.67.32/smsapi/jsonapi.jsp"
        coding = 0
        if isinstance(message,unicode):
            coding = 2
        querystring = {"username": config.SMS_STAKE_UNAME, "password": config.SMS_STAKE_PASS,
                       "to": contacts, "text": message, "from": "JAIJSP", "coding" : str(coding), "flash":"0"}
        # print querystring
        headers = {"Content-Type":"application/json"}

        response = requests.post(url, headers=headers, data=json.dumps(querystring))
        # print response.text

    except Exception as e:
        print(str(e))
        return ''


def otp_sms(phone,message):
    try:
        url = "https://49.50.67.32/smsapi/jsonapi.jsp"
        coding = 0
        if isinstance(message,unicode):
            coding = 2
        querystring = {"username": config.SMS_STAKE_OTP_UNAME, "password": config.SMS_STAKE_OTP_PASS,
                       "to": [phone], "text": message, "from": "JAIJSP", "coding" : str(coding), "flash":"0"}
        # print querystring
        headers = {"Content-Type":"application/json"}

        response = requests.post(url, headers=headers, data=json.dumps(querystring))
        # print response.text

    except Exception as e:
        print(str(e))
        return ''
