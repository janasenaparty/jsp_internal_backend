from main import sendmailmandrill,draw_image
import config
import requests
import psycopg2
from utilities.sms import otp_sms
def send_thank_you_mail(email):
	msg="""<!DOCTYPE html><html><head><title></title><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><meta http-equiv="X-UA-Compatible" content="IE=edge" /><style type="text/css"> body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} img{-ms-interpolation-mode: bicubic;} img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;} table{border-collapse: collapse !important;} body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;} a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important; } @media screen and (max-width: 525px) { .wrapper { width: 100% !important; max-width: 100% !important; } .logo img { margin: 0 auto !important; } .mobile-hide { display: none !important; } .img-max { max-width: 100% !important; width: 100% !important; height: auto !important; } .responsive-table { width: 100% !important; } .padding { padding: 10px 5% 15px 5% !important; } .padding-meta { padding: 30px 5% 0px 5% !important; text-align: center; } .padding-copy { padding: 10px 5% 10px 5% !important; text-align: center; } .no-padding { padding: 0 !important; } .section-padding { padding: 50px 15px 50px 15px !important; } .mobile-button-container { margin: 0 auto; width: 100% !important; } .mobile-button { padding: 15px !important; border: 0 !important; font-size: 16px !important; display: block !important; } } div[style*="margin: 16px 0;"] { margin: 0 !important; }</style></head><body style="margin: 0 !important; padding: 0 !important;"><div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;"> Entice the open with some amazing preheader text. Use a little mystery and get those subscribers to read through...</div><table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td bgcolor="#fff" align="center" style="padding: 70px 15px 70px 15px;" class="section-padding"> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table"> <tr> <td> <table width="100%" border="0" cellspacing="0" cellpadding="0"> <tr> <td class="padding" align="center"> <a target="_blank"><img src="http://janasenaparty.org/static/img/video-2.png" width="500" height="400" border="0" alt="Insert alt text here" style="display: block; padding: 0; color: #666666; text-decoration: none; font-family: Helvetica, arial, sans-serif; font-size: 16px;" class="img-max"></a> </td> </tr> <tr> <td align="center"> <table width="100%" border="0" cellspacing="0" cellpadding="0"> <tr> <td align="center" style="padding-top: 25px;" class="padding"> <table border="0" cellspacing="0" cellpadding="0" class="mobile-button-container"> <tr> <td align="center" > <p style="text-align: left; font-size: 0.7rem; font-family: arial; color: #333; padding-left: 40px;">Stay connected to Janasena Party<br><br> Follow us on social media</p> <p style="text-align: left; font-size: 1.0rem; color: white; padding-left: 40px;"><a href="https://www.facebook.com/janasenaparty/" target="_blank"><img src="https://cdn2.iconfinder.com/data/icons/black-white-social-media/32/facebook_online_social_media-128.png" width="40px"></a><a href="https://m.youtube.com/janasenaparty" target="_blank"><img src="https://cdn2.iconfinder.com/data/icons/black-white-social-media/32/youtube_online_social_media-128.png" width="40px"></a><a href="https://twitter.com/janasenaparty" target="_blank"><img src="https://cdn2.iconfinder.com/data/icons/black-white-social-media/32/online_social_media_twitter-128.png" width="40px"></a> </p> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr></table></body></html> """
	subject=" Janasena Party | Thank you "
	name="JANASENA PARTY"
	sendmailmandrill(name,email,msg,subject)
	return True

def sendOTPSMS(numberToSend, msgToSend):
    try:
        url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx"

        querystring = {"User": config.sms_country_user_otp, "passwd": config.sms_country_password_otp,
                       "mobilenumber": numberToSend, "message": msgToSend, "sid": "JAIJSP", "mtype": "N", "DR": "Y"}

        requests.request("POST", url, headers={}, params=querystring)


    except Exception as e:
        print(str(e))
        return ''

def genEmailOtp(umobile, email, type=None):
    try:
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        if type is not None:
            cur.execute(
                "select verification_code,user_verify_seq_id,otp_counter from user_verify where verify_status=%s and otp_type=%s and user_mobile=%s and update_dttm >= NOW() - INTERVAL '15 minutes' ",
                ("N", type, umobile,))

        else:
            cur.execute(
                "select verification_code,user_verify_seq_id,otp_counter from user_verify where verify_status=%s and user_mobile=%s and update_dttm >= NOW() - INTERVAL '15 minutes' ",
                ("N", umobile))
        existing_otp = cur.fetchone()

        if existing_otp is None:
            from random import randint
            user_pin = randint(100000, 999999)

            cur.execute(
                "update user_verify set verify_status='E' where user_mobile=%s and  verify_status='N' and otp_type=%s;",
                (umobile, type))
            cur.execute(
                """INSERT INTO user_verify(user_mobile,verification_code,verify_status,otp_type,otp_counter) VALUES (%s,%s,'N',%s,%s);""",
                (umobile, user_pin, type, 1))
            con.commit()
        else:
            otp_counter = existing_otp[2]
            if otp_counter >= 5:
                return False
            cur.execute("update user_verify set otp_counter=otp_counter+1 where user_verify_seq_id=%s",
                        (existing_otp[1],))
            con.commit()

            user_pin = existing_otp[0]

        con.close()
        if type == 'MB':
            msgToSend = "OTP to continue JanaSena Party Membership Process is: " + str(user_pin) + "- Team JSP."


        else:
            msgToSend = "OTP from JanaSena Party  is: " + str(user_pin) + "- Team JSP."

        subject = "JanaSena Party Membership OTP"
        msgToSendEmail = msgToSend
        name = "JanaSena Party"
        sendmailmandrill(name, email, msgToSendEmail, subject)

        return user_pin
    except psycopg2.Error as e:
        print(str(e))
        con.close()
        return False

def genOtp(umobile, type=None):
    try:
        # print config.DB_CONNECTION
        con = psycopg2.connect(config.DB_CONNECTION)
        cur = con.cursor()
        # print config.DB_CONNECTION
        if type is not None:
            cur.execute(
                "select verification_code,user_verify_seq_id,otp_counter from user_verify where verify_status=%s and otp_type=%s and user_mobile=%s and update_dttm >= NOW() - INTERVAL '15 minutes' ",
                ("N", type, umobile,))

        else:
            cur.execute(
                "select verification_code,user_verify_seq_id,otp_counter from user_verify where verify_status=%s and user_mobile=%s and update_dttm >= NOW() - INTERVAL '15 minutes' ",
                ("N", umobile))
        existing_otp = cur.fetchone()

        if existing_otp is None:
            if umobile=='919676881991':
                user_pin='111111'
            else:

                from random import randint
                user_pin = randint(100000, 999999)

            cur.execute(
                "update user_verify set verify_status='E' where user_mobile=%s and  verify_status='N' and otp_type=%s;",
                (umobile, type))
            cur.execute(
                """INSERT INTO user_verify(user_mobile,verification_code,verify_status,otp_type,otp_counter) VALUES (%s,%s,'N',%s,%s);""",
                (umobile, user_pin, type, 1))
            con.commit()
        else:
            otp_counter = existing_otp[2]
            if otp_counter >= 5:
                return False
            cur.execute("update user_verify set otp_counter=otp_counter+1 where user_verify_seq_id=%s",
                        (existing_otp[1],))
            con.commit()

            user_pin = existing_otp[0]

        con.close()
        if type == "NL":
            numberToSend = str(umobile)
            msgToSend = "Hello , OTP for JanasenaParty NRI APP Login  is: " + str(user_pin) + " - Team JSP."
        elif type == 'NR':
            numberToSend = str(umobile)
            msgToSend = "Hello , OTP for JanasenaParty NRI APP Registration  is: " + str(user_pin) + " - Team JSP."
        elif type == 'MB':
            msgToSend = "OTP to continue JanaSena Party Membership Process is: " + str(user_pin) + "- Team JSP."
            numberToSend = str(umobile)
        else:
            msgToSend = "OTP from JanaSena Party is: " + str(user_pin) + "- Team JSP."
            numberToSend = str(umobile)

        sendOTPSMS(numberToSend, msgToSend)

        return user_pin
    except psycopg2.Error as e:
        print(str(e))
        con.close()
        return ''

def sendNotification(data):
    push_service = FCMNotification(api_key=config.fcm_api_key)

    registration_id = "dA12DWQlWZE:APA91bGkUA4FkoZ1uafrVfXkpfS_dlXW8GyBxfNV4zkiaYGUt2v4okdFEaDH5fezaJ6eveU2xpYS278BEcw6eOn7zLk_HLK4cTb6l-aaPC2w13H9MKDu0i2oL6P_FpEtU0KDEOKQlnya"
    message_title = "Janasena Update "
    message_body = "Hi Dalu, This is Test Notification From Backend"
    result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title,
                                               message_body=message_body)

    # # Send to multiple devices by passing a list of ids.
    # registration_ids = ["<device registration_id 1>", "<device registration_id 2>", ...]

    # result = push_service.notify_multiple_devices(registration_ids=registration_ids, message_title=message_title, message_body=message_body)

    # print result

def senddonationsemail1(data):
    message = open("./thanks_note.html").read()

    try:

        email = data['email']
        mobile = data['phone']
        donar_name = data['donar_name']
        date = data['date']
        amount = data['transaction_amount']
        receipt_id = data['transaction_id']
        email_message = message

        donation_certificate = draw_image(data)
        attachments = [donation_certificate]
        subject = "Thank you for donating to Janasena Party"
        if email != '':
            name = "JANASENA PARTY"
            subject = "Thank you for donating to JANASENA PARTY"
            email_message = email_message.replace("{{donar_name}}", str(donar_name))
            email_message = email_message.replace("{{amount}}", str(amount))
            email_message = email_message.replace("{{receipt_id}}", str(receipt_id))
            email_message = email_message.replace("{{date}}", str(date))

            sendmailmandrill(name, email, email_message, subject, attachments=attachments)

    except Exception as e:
        print(str(e))

    return " sent successfully"