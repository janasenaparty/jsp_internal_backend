echo "setting up virtual environment"
virtualenv jspvenv
jspvenv\Scripts\activate
pip install -r requirements.txt
python wsgi.py